<?php

namespace gapi\template;

abstract class Driver
{
    //过滤标签
    protected array $patterns; //规则
    protected array $tags; //扩展标签
    protected bool $error = false;
    protected string $error_msg = '';
    protected string $suffix = '.html';

    //初始化
    public function __construct()
    {
        $this->setPatterns();
    }

    //模板输出
    public function compile(string $tpl)
    {


    }

    //模板变量
    public function translate(array $matches)
    {


    }

    public function extend(array $matches)
    {

    }

    public function error(string $msg)
    {
        $this->error = true;
        $this->error_msg = $msg;
    }

    public function getError()
    {
        return $this->error;
    }

    public function showError()
    {
        return $this->error_msg;
    }

}

?>