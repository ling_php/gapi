<?php

namespace gapi\template\driver;

use gapi\Config;
use gapi\template\Driver;
use gapi\View;


class Ling extends Driver
{
    public static array $includeFile = [];

    public function setPatterns()
    {

        //扩展标签
        $tags = '';
        if (!empty($this->tags)) {
            $tags = '|' . implode('|', $this->tags);
        }

        $this->patterns = [
            '/{(:|~)([^\}]+)}/i', // {:date("Y-m-d")} 方法 和 输出 内容的方法
            '/{(\/)?(\$|foreach|for|if|include|url|elseif|else|while|html|php' . $tags . ')\s*([^\}]*)\s*}/i', //{foreah name="lists" item="value" key="key"}
        ];
    }

    /**
     * 替换
     * @param mixed $matches
     * @return mixed
     * @throws \Exception
     */
    public function translate(mixed $matches): mixed
    {

        //print_r($matches);
        if ($matches[0] == '{php}') return '<?php ';
        if ($matches[0] == '{/php}') return '?>';
        if ($matches[1] == '/') return '<?php }?>';
        if ($matches[1] == ':') return '<?php echo ' . $matches[2] . ';?>';
        if ($matches[1] == '~') return '<?php ' . $matches[2] . ';?>';
        switch ($matches[2]) {
            case '$':
            {
                $str = $matches[3] = $this->parseVar(trim($matches[3]));
                $data = explode('|', $str);
                if ($str[0] == '.' || $str[0] == '(') return $matches[0];
                $len = count($data);

                if ($len == 1) return '<?php echo $' . $matches[3] . ' ?? \'\';?>';
                else if ($len == 2) return '<?php echo isset($' . $data[0] . ') ? $' . $data[0] . ':' . $data[1] . ';?>';
                else if ($len > 2) {
                    $filter = strtolower($data[$len - 1]);
                    switch ($filter) {
                        case 'encode':
                            return '<?php echo isset($' . $data[0] . ') ? htmlspecialchars($' . $data[0] . '):' . $data[1] . ';?>';
                        case 'int':
                        case 'str':
                        case 'float':
                        case 'txt':
                        case 'sql':
                        case 'text':
                            return '<?php echo isset($' . $data[0] . ') ? Format::' . $filter . '($' . $data[0] . '):' . $data[1] . ';?>';
                        default:
                            return '<?php echo isset($' . $data[0] . ') && $' . $data[0] . '?' . $data[1] . ':' . $data[2] . ';?>';
                    }
                }
            }
            case 'if':
                return '<?php if(' . $matches[3] . '){?>';
            case 'elseif':
                return '<?php }elseif(' . $matches[3] . '){?>';
            case 'else':
                return '<?php }else{' . $matches[3] . '?>';
            case 'php':
                return '<?php ' . $matches[3] . ';?>';
            case 'while':
                return '<?php while(' . $matches[3] . '){?>';
            case 'html':
                $attr = $this->getAttrs($matches[3]);
                $attr_html = '';
                foreach ($attr as $k => $v) {
                    if ($v != '') {
                        switch ($k) {
                            //特别的
                            case 'onclick':
                                $attr[$k] = "'{$v}'";
                                break;
                            default:
                                $attr[$k] = $this->parseValue($v);
                                break;
                        }
                        $attr_html .= "'{$k}' => {$attr[$k]},";
                    }
                }
                return "<?php echo \\gapi\\form\\Form::fetch([{$attr_html}]);?>";
            case 'list':
            case 'foreach':
            {
                $attr = $this->getAttrs($matches[3]);
                $attr['name'] = $attr['name'] ?? '$items';
                $attr['name'] = strpos($attr['name'], '$') === false ? '$' . $attr['name'] : $attr['name'];
                $attr['key'] = isset($attr['key']) ? '$' . $attr['key'] : '$key';
                $attr['item'] = isset($attr['item']) ? '$' . $attr['item'] : '$item';
                return '<?php if(!empty(' . $attr['name'] . ')) foreach(' . $attr['name'] . ' as ' . $attr['key'] . ' => ' . $attr['item'] . '){?>';
            }
            case 'for':
            {
                $attr = $this->getAttrs($matches[3]);
                $attr['name'] = isset($attr['name']) ? '$' . $attr['name'] : '$i';
                $attr['from'] = $attr['from'] ?? 0;
                $attr['to'] = $attr['to'] ?? 10;
                $attr['step'] = $attr['step'] ?? 1;
                return '<?php for(' . $attr['name'] . '=' . $attr['from'] . ';' . $attr['name'] . '<=' . $attr['to'] . ';' . $attr['name'] . '=' . $attr['name'] . '+' . $attr['step'] . '){?>';
            }
            case 'include':
            {
                //{include file='public/footer.html' module='admin'} 支持跨模块调用
                $attr = $this->getAttrs($matches[3]);
                $file_tmp = explode('/', $attr['file']);
                if (count($file_tmp) == 3) {
                    $module = $file_tmp[0];
                    $file = $file_tmp[1] . DS . $file_tmp[2];
                } else {
                    $file = $attr['file'];
                    $module = $attr['module'] ?? '';
                }
                $tpl = View::instance()->path($module) . DS . trim($file) . $this->suffix;

                /*return '<?php include(\''.$tpl.'\');?>';*/
                return $this->compile($tpl);
            }
            default:
            {
                return $this->extend($matches);
            }
        }

    }

    /**
     * 模板解析扩展
     * @param mixed $matches
     * @return mixed
     */
    public function extend(mixed $matches): mixed
    {
        $tag = $matches[2];
        if (trim($tag) != '' && method_exists($this, $tag)) {
            return $this->$tag($this->getAttrs($matches[3]));
        }
    }

    /**
     * 分析标签属性
     * @param string $str
     * @return array
     */
    public function getAttrs(string $str): array
    {
        preg_match_all('/(\w+)\s*=([\"][^=]+[\"])|(\w+)\s*=([\'][^=]+[\'])/iU', trim($str), $attrs);

        $attr = [];
        foreach ($attrs[0] as $value) {
            $tem = explode('=', $value, 2);
            $attr[trim($tem[0])] = preg_replace("/^[\"\']|[\"\']$/", '', trim($tem[1]));
        }
        return $attr;
    }

    /**
     * 编译文件
     * @param mixed|string $tpl
     * @return string
     * @throws \Exception
     */
    public function compile(mixed $tpl = ''): string
    {
        self::$includeFile[$tpl] = filemtime($tpl);
        $str = '';
        try {
            if (file_exists($tpl)) {
                $str = file_get_contents($tpl);
            } else {
                $this->error('模板文件' . str_replace(ROOT_PATH, '', $tpl) . '不存在');
            }
        } catch (Exception $e) {
            throw new \Exception($e);
        }

        //解析block
        $str = $this->parseBlock($str);
        //过滤静态标签
        $static_path = Config::app()['static_path'];
        $str = str_replace(array_keys($static_path), $static_path, $str);

        return preg_replace_callback($this->patterns, [$this, 'translate'], $str);
    }

    public function compileFile(string $file, string $cache_file): void
    {
        if (!$this->checkCache($file)) {
            $content = $this->compile($file);
            //加权限
            $content = '<?php if(!defined(\'CORE_NAME\')) exit(\'ACCESS DENY\'); /*' . serialize(self::$includeFile) . '*/?>' . $content;
            $cache_template_path = dirname($cache_file);
            if (!is_dir($cache_template_path)) {
                @mkdir($cache_template_path, 0777, 1);
            }

            //是否过滤空格
            if (Config::app()['tpl_strip_space']) {
                /* 去除html空格与换行 */
                $find = array('/>\s+</s', '/>[\n\r\s]+/');
                $replace = array('> <', '> ');
                $content = preg_replace($find, $replace, $content);
            }
            file_put_contents($cache_file, $content);
        }
    }

    public function checkCache(string $cache_file): bool
    {
        // 缓存文件不存在
        if (!is_file($cache_file)) {
            return false;
        }
        // 读取缓存文件失败
        if (!$handle = @fopen($cache_file, "r")) {
            return false;
        }
        // 读取第一行
        preg_match('/\/\*(.+?)\*\//', fgets($handle), $matches);
        if (!isset($matches[1])) {
            return false;
        }
        $includeFile = unserialize($matches[1]);
        if (!is_array($includeFile)) {
            return false;
        }
        // 检查模板文件是否有更新
        foreach ($includeFile as $path => $time) {
            if (is_file($path) && filemtime($path) > $time) {
                // 模板文件如果有更新则缓存需要更新
                return false;
            }
        }
        return true;
    }


    /**
     * 解析block
     * @param mixed $content
     * @return string
     */
    public function parseBlock(mixed $content): string
    {
        //解析出  extend
        preg_match_all("/{extend\s+name=[\"\']{1}([^\"\']+)[\"\']{1}\s*}/sU", $content, $extend_arr);

        if (isset($extend_arr[1][0]) && $extend_arr[1][0] != '') {
            $extend = $extend_arr[1][0];
        } else {
            return self::clearBlock($content);
        }

        $lists = explode('/', $extend);

        if ($lists == 3) {
            $view_path = View::instance()->path($lists[0]);
            unset($lists[0]);
        } else {
            $view_path = View::instance()->path();
        }

        $file = $view_path . implode(DS, $lists) . $this->suffix;
        if (file_exists($file)) {
            $html = file_get_contents($file);
        } else {
            return self::clearBlock($content);
        }


        //解析当前的block
        preg_match_all("/{block\s+name=[\"\']{1}([^\"\']+)[\"\']{1}\s*}.*{\/block}/sU", $content, $current_block_arr);

        $current_block = [];
        if (!empty($current_block_arr)) {
            foreach ($current_block_arr[1] as $key => $value) {
                $current_block[$value] = $current_block_arr[0][$key];
            }
        }

        //解析 继承的block
        preg_match_all("/{block\s+name=[\"\']{1}([^\"\']+)[\"\']{1}\s*}.*{\/block}/sU", $html, $block_arr);
        $block = [];
        if (!empty($block_arr)) {
            foreach ($block_arr[1] as $key => $value) {
                $block[$value] = $block_arr[0][$key];
            }
        }

        if (!empty($current_block)) {
            foreach ($current_block as $key => $value) {
                $html = str_replace($block[$key], $value, $html);
            }
        }
        //除去 block
        $html = self::clearBlock($html);
        return $html;
    }

    public function clearBlock(string $content)
    {
        return preg_replace("/\n?{block\s+name=[\"\']{1}([^\"\']+)[\"\']{1}\s*}\n?|\n?{\/block}\n?/", '', $content);
    }

    /**
     * 解析值
     * @param mixed $value
     * @return string
     */
    public function parseValue(mixed $value): string
    {
        if ($value == '') {
            $value = "''";
        } elseif (str_contains($value, '(')) {
            // 可以执行方法
        } elseif (str_contains($value, '$')) {
            //可以使用变量
        } else {
            $value = "'{$value}'";
        }
        return $value;
    }

    /**
     * 解析变量
     * @param string $var
     * @return string
     */
    public function parseVar(string $var): string
    {
        $arr = explode('.', $var);
        $name = $arr[0];
        unset($arr[0]);
        $var_arr = [];
        if (!empty($arr)) {
            foreach ($arr as $v) {
                $var_arr[] = "['{$v}']";
            }
        }
        return $name . implode('', $var_arr);
    }

}

?>