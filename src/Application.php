<?php
declare(strict_types=1);

namespace gapi;

use gapi\lib\Logger;

date_default_timezone_set('PRC');
define('RUN_START_TIME', microtime(true));
define('RUN_START_MEM', memory_get_usage());
define('CORE_PATH', __DIR__);
define('ROOT_PATH', dirname(CORE_PATH));
define('DS', DIRECTORY_SEPARATOR);
define('CORE_NAME', 'LingCMS');
define('APP_PATH', ROOT_PATH);
define('RUNTIME_PATH_NAME', 'runtime');
define('COLUMNS_PATH_NAME', 'columns');
define('EXT', '.php');
define('NOW_TIME', time());

ini_set('session.gc_lifetime', "86400"); // 秒
ini_set("session.cookie_lifetime", "86400"); // 秒

class Application
{
    private $name = 'GAPI';

    public function __construct(
        public string $version = '',
    )
    {

    }

    /**
     * 创建项目
     * @return $this
     * @throws \Exception
     */
    public function create(): self
    {
        header('X-Powered-By: Ling');
        $this->version = $this->version == '' ? Config::system()['app_version'] : $this->version;
        if ($v = (new Request())->get('v', '')) {
            $this->version = 'v' . $v;
        }
        define('APP_VERSION', $this->version);
        define('VERSION_PATH', APP_PATH . DS . $this->version);
        define('APP_DEBUG', Config::app('config.php')['debug']);
//        if (!is_dir(VERSION_PATH)) {
//            throw new \Exception(VERSION_PATH . ' 目录不存在.');
//        }
        define('COLUMNS_PATH', VERSION_PATH . DS . COLUMNS_PATH_NAME);
        Logger::info("version => {$this->version} - {$this->name}");
        define('RUNTIME_PATH', VERSION_PATH . DS . RUNTIME_PATH_NAME);
        if (!is_dir(RUNTIME_PATH)) {
            mkdir(RUNTIME_PATH, 0777, true);
        }

        //加载公共方法
        if (is_file(VERSION_PATH . DS . 'common.php')) {
            require VERSION_PATH . DS . 'common.php';
        }

        return $this;
    }

    /**
     * 发送请求
     * @param array|null $params
     * @param \Closure|null $callback
     * @throws exception\CompileErrorException
     * @throws exception\CoreErrorException
     * @throws exception\CoreWarningException
     * @throws exception\DeprecatedException
     * @throws exception\ErrorException
     * @throws exception\NoticeException
     * @throws exception\ParseException
     * @throws exception\RecoverableErrorException
     * @throws exception\StrictException
     * @throws exception\UserDeprecatedException
     * @throws exception\UserNoticeException
     * @throws exception\UserWarningException
     * @throws exception\WarningException
     */
    public function send(?array $params = [], \Closure $callback = null): void
    {

        set_error_handler(callback: static function ($err_severity, $err_msg, $err_file, $err_line) {
            if (0 === error_reporting()) {
                return false;
            }
            match ($err_severity) {
                E_ERROR => throw new \gapi\exception\ErrorException ($err_msg, 0, $err_severity, $err_file, $err_line),
                E_WARNING => throw new \gapi\exception\WarningException ($err_msg, 0, $err_severity, $err_file, $err_line),
                E_PARSE => throw new \gapi\exception\ParseException  ($err_msg, 0, $err_severity, $err_file, $err_line),
                E_NOTICE => throw new \gapi\exception\NoticeException  ($err_msg, 0, $err_severity, $err_file, $err_line),
                E_CORE_ERROR => throw new \gapi\exception\CoreErrorException  ($err_msg, 0, $err_severity, $err_file, $err_line),
                E_CORE_WARNING, E_COMPILE_WARNING => throw new \gapi\exception\CoreWarningException  ($err_msg, 0, $err_severity, $err_file, $err_line),
                E_COMPILE_ERROR => throw new \gapi\exception\CompileErrorException  ($err_msg, 0, $err_severity, $err_file, $err_line),
                E_USER_ERROR => throw new \gapi\exception\UserErrorException  ($err_msg, 0, $err_severity, $err_file, $err_line),
                E_USER_WARNING => throw new \gapi\exception\UserWarningException  ($err_msg, 0, $err_severity, $err_file, $err_line),
                E_USER_NOTICE => throw new \gapi\exception\UserNoticeException  ($err_msg, 0, $err_severity, $err_file, $err_line),
                E_STRICT => throw new \gapi\exception\StrictException  ($err_msg, 0, $err_severity, $err_file, $err_line),
                E_RECOVERABLE_ERROR => throw new \gapi\exception\RecoverableErrorException  ($err_msg, 0, $err_severity, $err_file, $err_line),
                E_DEPRECATED => throw new \gapi\exception\DeprecatedException  ($err_msg, 0, $err_severity, $err_file, $err_line),
                E_USER_DEPRECATED => throw new \gapi\exception\UserDeprecatedException  ($err_msg, 0, $err_severity, $err_file, $err_line),
            };
        }, error_levels: E_ALL);
        try {
            Loader::autoload();
            if (!defined('NO_HOOK')) {
                $tag_file = VERSION_PATH . DS . 'tags' . EXT;
                if (is_file($tag_file)) {
                    Hook::import(include $tag_file);
                }
                Hook::listen('app_init');
            }
            if (is_callable($callback)) {
                $callback();
            } else {
                (new Route())->send($params);
            }
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            $file = $e->getFile();
            $line = $e->getLine();
            $errorType = str_replace('gapi\\exception\\', '', get_class($e));
            Logger::error("{$errorType} ：{$msg}\n\t\t in {$file} on line {$line}");
            if (!APP_DEBUG) {
                $result = (new Response())->setCode(500)->setExtra([
                    'status' => false,
                    'msg' => "Internal Server Error",
                ]);
            } else {
                $result = (new Response())->setCode(500)->setExtra([
                    'status' => false,
                    'msg' => "{$errorType}:{$msg}",
                    'error' => "{$errorType} ：{$msg}\n\t\t in {$file} on line {$line}",
                ]);
            }
            $result = (string)$result;
            if (Config::app('config.php')['trace']) {
                echo Logger::getTrace();
            }
            echo $result;
        }
    }


}