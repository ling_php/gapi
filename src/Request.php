<?php

namespace gapi;

use gapi\lib\Checker;

class Request
{
    public static array $routes = [];
    public static array $middleWareData = [];
//    public static array $options = [
//        'HEAD',
//        'GET',
//        'POST',
//        'PUT',
//        'PATCH',
//        'DELETE',
//        'PURGE',
//        'OPTIONS',
//        'TRACE',
//        'CONNECT',
//    ];
    private static $instance = null;

    public static function instance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }


    public function __construct()
    {

    }


    /**
     * @return string
     */
    public function domain(): string
    {
        return $this->getScheme() . '://' . $this->getHttpHost();
    }

    /**
     * @return bool
     */
    private function isSecure(): bool
    {
        $https = $this->server->get('HTTPS');
        return !empty($https) && 'off' !== strtolower($https);
    }

    /**
     * @return string|null
     */
    private function getHttpHost(): ?string
    {
        $scheme = $this->getScheme();
        $port = $this->getPort();

        if (('http' === $scheme && 80 === $port) || ('https' === $scheme && 443 === $port)) {
            return $this->getHost();
        }

        return $this->getHost() . ':' . $port;
    }

    /**
     * 获取Host
     * @return string|null
     */
    public function getHost(): ?string
    {
        return $this->server->get('HTTP_HOST');
    }

    /**
     * 获取当前完整网址
     */
    public function url(): string
    {
        return $this->domain() . $this->requestUri;
    }

    /**
     * 设置或获取当前URL 不含QUERY_STRING
     * @access public
     * @param string $url URL地址
     * @return string
     */
    public function baseUrl($url = null)
    {
        return $this->domain() . Route::uri();
    }

    /**
     * @return string
     */
    public function getScheme(): string
    {
        return $this->isSecure() ? 'https' : 'http';
    }

    /**
     * 检测是否使用手机访问
     * @access public
     * @return bool
     */
    public function isMobile()
    {
        if (isset($_SERVER['HTTP_VIA']) && stristr($_SERVER['HTTP_VIA'], "wap")) {
            return true;
        } elseif (isset($_SERVER['HTTP_ACCEPT']) && strpos(strtoupper($_SERVER['HTTP_ACCEPT']), "VND.WAP.WML")) {
            return true;
        } elseif (isset($_SERVER['HTTP_X_WAP_PROFILE']) || isset($_SERVER['HTTP_PROFILE'])) {
            return true;
        } elseif (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/(blackberry|configuration\/cldc|hp |hp-|htc |htc_|htc-|iemobile|kindle|midp|mmp|motorola|mobile|nokia|opera mini|opera |Googlebot-Mobile|YahooSeeker\/M1A1-R2D2|android|iphone|ipod|mobi|palm|palmos|pocket|portalmmm|ppc;|smartphone|sonyericsson|sqh|spv|symbian|treo|up.browser|up.link|vodafone|windows ce|xda |xda_)/i', $_SERVER['HTTP_USER_AGENT'])) {
            return true;
        } else {
            return false;
        }
    }

    public function isWeixin(): bool
    {
        return strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false;
    }

    /**
     * @return int
     */
    public function getPort(): int
    {
        return (int)$this->server->get('SERVER_PORT');
    }

    public function route(mixed $route = null): mixed
    {
        if (is_array($route)) {
            self::$routes = $route;
            return $this;
        } else if (is_string($route)) {
            return self::$routes[$route] ?? '';
        } else {
            return self::$routes;
        }
    }


    /**
     * @param bool $flag
     * @return int|string
     */
    public function getClientIp(bool $flag = false): int|string
    {
        $server = $this->server->get();
        if ($server) {
            $server["REMOTE_ADDR"] = isset($server["REMOTE_ADDR"]) ? $server["REMOTE_ADDR"] : '';
            $ip_address = $server["HTTP_X_FORWARDED_FOR"] ?? $server["HTTP_CLIENT_IP"] ?? $server["REMOTE_ADDR"];
        } else if (getenv("HTTP_X_FORWARDED_FOR")) {
            $ip_address = getenv("HTTP_X_FORWARDED_FOR");
        } else if (getenv("HTTP_CLIENT_IP")) {
            $ip_address = getenv("HTTP_CLIENT_IP");
        } else {
            $ip_address = getenv("REMOTE_ADDR");
        }
        $ip = preg_match('/[\d\.]{7,15}/', $ip_address, $matches) ? $matches [0] : '';
        return $flag ? ip2long($ip) : $ip;
    }

    public static function initGetData()
    {
        //清除Route路由get参数
        if (isset($_GET[Route::$flag])) {
            unset($_GET[Route::$flag]);
        }
        return new Data($_GET);
    }

    public static function get(?string $key = '', ?string $default = '', ?string $filter = '', ?string $filter_msg = ''): string|array
    {
        //清除
        if ($key == '') {
            return self::initGetData()->get();
        }
        $value = '';
        if (isset($_GET[$key])) {
            $value = $_GET[$key];
        }

        if ($filter != '') {
            self::filter($filter, $key, $value, $filter_msg);
        }
        $value = $value === '' ? $default : $value;

        return $value;
    }

    public static function post(?string $key = '', ?string $default = '', ?string $filter = '', ?string $filter_msg = ''): string|array
    {
        if ($key == '') {
            return $_POST;
        }
        $value = '';
        if (isset($_POST[$key])) {
            $value = $_POST[$key];
        }
        if ($filter != '') {
            self::filter($filter, $key, $value, $filter_msg);
        }
        $value = $value === '' ? $default : $value;
        return $value;
    }

    public static function all(?string $key = '', ?string $default = '', ?string $filter = '', ?string $filter_msg = ''): string|array
    {
        if ($key == '') {
            return $_REQUEST;
        }
        $value = '';
        if (isset($_REQUEST[$key])) {
            $value = $_REQUEST[$key];
        }
        if ($filter != '') {
            self::filter($filter, $key, $value, $filter_msg);
        }
        $value = $value === '' ? $default : $value;
        return $value;
    }

    public static function filter(string $filter, string $key, mixed $value, string $msg = '')
    {
        if (method_exists(Checker::class, $filter)) {
            if (!Checker::$filter($value)) {
                $msg = $msg == '' ? Checker::msg($filter, $key) : $msg;
                (new Response())->setMsg($msg)->send();
            }
        } else {
            (new Response())->setMsg('不存在的过滤器:' . $filter)->send();
        }
    }


    /**
     * 懒加载 对方参数
     * @param $property
     * @return mixed
     */
    public function __get($property): mixed
    {
        return match ($property) {
            'route' => $this->route = new Data(self::$routes),
            'get' => $this->get = self::initGetData(),
            'post' => $this->post = new Data($_POST),
            'uri' => $this->uri = $this->requestUri(),
            'session' => Session::class,
            'cookie' => Cookie::class,
            'files' => $this->files = new Data($_FILES),
            'path' => $this->get->get(Route::$flag),
            'header' => $this->header = self::header(),
            'server' => $this->server = new Data($_SERVER),
            'request' => $this->request = new Data($_REQUEST),
            'input' => $this->input = file_get_contents('php://input'),
            'method' => $this->methobaseUrld = strtoupper($this->server->get('REQUEST_METHOD')),
            'baseUrl' => $this->baseUrl()
        };
    }

    public static function __callStatic(string $method, array $params): void
    {

    }

    private static function header()
    {
        $headers = [];
        $copy_server = [
            'CONTENT_TYPE' => 'Content-Type',
            'CONTENT_LENGTH' => 'Content-Length',
            'CONTENT_MD5' => 'Content-Md5',
        ];

        foreach ($_SERVER as $key => $value) {
            if (substr($key, 0, 5) === 'HTTP_') {
                $key = substr($key, 5);
                if (!isset($copy_server[$key]) || !isset($_SERVER[$key])) {
                    $key = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', $key))));
                    $headers[$key] = $value;
                }
            } elseif (isset($copy_server[$key])) {
                $headers[$copy_server[$key]] = $value;
            }
        }

        if (!isset($headers['Authorization'])) {
            if (isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION'])) {
                $headers['Authorization'] = $_SERVER['REDIRECT_HTTP_AUTHORIZATION'];
            } elseif (isset($_SERVER['PHP_AUTH_USER'])) {
                $basic_pass = isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : '';
                $headers['Authorization'] = 'Basic ' . base64_encode($_SERVER['PHP_AUTH_USER'] . ':' . $basic_pass);
            } elseif (isset($_SERVER['PHP_AUTH_DIGEST'])) {
                $headers['Authorization'] = $_SERVER['PHP_AUTH_DIGEST'];
            }
        }

        return new Data($headers);
    }

    /**
     * 获取上传file数据
     * @param array|string $name
     * @return mixed
     */
    public function file(array|string $name = ''): mixed
    {
        if (empty($this->file)) {
            $this->file = isset($_FILES) ? $_FILES : [];
        }
        if (is_array($name)) {
            return $this->file = array_merge($this->file, $name);
        }
        $files = $this->file;
        if (!empty($files)) {
            // 处理上传文件
            $array = [];
            foreach ($files as $key => $file) {
                if (is_array($file['name'])) {
                    $item = [];
                    $keys = array_keys($file);
                    $count = count($file['name']);
                    for ($i = 0; $i < $count; $i++) {
                        if (empty($file['tmp_name'][$i]) || !is_file($file['tmp_name'][$i])) {
                            continue;
                        }
                        $temp['key'] = $key;
                        foreach ($keys as $_key) {
                            $temp[$_key] = $file[$_key][$i];
                        }
                        $item[] = (new \gapi\lib\File($temp['tmp_name']))->setUploadInfo($temp);
                    }
                    $array[$key] = $item;
                } else {
                    if ($file instanceof File) {
                        $array[$key] = $file;
                    } else {
                        if (empty($file['tmp_name']) || !is_file($file['tmp_name'])) {
                            continue;
                        }
                        $array[$key] = (new \gapi\lib\File($file['tmp_name']))->setUploadInfo($file);
                    }
                }
            }
            if (strpos($name, '.')) {
                list($name, $sub) = explode('.', $name);
            }
            if ('' === $name) {
                // 获取全部文件
                return $array;
            } elseif (isset($sub) && isset($array[$name][$sub])) {
                return $array[$name][$sub];
            } elseif (isset($array[$name])) {
                return $array[$name];
            }
        }
        return false;
    }

    /**
     * 带参数 uri
     * @return string
     */
    public function uri(): string
    {
        return $this->requestUri();
    }

    /**
     * 不带参数 uri
     * @return string
     */
    public function routeUri(): string
    {
        return Route::uri();
    }


    private function requestUri(): string
    {
        $requestUri = '';

        if ('1' === $this->server->get('IIS_WasUrlRewritten') && '' !== $this->server->get('UNENCODED_URL')) {
            $requestUri = $this->server->get('UNENCODED_URL');
            $this->server->remove('UNENCODED_URL');
            $this->server->remove('IIS_WasUrlRewritten');
        } elseif ($this->server->has('REQUEST_URI')) {
            $requestUri = $this->server->get('REQUEST_URI');
            if ('' !== $requestUri && str_starts_with($requestUri, '/')) {
                if (false !== $pos = strpos($requestUri, '#')) {
                    $requestUri = substr($requestUri, 0, $pos);
                }
            } else {
                $uriComponents = parse_url($requestUri);
                if (isset($uriComponents['path'])) {
                    $requestUri = $uriComponents['path'];
                }
                if (isset($uriComponents['query'])) {
                    $requestUri .= '?' . $uriComponents['query'];
                }
            }
        } elseif ($this->server->has('ORIG_PATH_INFO')) {
            $requestUri = $this->server->get('ORIG_PATH_INFO');
            if ('' !== $this->server->get('QUERY_STRING')) {
                $requestUri .= '?' . $this->server->get('QUERY_STRING');
            }
            $this->server->remove('ORIG_PATH_INFO');
        }

        $this->server->set('REQUEST_URI', $requestUri);
        return $requestUri;
    }


    public function isPost(): bool
    {
        return $this->method == 'POST';
    }

    public function isGet(): bool
    {
        return $this->method == 'GET';
    }

    public function isRequest()
    {
        return $this->isPost() || $this->isGet();
    }

    public function isAjax(): bool
    {
        return $this->server->has('HTTP_X_REQUESTED_WITH') && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
    }

    public function isXmlHttpRequest(): bool
    {
        return 'XMLHttpRequest' === $this->header->get('X-Requested-With');
    }

    public function isPut(): bool
    {
        return $this->method == 'PUT';
    }

    public function isDelete(): bool
    {
        return $this->method == 'DELETE';
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function __call(string $method, array $params): bool
    {
        return match ($method) {
            'isGet' => $this->method == 'GET',
            'isPost' => $this->method == 'POST',
            'isAjax' => $this->server->has('HTTP_X_REQUESTED_WITH') && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest',
            'isXmlHttpRequest' => 'XMLHttpRequest' === $this->header->get('X-Requested-With'),
            'isPut' => $this->method == 'PUT',
            'isDelete' => $this->method == 'DELETE',
        };
    }


    public function module(): string
    {
        return MODULE_NAME;
    }

    public function controller(): string
    {
        return CONTROLLER_NAME;
    }

    public function action(): string
    {
        return ACTION_NAME;
    }

    public function getRoute(): string
    {
        return strtolower(MODULE_NAME . '/' . CONTROLLER_NAME . '/' . ACTION_NAME);
    }

    public function middleWareData(array $data): void
    {
        self::$middleWareData = array_merge(self::$middleWareData, $data);
    }

    public function getMiddleWareData()
    {
        return self::$middleWareData;
    }


}