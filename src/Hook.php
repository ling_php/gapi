<?php

namespace gapi;

use gapi\lib\Logger;

class Hook
{
    /**
     * @var array 标签
     */
    private static $tags = [];

    /**
     * 动态添加行为扩展到某个标签
     * @access public
     * @param string $tag 标签名称
     * @param mixed $behavior 行为名称
     * @param bool $first 是否放到开头执行
     * @return void
     */
    public static function add($tag, $behavior, $first = false)
    {
        isset(self::$tags[$tag]) || self::$tags[$tag] = [];

        if (is_array($behavior) && !is_callable($behavior)) {
            if (!array_key_exists('_overlay', $behavior) || !$behavior['_overlay']) {
                unset($behavior['_overlay']);
                self::$tags[$tag] = array_merge(self::$tags[$tag], $behavior);
            } else {
                unset($behavior['_overlay']);
                self::$tags[$tag] = $behavior;
            }
        } elseif ($first) {
            array_unshift(self::$tags[$tag], $behavior);
        } else {
            self::$tags[$tag][] = $behavior;
        }
    }

    /**
     * 批量导入插件
     * @access public
     * @param array $tags 插件信息
     * @param boolean $recursive 是否递归合并
     * @return void
     */
    public static function import(array $tags, $recursive = true)
    {
        if ($recursive) {
            foreach ($tags as $tag => $behavior) {
                self::add($tag, $behavior);
            }
        } else {
            self::$tags = $tags + self::$tags;
        }
    }

    /**
     * 获取插件信息
     * @access public
     * @param string $tag 插件位置(留空获取全部)
     * @return array
     */
    public static function get(string $tag = ''): array
    {
        if (empty($tag)) {
            return self::$tags;
        }

        return array_key_exists($tag, self::$tags) ? self::$tags[$tag] : [];
    }

    /**
     * 监听标签的行为
     * @param string $tag
     * @param array $params
     */
    public static function listen(string $tag, array &$params = []):void
    {
        foreach (static::get($tag) as $key => $name) {
            self::exec($name, $tag, $params);
        }
    }

    /**
     * 执行某个行为
     * @param string $class
     * @param string $tag
     * @param array $params
     */
    public static function exec(string $class, string $tag = 'run', array &$params = [])
    {
        APP_DEBUG && Debug::remark('behavior_start', 'time');

        if ($class instanceof \Closure) {
            call_user_func_array($class, $params);
            $class = 'Closure';
        } else {
            $obj = new $class();
            if (is_callable([$obj, 'run'])) {
                $obj->run($params);
            }
        }

        if (APP_DEBUG) {
            Debug::remark('behavior_end', 'time');
            Logger::debug('[ BEHAVIOR ] Run ' . $class . ' @' . $tag . ' [ RunTime:' . Debug::getRangeTime('behavior_start', 'behavior_end') . 's ]');
        }
    }

}
