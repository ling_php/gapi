<?php
namespace gapi\database;


/**
 * Class mysqliDriver
 */
class Mysqli extends Database
{
    /**
     * @var \mysqli
     */
    public \mysqli $mysqli;

    /**
     * @param array $database
     */
    public function _connect(array $database): void
    {
        //=====使用长连接
        $mysqli = new \mysqli($database['hostname'], $database['username'], $database['password'],$database['database'], $database['hostport']);
        if ($mysqli->connect_error) {
            new \ErrorException('连接数据库失败：' . $mysqli->connect_error);
        } else {
            $this->mysqli = $mysqli;
            $this->mysqli->set_charset($database['charset']);
        }
    }

    /**
     * @param $string
     *
     * @return string
     */
    public function quote($string): string
    {
        $string = mysqli_real_escape_string($this->mysqli, $string);
        if (is_numeric($string)) {
            return $string;
        } else {
            return '\'' . $string . '\'';
        }
    }

    /**
     * 返回错误信息
     * @return string
     */
    public function getError(): string
    {
        return $this->mysqli->error;
    }

    /**
     * 数据库驱动必须创建下列方法
     * 并且必须返回正确的值
     * @param string|Query $sql
     * @return mixed
     * @throws \ErrorException
     */
    public function query(string|Query $sql): mixed
    {
        $query = $this->mysqli->query($sql);
        $result = [];
        if ($query) {
            while ($row = $query->fetch_assoc()) {
                $data = $row;
                $result[] = $data;
                unset($data);
            }
            unset($query);
            return $result;
        }else{
            throw new \ErrorException($this->getError());
        }
        return $result;
    }

    /**
     * @return bool
     */
    public function close(): bool
    {
        return mysqli_close($this->mysqli);
    }

    /**
     * @param string|Query $sql
     * @return mixed
     */
    public function exec(string|Query $sql): mixed
    {
        $query = $this->mysqli->query($sql);
        if ($query) {
            return $query;
        }else{
            throw new \ErrorException($this->getError());
        }
    }

    /**
     * @return bool
     */
    public function rollBack(): bool
    {
        return $this->mysqli->rollback();
    }

    /**
     * @return bool
     */
    public function commit(): bool
    {
        return $this->mysqli->commit();
    }

    /**
     * @return bool
     */
    public function startTrans(): bool
    {
        return $this->mysqli->autocommit(FALSE);
    }
}