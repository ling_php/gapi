<?php

namespace gapi\database;


/**
 * Class PdoDriver
 */
class PdoSqlite extends Database
{

    private \PDO $db;
    private string $configName = 'default';

    /**
     * @param array $database
     */
    public function _connect(array $database): void
    {
        $this->db = new \PDO('sqlite:' . $database['filename'], null, null,
            [
                \PDO::ATTR_CASE => \PDO::CASE_NATURAL,
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_ORACLE_NULLS => \PDO::NULL_NATURAL,
                \PDO::ATTR_STRINGIFY_FETCHES => false,
                \PDO::ATTR_EMULATE_PREPARES => false,
            ]
        );
        //$this->exec('set names ' . $database['charset']);
    }

    /**
     * 返回错误信息
     *
     * @return string
     */
    public function getError(): string
    {
        return implode(' | ', $this->db->errorInfo());
    }

    /**
     * 数据库驱动必须创建下列方法
     * 并且必须返回正确的值
     * @param string|Query $sql
     * @return array|Query
     * @throws \ErrorException
     */
    public function query(string|Query $sql): array|Query
    {
        $query = $this->db->query($sql);

        if ($query) {
            $result = [];
            $data = $query->fetchAll(\PDO::FETCH_ASSOC);   //只获取键值
            foreach ($data as &$item) {
                $result[] = $item;
            }
            return $result;
        }
        unset($query);

        return [];

    }

    /**
     * @param $string
     *
     * @return string
     */
    public function quote($string): string
    {
        return $this->db->quote($string);
    }

    /**
     * @param string|Query $sql
     * @return mixed
     */
    public function exec(string|Query $sql): mixed
    {
        return $this->db->exec($sql);
    }

    /**
     * @return bool
     */
    public function startTrans(): bool
    {
        return $this->db->beginTransaction();
    }

    /**
     * @return bool
     */
    public function commit(): bool
    {
        return $this->db->commit();
    }

    /**
     * @return bool
     */
    public function rollBack(): bool
    {
        return $this->db->rollBack();
    }

    /**
     *
     */
    public function close()
    {

    }
}