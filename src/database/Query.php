<?php

namespace gapi\database;

class Query
{
    public string $sql = '';
    public string $tableName = '';
    public string $alias = '';

    // SQL表达式
    protected string $selectSql = 'SELECT%DISTINCT% %FIELD% FROM %TABLE%%FORCE%%JOIN%%WHERE%%GROUP%%HAVING%%ORDER%%LIMIT% %UNION%%LOCK%%COMMENT%';
    protected string $insertSql = '%INSERT% INTO %TABLE% (%FIELD%) VALUES (%DATA%) %COMMENT%';
    protected string $insertAllSql = '%INSERT% INTO %TABLE% (%FIELD%) VALUES %DATA% %COMMENT%';
    protected string $updateSql = 'UPDATE %TABLE% SET %SET% %JOIN% %WHERE% %ORDER%%LIMIT% %LOCK%%COMMENT%';
    protected string $deleteSql = 'DELETE FROM %TABLE%%USING%%JOIN%%WHERE%%ORDER%%LIMIT%%LOCK%%COMMENT%';

    /**
     * @var array|string[]
     */
    public array $section = [
        'select' => '',
        'distinct' => '',
        'field' => '',
        'where' => '',
        'join' => '',
        'group' => '',
        'having' => '',
        'order' => '',
        'limit' => '',
        'union' => '',
        'lock' => '',
        'comment' => '',
        'force' => '',
        'insert' => '',
        'set' => '',
        'data' => '',
        'using' => '',
        'page' => '1',
        'pagesize' => '',
    ];

    public function __contruct()
    {

    }

    public function columns(string $table = '', string $prefix = '', string $full = ''): string|array
    {
        return "SHOW {$full} COLUMNS from {$prefix}{$table};";
    }

    public function tables(string $table = ''): string|array
    {
        $table = $table == '' ? '' : " where Name='{$table}'";
        return "SHOW TABLE STATUS{$table};";
    }

    public function tableinfo(string $table = '', string $prefix = ''): string|array
    {
        return "show table status like '{$prefix}{$table}'";
    }

    public function group(string $group): self
    {
        $this->section['group'] = " group by {$group}";
        return $this;
    }

    public function distinct(): self
    {
        $this->section['distinct'] = " distinct";
        return $this;
    }

    //联合
    public function union(): self
    {
        $this->section['union'] = ' union all';
        return $this;
    }

    public function innerJoin(string $table, string $on1, string $on2, string $on = ''): self
    {
        $table = $this->prefixTable($table);
        $on = $on == '' ? "{$on1}={$on2}" : $on;
        $this->section['join'] = " inner join {$table} on {$on}";
        return $this;
    }

    public function fullJoin(string $table, string $on1, string $on2, string $on = ''): self
    {
        $table = $this->prefixTable($table);
        $on = $on == '' ? "{$on1}={$on2}" : $on;
        $this->section['join'] = " full join {$table} on {$on}";
        return $this;
    }

    public function rightJoin(string $table, string $on1, string $on2, string $on = ''): self
    {
        $table = $this->prefixTable($table);
        $on = $on == '' ? "{$on1}={$on2}" : $on;
        $this->section['join'] = " right join {$table} on {$on}";
        return $this;
    }

    public function leftJoin(string $table, string $on1, string $on2, string $on = ''): self
    {
        $table = $this->prefixTable($table);
        $on = $on == '' ? "{$on1}={$on2}" : $on;
        $this->section['join'] = " left join {$table} on {$on}";
        return $this;
    }

    public function join(string $table, string $on1, string $on2, string $on = ''): self
    {
        $table = $this->prefixTable($table);
        $on = $on == '' ? "{$on1}={$on2}" : $on;
        $this->section['join'] = " join {$table} on {$on}";
        return $this;
    }

    public function prefixTable(string $table): string
    {
        return $table;
    }

    public function getTable(): string
    {
        return $this->table;
    }

    public function getTableName(): string
    {
        return $this->tableName;
    }


    // - 减
    public function setDec(string $field, int $num = 1)
    {
        $this->section['set'] = "$field={$field}+{$num}";
    }

    // + 加
    public function setInc(string $field, int $num = 1)
    {
        $this->section['set'] = "$field={$field}-{$num}";
    }

    public function table(string $table, string $prefix = '', string $as = ''): self
    {
        $this->tableName = $table;
        $this->table = $prefix . $table . ($as == '' ? '' : " as {$as}");
        return $this;
    }

    /**
     * 组合where语句
     * @param string|array $where
     * @param string $type where() 和 where() 之间 and/or
     * @param string $field_type
     * @return $this
     */
    public function where(string|array $where, string $type = 'and', string $field_type = 'and'): self
    {

        $this->section['where'] .= $this->section['where'] == '' ? ' where ' : " {$type} ";

        if (is_string($where)) {
            $this->section['where'] .= "{$where}";
        } else {
            $where_sql = [];
            foreach ($where as $field => $option) {
                if (is_array($option)) {
                    $where_sql[] = $this->parseWhereOption($field, $option);
                } else {
                    $where_sql[] = $field . '=' . $this->parseValue($option);
                }
            }
            $this->section['where'] .= "(" . implode(" {$field_type} ", $where_sql) . ")";
        }
        return $this;
    }

    public function parseWhereOption(string $field, array $option): string
    {
        # > >= <> <= < like notlike in notin between notbetween is not null is null
        $exp = $option[0];
        $value = isset($option[1]) ? $option[1] : '';
        $whereStr = '';

        if (in_array($exp, ['=', '<>', '>', '>=', '<', '<='])) {
            // 比较运算
            $whereStr .= $field . ' ' . $exp . ' ' . $this->parseValue($value);
        } elseif (in_array($exp, ['in', 'not in'])) {
            if (is_array($value)) {
                $whereStr .= $field . ' ' . $exp . ' (' . "'" . implode("','", $value) . "'" . ')';
            } else {
                $whereStr .= $field . ' ' . $exp . ' (' . $value . ')';
            }

        } elseif (in_array($exp, ['between', 'notbetween'])) {
            if (is_array($value)) {
                $whereStr .= $field . ' ' . $exp . ' ' . implode(' and ', $value);
            } else {
                $whereStr .= $field . ' ' . $exp . ' ' . implode(' and ', explode(',', $value));
            }
        } elseif (in_array($exp, ['like', 'not like'])) {
            $liketype = isset($option[2]) ? $option[2] : 'both';
            $whereStr = match ($liketype) {
                'both' => $field . ' ' . $exp . ' ' . "'%{$value}%'",
                'left' => $field . ' ' . $exp . ' ' . "'%{$value}'",
                'right' => $field . ' ' . $exp . ' ' . "'{$value}%'",
            };
        } elseif (in_array($exp, ['is null', 'is not null'])) {
            $whereStr = match ($exp) {
                'is null', 'is not null' => $field . ' ' . $exp,
            };
        }
        return $whereStr;
    }


    public function parseValue(string|array $value): string|array
    {
        if (is_string($value)) {
            $value = addslashes($value);
        } elseif (is_array($value)) {
            $value = array_map([$this, 'parseValue'], $value);
        } elseif (is_bool($value)) {
            $value = $value ? '1' : '0';
        } elseif (is_null($value)) {
            $value = 'null';
        }
        return addslashes($value);
    }

    public function limit(string $limit = ''): self
    {
        if ($limit != '') {
            $this->section['limit'] = " limit {$limit}";
        }
        return $this;
    }

    public function order(string $order = ''): self
    {
        if ($order != '') {
            $this->section['order'] = " order by {$order}";
        }
        return $this;
    }

    public function find(): mixed
    {
        $this->section['limit'] = ' limit 1';
        $this->section['field'] = $this->section['field'] == '' ? '*' : $this->section['field'];

        $this->sql = str_replace(
            ['%TABLE%', '%DISTINCT%', '%FIELD%', '%JOIN%', '%WHERE%', '%GROUP%', '%HAVING%', '%ORDER%', '%LIMIT%', '%UNION%', '%LOCK%', '%COMMENT%', '%FORCE%'],
            [
                $this->table . $this->alias,
                $this->section['distinct'],
                $this->section['field'],
                $this->section['join'],
                $this->section['where'],
                $this->section['group'],
                $this->section['having'],
                $this->section['order'],
                $this->section['limit'],
                $this->section['union'],
                $this->section['lock'],
                $this->section['comment'],
                $this->section['force'],
            ],
            $this->selectSql
        );
        $this->reset();
        return $this;
    }

    /**
     * 生成select语句
     * @return mixed
     */
    public function select(): mixed
    {
        $this->section['field'] = $this->section['field'] == '' ? '*' : $this->section['field'];

        if ($this->section['pagesize'] != '') {
            $this->section['limit'] = ' limit ' . ($this->section['page'] - 1) * $this->section['pagesize'] . ',' . $this->section['pagesize'];
        }

        $this->sql = str_replace(
            ['%TABLE%', '%DISTINCT%', '%FIELD%', '%JOIN%', '%WHERE%', '%GROUP%', '%HAVING%', '%ORDER%', '%LIMIT%', '%UNION%', '%LOCK%', '%COMMENT%', '%FORCE%'],
            [
                $this->table . $this->alias,
                $this->section['distinct'],
                $this->section['field'],
                $this->section['join'],
                $this->section['where'],
                $this->section['group'],
                $this->section['having'],
                $this->section['order'],
                $this->section['limit'],
                $this->section['union'],
                $this->section['lock'],
                $this->section['comment'],
                $this->section['force'],
            ],
            $this->selectSql
        );
        $this->reset();
        return $this;
    }

    /**
     * 生成insert语句
     * @param array $data
     * @param bool $replace
     * @param array $option
     * @return mixed
     */
    public function insert(array $data = [], bool $replace = false, array $option = []): mixed
    {
        $this->section['insert'] = $replace ? 'REPLACE' : 'INSERT';
        $this->section['data'] = $this->parseData($data, $option);
        $this->sql = str_replace(
            ['%INSERT%', '%TABLE%', '%FIELD%', '%DATA%', '%COMMENT%'],
            [
                $this->section['insert'],
                $this->table,
                $this->section['field'],
                $this->section['data'],
                $this->section['comment'],
            ], $this->insertSql);
        $this->reset();
        return $this;
    }

    public function parseData(array $data, array $options = []): string
    {
        $values = [];
        $fields = [];
        foreach ($data as $field => $value) {
            $values[] = "'" . addslashes($value) . "'";
            $fields[] = "`{$field}`";
        }
        $this->section['field'] = implode(',', $fields);
        return implode(',', $values);
    }

    /**
     * 生成insertAll语句
     * @param array $dataSet
     * @param array $options
     * @param bool $replace
     * @return mixed
     */
    public function insertAll(array $dataSet, array $options = [], bool $replace = false): mixed
    {
        $this->section['insert'] = $replace ? 'REPLACE' : 'INSERT';
        $fields = [];
        foreach ($dataSet as $key => $data) {
            $values = [];
            foreach ($data as $field => $value) {
                $values[] = $this->parseValue($value);
                if ($key == 0) {
                    $fields[] = $field;
                }
            }
            $datas[] = "(" . implode(",", $values) . ")";
        }
        $this->section['field'] = implode(',', $fields);
        $this->section['data'] = implode(',', $datas);
        $this->sql = str_replace(
            ['%INSERT%', '%TABLE%', '%FIELD%', '%DATA%', '%COMMENT%'],
            [
                $this->section['insert'],
                $this->table,
                $this->section['field'],
                $this->section['data'],
                $this->section['comment'],
            ], $this->insertAllSql);
        $this->reset();
        return $this;
    }


    /**
     * 生成updateSql
     * @param array $data
     * @param array $options
     * @param false $flag 确定不需要条件 直接更新
     * @return mixed
     */
    public function update(array $data, array $options = [], $flag = false): mixed
    {
        $this->sql = 'update';

        $this->section['set'] = implode(',', $this->parseUpdateData($data));
        if ($options) {
            $this->section['where'] = $this->section['where'] == '' ? ' where ' . implode(' and ', $this->parseUpdateData($options)) : implode(' and ', $this->parseUpdateData($options));
        }

        if ($this->section['where'] == '' && $flag == false) {
            throw new \ErrorException(__FUNCTION__ . ':操作无条件限制，必须操作，请设置flag:true');
        }

        $this->sql = str_replace(
            ['%TABLE%', '%SET%', '%JOIN%', '%WHERE%', '%ORDER%', '%LIMIT%', '%LOCK%', '%COMMENT%'],
            [
                $this->table,
                $this->section['set'],
                $this->section['join'],
                $this->section['where'],
                $this->section['order'],
                $this->section['limit'],
                $this->section['lock'],
                $this->section['comment'],
            ], $this->updateSql);
        $this->reset();
        return $this;
    }

    public function parseUpdateData(array $data, array $options = []): array
    {
        $values = [];
        foreach ($data as $field => $value) {
            if (is_array($value)) {
                $values[] = $field . '=' . "`{$value[0]}` {$value[1]}";
            } else {
                $values[] = $field . '=' . "'{$value}'";
            }
        }
        return $values;
    }

    /**
     * 生成delete语句
     * @param $options
     * @return mixed
     */
    public function delete(array $options = [], bool $flag = false): mixed
    {
        if ($options) {
            $this->section['where'] = $this->section['where'] == '' ? ' where ' . implode(' and ', $this->parseUpdateData($options)) : implode(' and ', $this->parseUpdateData($options));
        }

        if ($this->section['where'] == '' && $flag == false) {
            throw new \ErrorException(__FUNCTION__ . ':操作无条件限制，必须操作，请设置flag:true');
        }

        $this->sql = str_replace(
            ['%TABLE%', '%USING%', '%JOIN%', '%WHERE%', '%ORDER%', '%LIMIT%', '%LOCK%', '%COMMENT%'],
            [
                $this->table,
                $this->section['using'],
                $this->section['join'],
                $this->section['where'],
                $this->section['order'],
                $this->section['limit'],
                $this->section['lock'],
                $this->section['comment'],
            ], $this->deleteSql);
        $this->reset();
        return $this;
    }

    public function force(string $index): self
    {
        $this->section['force'] = sprintf(" FORCE INDEX ( %s ) ", $index);
        return $this;
    }

    public function having(string $field): self
    {
        $this->section['having'] = 'having ' . $field;
        return $this;
    }

    public function using(string $field): self
    {
        $this->section['using'] = "using ({$field})";
        return $this;
    }

    // LOCK TABLES tbl_name[[AS] alias] lock_type [, tbl_name [[AS] alias] lock_type]
    // write / read
    public function lock(string $lock = 'write'): string
    {
        return "lock table {$this->table} {$lock};";
    }

    //UNLOCK TABLES
    public function unlock(string $table): string
    {
        return "unlock table;";
    }


    public function comment(string $comment): self
    {
        $this->section['comment'] = "/**{$comment}*/";
        return $this;
    }

    public function field(): self
    {
        $args = func_get_args();
        if (func_num_args() == 1) {
            $field = $args[0];
            $this->section['field'] = $field;
        } else {
            $field = '`' . implode('`,`', $args) . '`';
            $this->section['field'] = $field;
        }
        return $this;
    }


    public function sum(string $field, string $as = ''): mixed
    {
        $as = $as == '' ? $field : $as;
        $this->section['field'] = $as == '' ? "sum($field)" : "sum($field) as {$as}";
        return $this;
    }

    public function count(string $field = '*', string $as = ''): mixed
    {
        $as = $as == '' ? $field : $as;
        $this->section['field'] = $as == '' ? "count($field)" : "count($field) as {$as}";
        return $this;
    }


    public function min(string $field, string $as = ''): mixed
    {
        $as = $as == '' ? $field : $as;
        $this->section['field'] = $as == '' ? "min($field)" : "min($field) as {$as}";
        return $this;
    }

    public function max(string $field, string $as = ''): mixed
    {
        $as = $as == '' ? $field : $as;
        $this->section['field'] = $as == '' ? "max($field)" : "max($field) as {$as}";
        return $this;
    }

    public function avg(string $field, string $as = ''): mixed
    {
        $as = $as == '' ? $field : $as;
        $this->section['field'] = $as == '' ? "avg($field)" : "avg($field) as {$as}";
        return $this;
    }

    public function version(): string
    {
        return 'SELECT VERSION()';
    }

    public function lastInsertId(): string
    {
        return 'SELECT LAST_INSERT_ID() as id';
    }

    public function truncate()
    {
        return "truncate table " . $this->table;
    }

    public function __toString()
    {
        return $this->sql;
    }

    public function reset()
    {
        foreach ($this->section as $k => $v) {
            $this->section[$k] = '';
        }
    }


}