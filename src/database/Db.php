<?php

namespace gapi\database;

use gapi\Config;
use gapi\lib\Logger;
use gapi\Loader;

class Db extends Database
{
    private static mixed $db = null;
    private static mixed $instance = null;


    private function __construct()
    {

    }

    public function columns(string $table = '', string $prefix = '', string $full = ''): array
    {

        //生成字段模型
        if (!is_dir(COLUMNS_PATH)) {
            mkdir(COLUMNS_PATH, 0777);
        }
        $columns_file = COLUMNS_PATH_NAME . DS . "t_{$table}.php";
        $version_columns_file = Loader::versionfile($columns_file);

        if (is_file($version_columns_file) && $full == '') {
            $class = "\\app\\columns\\t_{$table}";
            return $class::$fields;
        }

        $prefix = $prefix == '' ? $this->config['prefix'] : $prefix;
        $fields = $this->query((new Query())->columns($table, $prefix, $full));

        if ($full == 'full') {
            return $fields;
        }
        $content = [];
        if ($fields) {
            foreach ($fields as $field) {
                $content[] = $field['Field'];
            }
        }
        return $content;
    }

    public function formatColumns(string $table, array $fields, array $tableinfo): string
    {
        if (!is_dir(COLUMNS_PATH)) {
            mkdir(COLUMNS_PATH, 0777);
        }
        $columns_file = COLUMNS_PATH . DS . "t_{$table}.php";
        file_put_contents($columns_file, $this->parseColumns($table, $fields, $tableinfo));
        return $columns_file;
    }


    public function parseColumns(string $table, array $fields, array $tableinfo): string
    {

        $fields_strs = [];
        $fields_contents = [];
        $pk = '';
        $pk_comment = '';
        if ($fields) {
            foreach ($fields as $key => $value) {
                $comment = $value['Comment'];
                unset($value['Comment']);
                unset($value['Privileges']);
                $field_info = json_encode($value);

                $upper_filed = strtoupper($value['Field']);
                $fields_strs[] = "'" . $value['Field'] . "'";
                $fields_contents[] = <<<DDL

    /**
    * @var {$comment}
    * {$field_info}
    */
    public const {$upper_filed} = '{$value['Field']}';
DDL;

                if ($value['Key'] == 'PRI') {
                    $pk = $value['Field'];
                    $pk_comment = $comment;
                }
            }
        }


        $fields_content = implode("\n", $fields_contents);
        $fields_str = 'public static $fields = [' . implode(',', $fields_strs) . '];';
        $pk_str = 'public const PRIMARY_PK = \'' . $pk . "';";
        $content = <<<DDL
<?php
namespace app\\columns;
/**
 * Class t_{$table}
 * @package columns
 */
class t_{$table}{

    /**
    * @var {$tableinfo['Comment']}
    */
    public const TABLE_NAME = '{$table}';    
    
    /**
    * @var {$tableinfo['Comment']}
    */
    public const FULL_TABLE_NAME = '{$tableinfo['Name']}';
    {$fields_content}
    
    /* fields array */
    {$fields_str}
    
    /**
    * @var {$pk_comment}
    */
    {$pk_str}
    
}
DDL;
        return $content;
    }

    public function pk(string $table): string
    {
        $fields = $this->query((new Query())->columns($table, $this->config['prefix']));
        $pk = '';
        if ($fields) {
            foreach ($fields as $field) {
                if ($field['Key'] == 'PRI') {
                    $pk = $field['Field'];
                }
            }
        }
        return $pk;
    }

    /**
     * @return string|null
     */
    public function version(): string|null
    {
        $version = $this->query((new Query())->version());
        return $version ? $version[0]['VERSION()'] : NULL;
    }

    /**
     * 数据库驱动必须创建下列方法
     * 并且必须返回正确的值
     * @param string|Query $sql
     * @return mixed
     * @throws \ErrorException
     */
    public function query(string|Query $sql): mixed
    {
        try {
            Logger::sql($sql);
            return self::$db->query($sql);
        } catch (\Exception) {
            throw new \ErrorException($this->getError());
        }
        return [];
    }


    /**
     * @param string|Query $sql
     * @return mixed
     * @throws \ErrorException
     */
    public function exec(string|Query $sql): mixed
    {
        try {
            Logger::sql($sql);
            return self::$db->exec($sql);
        } catch (\Exception) {
            throw new \ErrorException($this->getError());
        }
        return null;
    }

    public function getError(): string
    {
        return self::$db->getError();
    }

    public function quote($string): string
    {
        return self::$db->quote($string);
    }


    public static function driver(string $driver): Database
    {
        if (!(self::$db instanceof Database)) {
            try {
                $driver = "\\gapi\\database\\" . $driver;
                self::$db = new $driver();
            } catch (\ErrorException $e) {
                Logger::error($e->getMessage());
            }
        }
        return self::$db;
    }

    public static function connect($database): Database
    {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self();
            self::$instance->config = $database;
            self::driver($database['type'])->_connect($database);
        }
        return self::$instance;
    }

    public function startTrans(): bool
    {
        return self::$db->startTrans();
    }

    public function commit(): bool
    {
        return self::$db->commit();
    }

    public function rollBack(): bool
    {
        return self::$db->rollBack();
    }


}