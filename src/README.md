# LingCMS内容管理系统

----

`composer update`

`composer dev`

## 描述

gapi-多版本管理api框架

## 安装 Composer

框架使用 Composer 来管理其依赖性。所以，在你使用之前，你必须确认在你电脑上是否安装了 Composer。 如果未安装请前往 [https://getcomposer.org/](https://getcomposer.org/)
下载安装。

## composer地址

https://packagist.org/packages/gapi/project

## 创建项目

通过 composer 快速创建一个项目

```
composer create-project gapi/project project_dir

或者

在composer.json中 
"require": {
    "php": ">=8.0",
    "gapi/project": "dev-master"
}
```

## 运行项目

```
composer serve
```

## 环境需求

PHP 版本 >= 8.0

## 命令

### 生成路由

```
php build route vx.x.x[v1.0.0]

php build auto route
```

### 生成字段列表

```
php build fields vx.x.x

php build auto fields
```

### 清理runtime

```
php build clear
```

### 全版本复制 生成完整的最新的版本

```
php build version vx.x.x[v1.0.1] [full]
```

### 生成表

```
php build table admin[模块名] v1.0.0[版本号]
```

### 安装i5ting_toc

```
npm install -g i5ting_toc
```

### README.md 生成 html

```
#https://www.runoob.com/markdown/md-code.html
i5ting_toc -f README.md -o
```

### 生成默认数据

```
php build CreateData test ./plugins/locoy_plugin
```