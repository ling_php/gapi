<?php
namespace gapi\enum;

class RequestMethod{
    /**
     * @var Get类型
     */
    public const MEHTOD_GET = 'get';
    /**
     * @var Post类型
     */
    public const MEHTOD_POST = 'post';
    /**
     * @var Get|Post类型
     */
    public const MEHTOD_ALL = 'all';
    /**
     * @var Request类型
     */
    public const MEHTOD_REQUEST = 'request';
    /**
     * @var Head类型
     */
    public const MEHTOD_HEAD = 'head';

    /**
     * @var Delete类型
     */
    public const MEHTOD_DELETE = 'delete';

    /**
     * @var Put类型
     */
    public const MEHTOD_PUT = 'put';

    /**
     * @var Options类型
     */
    public const MEHTOD_OPTIONS = 'options';

}