<?php

namespace gapi;

use gapi\lib\Logger;

class Response
{
    // string | json
    private string $type = 'string';
    private array $section = [
        'status' => 1,
        'code' => 200,
        'data' => [],
        'msg' => '',
    ];
    private array $extra = [];
    private mixed $view = null;
    private static $instance = null;

    public static function instance($extra = [])
    {
        if (self::$instance == null) {
            self::$instance = new self($extra);
        } else {
            self::$instance->setExtra($extra);
        }
        return self::$instance;
    }


    public function __construct($extra = [])
    {
        $this->extra = $extra;

        if (Config::file()['js_allow_origin']) {
            self::cors();
        }
        $this->type = Config::system()['default_return'];
    }

    /**
     * @param string $msg
     * @return Response
     */
    public function setMsg(string $msg): self
    {
        $this->section['msg'] = $msg;
        return $this;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function setData(string|array $data, bool|string $key = 'data'): self
    {
        if ($key === false) {
            $this->section = array_merge($this->section, $data);
        } else {
            $this->section[$key] = $data;
        }
        return $this;
    }

    /**
     * @param int $code
     * @return $this
     */
    public function setCode(int $code): self
    {

        $this->section['code'] = $code;
        switch ($code) {
            case 301:
                header('HTTP/1.1 301 Moved Permanently');
                break;
            case 401:
                header('HTTP/1.1 401 Unauthorized');
                header('WWW-Authenticate: Basic realm="登录信息"');
                break;
            case 403:
                header('HTTP/1.1 403 Forbidden');
                break;
            case 404:
                header("HTTP/1.1 404 Not Found");
                header("Status: 404 Not Found");
                break;
            case 500:
                header('HTTP/1.1 500 Internal Server Error');
                break;
            default:
                break;
        }
        return $this;
    }

    public function redirect(string $url)
    {
        header("Location:{$url}");
    }

    /**
     * @param array $extra
     * @return Response
     */
    public function setExtra(array $extra): self
    {
        $this->extra = $extra;
        return $this;
    }

    public function success(): self
    {
        $this->section['status'] = 1;
        return $this;
    }

    public function fail(): self
    {
        $this->section['status'] = 0;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getData(): mixed
    {
        return $this->section['data'];
    }

    /**
     * @return array
     */
    public function getExtra(): array
    {
        return $this->section['extra'];
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->section['code'];
    }

    /**
     * @return string
     */
    public function getMsg(): string
    {
        return $this->section['msg'];
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->section['status'];
    }

    public function json(): self
    {
        $this->type = 'json';
        return $this;
    }

    public function string(): self
    {
        $this->type = 'string';
        return $this;
    }

    public function xml(): self
    {
        $this->type = 'xml';
        return $this;
    }


    public function setHeader($content): self
    {
        if (!is_cli()) {
            header($content);
        }
        return $this;
    }

    public function assign(string $key, mixed $value): self
    {
        View::instance()->assign($key, $value);
        return $this;
    }


    public function view(string $file = '', array $data = [], bool $app = true, string $suffix = '.html'): mixed
    {
        $this->type = 'view';
        View::instance()->setFile($file)->setApp($app)->setData($data)->setSuffix($suffix);
        //错误页面
        if ($this->section['status'] == 0) {
            View::instance()->assign('msg', $this->section['msg']);
            View::instance()->setFile('admin/tips/error');
        }
        return $this;
    }

    /**
     * @return mixed
     * @throws \JsonException
     */
    public function send(): mixed
    {
        switch ($this->type) {
            case 'string':
                echo $this->section['msg'];
                break;
            case 'json':
                $this->setHeader('Content-Type:application/json');
                $this->section = $this->extra ? array_merge($this->section, $this->extra) : $this->section;
                echo (new Data($this->section))->toJson();
                break;
            case 'xml':
                $this->setHeader('Content-type: text/xml');
                echo '<?xml version="1.0" encoding="UTF-8"?><root>' . $this->section['msg'] . '</root>';
                break;
            case 'view':
                echo View::instance()->setData($this->section['data'])->fetch();
                break;
            default:
                break;
        }
        Hook::listen('app_end');
        if (APP_DEBUG) {
            Logger::info("\n data => " . var_export($this->section, 1));
            Debug::end();
        }
        exit;
    }

    /**
     * js 跨域配置
     */
    public static function cors(): void
    {
        $origin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : '';
        $allow_origin = explode(',', Config::file()['allow_origin']);
        if (in_array($origin, $allow_origin)) {
            header('Access-Control-Allow-Origin:' . $origin);
        }
        header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization, Token");
        header('Access-Control-Max-Age: 1728000');
        header('Access-Control-Allow-Credentials: true');
    }

    public function __toString(): string
    {
        switch ($this->type) {
            case 'string':
                return $this->section['msg'];
            case 'json':
                $this->setHeader('Content-Type:application/json');
                $data = $this->extra ? array_merge($this->section, $this->extra) : $this->section;
                return (new Data($data))->toJson();
            case 'xml':
                $this->setHeader('Content-type: text/xml');
                return '<?xml version="1.0" encoding="UTF-8"?><root>' . $this->section['msg'] . '</root>';
            case 'view':
                return View::instance()->setData($this->section['data'])->fetch();
            default:
                break;
        }
        return '';
    }
}