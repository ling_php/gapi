<?php

namespace gapi;

class Config
{

    public static array $config = [];
    private static $range = 'sys';

    // 设定配置参数的作用域
    public static function range(string $range): void
    {
        self::$range = $range;
        if (!isset(self::$config[$range])) {
            self::$config[$range] = [];
        }
    }

    /**
     * 解析配置文件或内容
     * @param string $config 配置文件路径或内容
     * @param string $type 配置解析类型
     * @param string $name 配置名（如设置即表示二级配置）
     * @param string $range 作用域
     * @return mixed
     */
    public static function parse(string $config, string $type = '', string $name = '', string $range = ''): mixed
    {
        $range = $range ?: self::$range;
        if (empty($type)) {
            $type = pathinfo($config, PATHINFO_EXTENSION);
        }
        $class = str_contains($type, '\\') ? $type : '\\gapi\\config\\' . ucwords($type);
        return self::set($class::parse($config), $name, $range);
    }

    public static function file(string $file = 'config.php', string $name = 'app'): mixed
    {
        $config_file = Loader::versionfile(file: $file);

        if ($config_file!='') {
            if(!isset(self::$config[$name])){
                self::$config[$name] = include $config_file;
            }
            return self::$config[$name];
        }else{
            return [];
        }
    }

    public static function system(string $file = 'config.php'): mixed
    {
        $config_file = Loader::systemfile(file: $file);
        if (!isset(self::$config[self::$range])) {
            self::$config[self::$range] = include $config_file;
        }
        return self::$config[self::$range];
    }

    /**
     * 加载模块配置 覆盖 range
     * @param string $file
     * @param string $range
     */
    public static function loadModule(string $file = 'config.php', string $range = '', string $module = ''): void
    {
        $range = $range == '' ? self::$range : $range;
        $module_config_file = VERSION_PATH . DS . ($module == '' ? MODULE_NAME : $module) . DS . $file;
        if (is_file($module_config_file)) {
            $module_config = include $module_config_file;
            if ($module_config) {
                foreach ($module_config as $key => $value) {
                    self::$config[$range][$key] = $value;
                }
            }
        }
    }

    /**
     * 获取模块配置
     * @param string $file
     * @return array
     */
    public static function getModule(string $file = 'config.php', string $module = ''): array
    {
        $module_config_file = VERSION_PATH . DS . ($module == '' ? MODULE_NAME : $module) . DS . $file;
        if (is_file($module_config_file)) {
            return include $module_config_file;
        }
        return [];
    }

    /**
     * 加载配置文件（PHP格式）
     * @param string $file
     * @param string $name
     * @param string $range
     * @return mixed
     */
    public static function load(string $file, string $name = '', string $range = ''): mixed
    {
        $range = $range ?: self::$range;
        if (!isset(self::$config[$range])) {
            self::$config[$range] = [];
        }
        if (is_file($file)) {
            $name = strtolower($name);
            $type = pathinfo($file, PATHINFO_EXTENSION);
            if ('php' == $type) {
                return self::set(include $file, $name, $range);
            } elseif ('yaml' == $type && function_exists('yaml_parse_file')) {
                return self::set(yaml_parse_file($file), $name, $range);
            } else {
                return self::parse($file, $type, $name, $range);
            }
        } else {
            return self::$config[$range];
        }
    }

    /**
     * 检测配置是否存在
     * @param string $name 配置参数名（支持二级配置 .号分割）
     * @param string $range 作用域
     * @return bool
     */
    public static function has(string $name, string $range = ''): bool
    {
        $range = $range ?: self::$range;

        if (!strpos($name, '.')) {
            return isset(self::$config[$range][strtolower($name)]);
        } else {
            // 二维数组设置和获取支持
            $name = explode('.', $name, 2);
            return isset(self::$config[$range][strtolower($name[0])][$name[1]]);
        }
    }

    /**
     * 获取配置参数 为空则获取所有配置
     * @param string $name 配置参数名（支持二级配置 .号分割）
     * @param string $range 作用域
     * @return mixed
     */
    public static function get(?string $name = null, string $range = ''): mixed
    {

        $range = $range ?: self::$range;
        // 无参数时获取所有
        if (empty($name) && isset(self::$config[$range])) {
            return self::$config[$range];
        }

        if (empty($name) && $range == '') {
            return self::$config;
        }

        if ($range == '') {
            if (strpos($name, '.')) {
                // 二维数组设置和获取支持
                $name = explode('.', $name, 2);
                $name[0] = strtolower($name[0]);
                return isset(self::$config[$name[0]][$name[1]]) ? self::$config[$name[0]][$name[1]] : null;
            }
        }

        if (!strpos($name, '.')) {
            $name = strtolower($name);
            return isset(self::$config[$range][$name]) ? self::$config[$range][$name] : null;
        } else {
            // 二维数组设置和获取支持
            $name = explode('.', $name, 2);
            $name[0] = strtolower($name[0]);
            return isset(self::$config[$range][$name[0]][$name[1]]) ? self::$config[$range][$name[0]][$name[1]] : null;
        }
    }

    /**
     * 设置配置参数 name为数组则为批量设置
     * @param string|array $name 配置参数名（支持二级配置 .号分割）
     * @param mixed $value 配置值
     * @param string $range 作用域
     * @return mixed
     */
    public static function set(string|array $name, mixed $value = null, string $range = ''): mixed
    {
        $range = $range ?: self::$range;
        if (!isset(self::$config[$range])) {
            self::$config[$range] = [];
        }
        if (is_string($name)) {
            if (!strpos($name, '.')) {
                self::$config[$range][strtolower($name)] = $value;
            } else {
                // 二维数组设置和获取支持
                $name = explode('.', $name, 2);
                self::$config[$range][strtolower($name[0])][$name[1]] = $value;
            }
            return null;
        } elseif (is_array($name)) {
            // 批量设置
            if (!empty($value)) {
                self::$config[$range][$value] = isset(self::$config[$range][$value]) ?
                    array_merge(self::$config[$range][$value], $name) : $name;
                return self::$config[$range][$value];
            } else {
                return self::$config[$range] = array_merge(self::$config[$range], array_change_key_case($name));
            }
        } else {
            // 为空直接返回 已有配置
            return self::$config[$range];
        }
    }

    /**
     * 重置配置参数
     */
    public static function reset(string $range = ''): void
    {
        $range = $range ?: self::$range;
        if (true === $range) {
            self::$config = [];
        } else {
            self::$config[$range] = [];
        }
    }

    /**
     * @param string $method
     * @param array $params
     * @return Router
     */
    public static function __callStatic(string $method, array $params = []): mixed
    {
        if (!method_exists(self::class, $method)) {
            $file = isset($params[0]) ? $params[0] : $method . '.php';
            return self::file($file, $method);
        }
    }
}