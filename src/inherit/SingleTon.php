<?php

namespace gapi\inherit;

class SingleTon
{
    protected static mixed $instance = null;

    protected function __construct()
    {
    }

    public static function instance(): self
    {
        if (self::$instance === null) {
            self::$instance = new static();
        }
        return self::$instance;
    }

}