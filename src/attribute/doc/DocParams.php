<?php

namespace gapi\attribute\doc;

use \Attribute;

/**
 * TARGET_CLASS    //类的注解类
 * TARGET_FUNCTION //函数注解类
 * TARGET_METHOD   //方法注解类
 * TARGET_PROPERTY //属性注解类
 * TARGET_CLASS_CONSTANT //类常量注解类
 * TARGET_PARAMETER //参数注解类
 * TARGET_ALL
 * Class MiddleWare
 * @package gapi\attribute
 */
#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD)]
class DocParams
{

    public function __construct(
        public string $name = '',
        public string $type = '',
        public mixed $value = '',
        public bool $require = true,
        public string $summary = '',
        public string $description = '',
    )
    {

    }


    public function setHandler($handler): self
    {
        $this->handler = $handler;
        return $this;
    }

    public function run(): void
    {
        call_user_func([new $this->handler->class, $this->handler->name]);
    }


    /**
     * @param string $method
     * @param array $params
     * @return static
     */
    #[Pure] public static function __callStatic(string $method, array $params = []): static
    {
        return new static($method, $params['name'], $params['type'], $params['value'], $params['require'], $params['summary'], $params['description']);
    }

}