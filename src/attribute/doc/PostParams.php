<?php

namespace gapi\attribute\doc;
use \Attribute;
/**
 * TARGET_CLASS    //类的注解类
 * TARGET_FUNCTION //函数注解类
 * TARGET_METHOD   //方法注解类
 * TARGET_PROPERTY //属性注解类
 * TARGET_CLASS_CONSTANT //类常量注解类
 * TARGET_PARAMETER //参数注解类
 * TARGET_ALL
 * Class MiddleWare
 * @package gapi\attribute
 */
#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD)]
class PostParams extends DocParams
{

}