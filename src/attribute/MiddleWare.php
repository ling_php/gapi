<?php

namespace gapi\attribute;

use \Attribute;
use gapi\Request;
use gapi\Response;

/**
 * TARGET_CLASS    //类的注解类
 * TARGET_FUNCTION //函数注解类
 * TARGET_METHOD   //方法注解类
 * TARGET_PROPERTY //属性注解类
 * TARGET_CLASS_CONSTANT //类常量注解类
 * TARGET_PARAMETER //参数注解类
 * TARGET_ALL
 * Class MiddleWare
 * @package gapi\attribute
 */
#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD)]
class MiddleWare implements AttributeInterface
{

    public $handler;

    public function __construct(
        public array $middleware = [],
    )
    {

    }

    public function setHandler($handler): self
    {
        $this->handler = $handler;
        return $this;
    }

    public function run(): void
    {
        call_user_func([new $this->handler->class, $this->handler->name]);
    }

    /**
     * @param string $method
     * @param array $params
     * @return Route
     */
    public static function __callStatic(string $method, array $params = []): static
    {
        return new static($method, $params['middleware']);
    }


    public function handle(Request $request, Response $response): void
    {
        foreach ($this->middleware as $item) {
            (new $item())->handle($request, $response);
        }
    }


}