<?php

namespace gapi\attribute;

use gapi\Request;
use gapi\Response;

interface AttributeInterface
{
    public function handle(Request $request, Response $response): void;
}