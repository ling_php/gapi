<?php

namespace gapi\attribute;

use \Attribute;
use gapi\enum\RequestMethod;
use gapi\lib\Validate;
use gapi\Request;
use gapi\Response;

/**
 * TARGET_CLASS    //类的注解类
 * TARGET_FUNCTION //函数注解类
 * TARGET_METHOD   //方法注解类
 * TARGET_PROPERTY //属性注解类
 * TARGET_CLASS_CONSTANT //类常量注解类
 * TARGET_PARAMETER //参数注解类
 * TARGET_ALL
 * Class MiddleWare
 * @package gapi\attribute
 */
#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD)]
class ValidateParams extends RequestMethod implements AttributeInterface
{

    public $handler;

    // 'page', Validate::IS_NUMBER, '分页必须是数字', ValidateParams::MEHTOD_GET
    public function __construct(
        public string $name = '',
        public string $type = '',
        public string $msg = '',
        public string $method = '',
    )
    {

    }

    public function setHandler($handler): self
    {
        $this->handler = $handler;
        return $this;
    }

    public function run(): void
    {
        call_user_func([new $this->handler->class, $this->handler->name]);
    }

    /**
     * @param string $method
     * @param array $params
     * @return static
     */
    public static function __callStatic(string $method, array $params = []): static
    {
        return new static($method, $params['name'], $params['type'], $params['msg'], $params['method']);
    }

    /**
     * params ['page', Validate::IS_NUMBER, '分页必须是数字', ValidateParams::MEHTOD_GET]
     * key,过滤类型,错误提示,请求类型
     * @param Request $request
     * @param Response $response
     */
    public function handle(Request $request, Response $response): void
    {
        $method = $this->method;
        (new Validate($request->$method($this->name)))->filter($this->type)->setMsg($this->msg);
    }


}