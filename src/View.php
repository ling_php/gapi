<?php

namespace gapi;

use gapi\lib\Logger;
use gapi\template\Driver;

class  View
{
    public string $viewPath = 'view';
    public Driver $template;
    public string $templatePath = RUNTIME_PATH . DS . 'template';
    public string $path = '';
    public string $file = '';
    public array $data = [];
    public string $suffix;
    public bool $app = true;

    private static mixed $instance = null;

    public function setTemplate(string $template = 'Ling')
    {
        $this->template = new $template();
        return $this;
    }

    private function __construct()
    {
        $this->path = VERSION_PATH;
        $template = Config::app()['template']['engine'];
        $this->template = new $template();
        $this->setViewPath();
    }

    public static function instance(): self
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function setPath(string $path)
    {
        $this->path = $path;
        return $this;
    }

    public function setViewPath(string $viewPath = 'view')
    {
        $this->viewPath = $viewPath == '' ? '' : $viewPath . DS;
        return $this;
    }

    public function setApp(bool $app = true)
    {
        $this->app = $app;
        return $this;
    }

    public function setSuffix(string $suffix = '.html')
    {
        $this->suffix = $suffix;
        return $this;
    }

    public function assign(string $key, mixed $value)
    {
        $this->data[$key] = $value;
        return $this;
    }


    public function fetch(): string
    {
        $file = $this->tplFile($this->file);
        Logger::info("Template:{$file}");
        if (!is_file($file)) {
            throw new \Exception("模板文件{$file}不存在");
        }

        $teplate_c_file = $this->templatePath . DS . md5($file) . '.php';
        $this->template->compileFile($file, $teplate_c_file);
        Hook::listen('view_filter', $this->data);
        extract($this->data);
        ob_start();
        include($teplate_c_file);
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function tplFile(string $file = ''): string
    {
        $file = $file == '' ? $this->file : $file;

        if ($this->app) {
            if ($file != '') {
                $file = $this->versionFile($file);
            } else {
                $file = $this->path . DS . MODULE_NAME . DS . $this->viewPath . strtolower(CONTROLLER_NAME) . DS . strtolower(ACTION_NAME) . $this->suffix;
            }
        }
        return $file;
    }


    public function setFile(string $file = '', bool $app = true)
    {
        $this->file = $file;
        return $this;
    }

    public function versionFile(string $file = '')
    {
        $tmp = explode('/', $file);
        if (count($tmp) == 3) {
            $module = $tmp[0];
            $path = $tmp[1] . DS . $tmp[2];
            $file = $this->path . DS . $module . DS . $this->viewPath . $path . $this->suffix;
        } else if (count($tmp) == 2) {
            $module = $tmp[0];
            $path = $tmp[1];
            $file = $this->path . DS . $module . DS . $this->viewPath . $path . $this->suffix;
        } else {
            $file = $this->path . DS . $this->viewPath . $this->file . $this->suffix;
        }
        return $file;
    }


    public function setData(array $data = [])
    {
        $this->data = $this->data + $data;
        return $this;
    }

    /**
     * 获取文件地址
     * @param string $module 模块
     * @return string
     */
    public function path(string $module = '')
    {
        $module = $module == '' ? '' : $module . DS;
        return $this->path . DS . $module . $this->viewPath;
    }


}