<?php

namespace gapi;

define('RUN_START_TIME', microtime(true));
define('RUN_START_MEM', memory_get_usage());
define('DS', DIRECTORY_SEPARATOR);
define('CORE_PATH', __DIR__);
define('ROOT_PATH', dirname(CORE_PATH));
define('APP_PATH', ROOT_PATH);
define('TEST_PATH', ROOT_PATH . DS . 'test');
define('RUNTIME_PATH_NAME', 'runtime');
define('COLUMNS_PATH_NAME', 'columns');
define('NOW_TIME', time());
define('EXT', '.php');

Loader::autoload();

/**
 * Class Console
 * @package GFPHP
 */
class Command
{
    protected string $name = " 
 = = = = = = = = = = = = = = = = = = = = GAPI = = = = = = = = = = = = = = = = = = = = =
";
    protected $stdout;
    protected $stdin;
    protected $stderr;
    protected $argv;

    /**
     * Command constructor.
     */
    public function __construct(
        public string $version = '',
    )
    {

        error_reporting(E_ALL ^ E_NOTICE);
        date_default_timezone_set("PRC");

        $this->stdout = fopen('php://stdout', 'wb');
        $this->stdin = fopen('php://stdin', 'rb');
        $this->stderr = fopen('php://stderr', 'wb');

        header('X-Powered-By: Ling');
        $this->version = $this->version == '' ? Config::system()['app_version'] : $this->version;
        if ($v = Request::get('v', '')) {
            $this->version = 'v' . $v;
        }
        define('APP_VERSION', $this->version);
        define('VERSION_PATH', APP_PATH . DS . $this->version);
        define('APP_DEBUG', Config::app('config.php')['debug']);
        define('COLUMNS_PATH', VERSION_PATH . DS . COLUMNS_PATH_NAME);

        if (!is_dir(VERSION_PATH)) {
            throw new \Exception(VERSION_PATH . ' 目录不存在.');
        }

        Config::file();
        Config::set('js_allow_origin', false, 'app');

        echo $this->name;

        $tag_file = VERSION_PATH . DS . 'tags' . EXT;
        if (is_file($tag_file)) {
            Hook::import(include $tag_file);
        }
        Hook::listen('app_init');

    }

    /**
     * 执行
     */
    public function execute()
    {
        array_shift($_SERVER['argv']);
        $this->argv = $_SERVER['argv'];
        $class = "\\gapi\\command\\{$this->argv[0]}";
        array_shift($this->argv);
        $class::execute($this->argv, $this);

    }


    /**
     * 输出一行
     * @param string $message 输出的消息
     */
    public function writeln($message)
    {
        $this->write($message . "\r\n");
    }

    /**
     * 输出内容
     * @param string $message 输出的消息
     */
    public function write(string $message): void
    {
        if (!is_string($message)) {
            $message = var_export($message, true);
        }
        fwrite($this->stdout, $message);
    }

    /**
     * 读取一行内容
     * @param string $notice
     * @return array
     */
    public function getStdin($notice = ''): array
    {
        $this->write($notice);
        return explode(' ', str_replace(["\r\n", "\n"], '', fgets($this->stdin)));
    }

}