<?php

namespace gapi;

use gapi\attribute\MiddleWare;
use gapi\attribute\ValidateParams;
use gapi\lib\Logger;

class Route
{
    public static string $version = '';
    public static string $flag = 's';
    public static int $runIndex = 0;
    public static array $controllers = [];
    public static $middleWareClass = [
        MiddleWare::class,
        ValidateParams::class
    ];

    /**
     * @var array|string[]
     */
    public static array $routeTypes = ['all', 'get', 'post', 'request', 'put', 'delete', 'options', 'head'];

    public function send(?array $params = []): void
    {
        if (APP_DEBUG) {
            # 自动生成当前版本路由
            $file = \gapi\command\Route::update(APP_VERSION, false);
            Logger::info("route => {$file}");
        }
        Hook::listen('module_init');
        if ($params) {
            $method = $params['method'];
            if (method_exists(self::class, $method)) {
                Route::$method($params['path'], $params['action'], isset($params['pattern']) ? $params['pattern'] : []);
            } else {
                throw new \Exception("ROUTE::{$method} 不存在");
            }
        } else {
            Loader::system('router.php');
            Loader::file('router.php');
            self::runDefaultRoute();
            (new Response())->fail()->setCode(404)->send();
        }
    }

    public static function uri(): string
    {
        $uri = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';
        if ($uri == '') return '/';
        $uri_params = explode('&', $uri);
        $params = [];
        if ($uri_params) {
            foreach ($uri_params as $v) {
                $tmp = explode('=', $v);
                $params[$tmp[0]] = isset($tmp[1]) ? $tmp[1] : '';
            }
        }
        return isset($params[self::$flag]) ? (substr($params[self::$flag], 0, 1) == '/' ? $params[self::$flag] : '/' . $params[self::$flag]) : '/';
    }

    public static function all(array $paths, string|callable $action, array $pattern = [], string $type = 'all'): void
    {
        if (!in_array($type, self::$routeTypes)) {
            throw new \ErrorException("不存在的请求类型[{$type}]");
        }
        self::$runIndex++;

        $route = '';
        $params = [];
        $uri = self::uri();
        if ($pattern) {
            foreach ($paths as $path) {
                $params = self::matchPattern($path, $pattern, $uri);
                if ($params) {
                    $route = $action;
                    break;
                }
            }
        } else {
            foreach ($paths as $v) {
                if ($uri == $v) {
                    $route = $action;
                    break;
                }
            }
        }

        if ($route != '') {
            if ($type != 'all') {
                $method = 'is' . ucfirst($type);
                if (!(new Request())->$method()) {
                    (new Response())->setMsg("请求类型错误:" . $type)->json()->send();
                }
            }

            if ($route instanceof \Closure) {
                $result = $route((new Request())->route($params), new Response());
            } else {

                $result = static::runRoute($route, $params);
            }
            if ($result instanceof Response) {
                $result = (string)$result;
            }
            echo $result;
            Hook::listen('app_end');
            Logger::info("data => {$result}");
            Debug::end();
            exit;
        }

    }

    public static function get(array $paths, string|callable $action, array $pattern = []): void
    {
        self::all($paths, $action, $pattern, 'get');
    }

    public static function post(array $paths, string|callable $action, array $pattern = []): void
    {
        self::all($paths, $action, $pattern, 'post');
    }

    public static function request(array $paths, string|callable $action, array $pattern = []): void
    {
        self::all($paths, $action, $pattern, 'request');
    }

    public static function head(array $paths, string|callable $action, array $pattern = []): void
    {
        self::all($paths, $action, $pattern, 'head');
    }

    public static function delete(array $paths, string|callable $action, array $pattern = []): void
    {
        self::all($paths, $action, $pattern, 'delete');
    }

    public static function options(array $paths, string|callable $action, array $pattern = []): void
    {
        self::all($paths, $action, $pattern, 'options');
    }

    public static function put(array $paths, string|callable $action, array $pattern = []): void
    {
        self::all($paths, $action, $pattern, 'put');
    }


    public static function matchPattern(string $path, array $pattern, string $uri): array
    {

        $path = str_replace('/', '\/', $path);
        foreach ($pattern as $name => $value) {
            $path = str_replace('{' . $name . '}', '(' . $value . ')', $path);
        }
        preg_match_all('/^' . $path . '$/i', $uri, $params);
        $routes = [];
        if ($params[0]) {
            $i = 0;
            foreach ($pattern as $name => $value) {
                $i++;
                if (isset($params[$i])) {
                    $routes[$name] = $params[$i][0];
                }
            }
        }
        return $routes;
    }

    //默认路由
    public static function runDefaultRoute(): void
    {

        $route = substr(self::uri(), 1);

        $mvc = explode('/', $route);
        if (isset($mvc[1])) {
            $controller = $mvc[1];
            $module = $mvc[0];
            if (in_array(strtolower($module . '/' . $controller), self::$controllers)) {

                $result = static::runRoute($route);
                if ($result instanceof Response) {
                    $result = (string)$result;
                    echo $result;
                    Hook::listen('app_end');
                    Logger::info("data => {$result}");
                    Debug::end();
                    exit;
                }
            }
        }
        return;
    }

    public static function runRoute(string $route, array $params = [], string $namespace = 'app'): mixed
    {
        if ($route == '') return null;
        $mvc = explode('/', $route);
        $action = $mvc[2];
        $controller = $mvc[1];
        $module = $mvc[0];
        $class = "\\{$namespace}\\{$module}\\controller\\" . ucfirst($controller);
        if (class_exists($class)) {
            define('ACTION_NAME', $mvc[2]);
            define('CONTROLLER_NAME', $mvc[1]);
            define('MODULE_NAME', $mvc[0]);
            $controller = new $class();
            Logger::info("method => {$namespace}#{$class}@{$action} ROUTE[" . self::$runIndex . "]");
            $request = (new Request())->route($params);

            if (APP_PATH == ROOT_PATH) {
                $response = new Response();
            } else {
                $response = new Response(['version' => substr((new \ReflectionClass($class))->getFileName(), strlen(APP_PATH) + 1, 6)]);
            }
            return self::runControllerMiddleWare($controller, $action, $request, $response);
        }
        return null;
    }

    public static function runControllerMiddleWare(mixed $controller, string $action, Request $request, Response $response)
    {
        //中间件
        $middleController = new \ReflectionClass($controller);
        #执行 类的注解
        $classAttributes = $middleController->getAttributes();
        if ($classAttributes) {
            foreach (self::$middleWareClass as $middleWare) {
                foreach ($classAttributes as $classAttribute) {
                    $item = $classAttribute->newInstance();
                    if ($item instanceof $middleWare) {
                        foreach ($item->middleware as $ware) {
                            (new $ware())->handle($request, $response);
                        }
                    }
                }
            }
        }
        #执行 方法的注解
        $methods = $middleController->getMethods(\ReflectionMethod::IS_PUBLIC);
        if ($methods) {
            //多中间件定义
            foreach ($methods as $method) {
                foreach (self::$middleWareClass as $class) {
                    self::runMiddleWare($class, $method, $action, $request, $response);
                }
            }
        }

        if (is_callable([$controller, $action])) {
            Hook::listen('action_begin');
            return $controller->$action($request, $response);
        }

        return null;
    }

    private static function runMiddleWare(string $class, \ReflectionMethod $method, string $action, Request $request, Response $response): void
    {
        $attributes = $method->getAttributes($class);
        foreach ($attributes as $attribute) {
            $middleware = $attribute->newInstance()->setHandler($method);
            if ($middleware->handler->name == $action) {
                $middleware->handle($request, $response);
            }
        }
    }


    public static function controller(string $controller)
    {
        self::$controllers[] = $controller;
    }

}
