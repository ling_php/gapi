<?php

namespace gapi;

use gapi\database\Db;
use gapi\database\Query;

class Model extends Query
{

    public Db $db;
    public string $table = '';
    public string $tableName = '';
    public string $prefix;
    public static array $tableFields = [];
    public static array $fields = [];
    public static array $validate = [];
    public array $data = [];
    public static array $models = [];
    public static bool $sqlCache = false;

    protected $auto = [];
    protected $update = [];
    protected $insert = [];
    protected static $selectData = [];


    public function __construct(string $table = '')
    {
        $this->db = self::db();
        $this->prefix = $this->db->config['prefix'];
        if ($table != '') {
            $this->tableName = $table;
        } else {
            $class = explode('\\', static::class);
            $class = end($class);
            $this->tableName = substr(strtolower(preg_replace("/([A-Z])/", "_$1", $class)), 1);
        }

        $this->table($this->tableName, $this->prefix);
    }

    public static function setSqlCache(bool $flag = false)
    {
        self::$sqlCache = $flag;
    }

    public static function db(): Db
    {
        return Db::connect(Config::database());
    }

    public function check(string $scene, array $data, bool $batch = false): bool
    {
        if (!isset(self::$validate[static::class])) {
            $class = str_replace('\\model\\', '\\validate\\', static::class);
            if (class_exists($class)) {
                self::$validate[static::class] = new $class();
            } else {
                return true;
            }
        }
        $this->data = $data;
        return self::$validate[static::class]->batch($batch)->scene($scene)->check($data);
    }

    public function filter(array $data): array
    {
        $fields = $this->getTableFields();
        $filter_data = [];
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $filter_data[$field] = $data[$field];
            }
        }
        return $filter_data;
    }

    public function getError()
    {
        return self::$validate[static::class]->getError();
    }

    public function setPrefix(string $prefix): self
    {
        $this->prefix = $prefix;
        return $this;
    }

    public function getPrefix(): string
    {
        return $this->prefix;
    }

    public function columns(string $table = '', string $prefix = '', string $full = ''): array
    {

        if (isset(self::$fields[$this->table]) && $full == '') {
            return self::$fields[$this->table];
        }
        $table = $table == '' ? $this->tableName : $table;
        $prefix = $prefix == '' ? $this->prefix : $prefix;

        return self::$fields[$this->table] = $this->db->columns($table, $prefix, $full);
    }

    public function fields(string $table = '', string $prefix = ''): array
    {
        if (isset(self::$tableFields[$prefix . $table])) {
            return self::$tableFields[$prefix . $table];
        }
        $table = $table == '' ? $this->tableName : $table;
        $prefix = $prefix == '' ? $this->prefix : $prefix;
        $columns = $this->db->query((new Query())->columns($table, $prefix));
        $fields = [];
        $type = [];
        $pk = '';
        if ($columns) {
            foreach ($columns as $field) {
                $fields[$field['Field']] = $field['Field'];
                $type[$field['Field']] = $field['Type'];
                if ($field['Key'] == 'PRI') {
                    $pk = $field['Field'];
                }
            }
        }
        return self::$tableFields[$prefix . $table] = ['fields' => $fields, 'type' => $type, 'pk' => $pk];
    }

    public function getTableFields()
    {
        $class = $this->columnClass();
        return $class::$fields;
    }

    public function tables(string $table = ''): string|array
    {
        return $this->db->query(parent::tables($table));
    }

    public function tableinfo(string $table = '', string $prefix = ''): string|array
    {
        $table = $table == '' ? $this->table : $table;
        return $this->db->query(parent::tableinfo($table, $prefix));
    }

    public function table(string $table, string $prefix = '', string $as = ''): self
    {
        parent::table($table, $prefix == '' ? $this->prefix : $prefix, $as);
        return $this;
    }

    public function setTable(string $table): self
    {
        $this->tableName = $table;
        $this->table = $this->prefix . $table;
        return $this;
    }

    /**
     * 表 别名
     * @param string $as
     * @return $this
     */
    public function alias(string $alias): self
    {
        $this->alias = " as {$alias}";
        return $this;
    }

    // 自动替换 __开头的表明前缀 为 当前数据库前缀
    public function prefixTable(string $table): string
    {
        return preg_replace('/^__/', $this->prefix, $table);
    }

    public function field(): self
    {
        $args = func_get_args();
        $num = func_num_args();
        if ($num == 0) {
            $fields = $this->columns($this->tableName, $this->prefix);
            $this->section['field'] = '`' . implode('`,`', $fields) . '`';
        } else if ($num == 1) {
            $field = $args[0];
            if ($field === true) {
                $fields = $this->columns($this->tableName, $this->prefix);
                $field = '`' . implode('`,`', $fields) . '`';
            }
            $this->section['field'] = $field;
        } else {
            $fields = [];
            foreach ($args as $arg) {
                if (strpos($arg, ' as ')) {
                    $tmp = explode(' as ', $arg);
                    $fields[] = "{$tmp[0]}` as `{$tmp[1]}";
                } else {
                    $fields[] = $arg;
                }
            }
            $field = '`' . implode('`,`', $fields) . '`';
            $this->section['field'] = $field;
        }
        return $this;
    }


    /**
     * 组合where语句
     * @param string|array $where
     * @param string $type where() 和 where() 之间 and/or
     * @param string $field_type
     * @return $this
     */
    public function where(string|array $where, string $type = 'and', string $field_type = 'and'): self
    {


        if (empty($where)) {
            return $this;
        }

        $this->section['where'] .= $this->section['where'] == '' ? ' where ' : " {$type} ";
        if (is_string($where)) {
            $this->section['where'] .= "{$where}";
        } else {
            $where_sql = [];

            foreach ($where as $field => $option) {
                $field = self::parseField($field);
                if (is_array($option)) {
                    $where_sql[] = $this->parseWhereOption($field, $option);
                } else {
                    $where_sql[] = $field . '=' . $this->quote($option);
                }
            }

            $this->section['where'] .= "(" . implode(" {$field_type} ", $where_sql) . ")";

        }
        return $this;
    }

    public static function parseField(string $field)
    {
        return '`' . implode('`.`', explode('.', $field)) . '`';
    }

    public function parseWhereOption(string $field, array $option): string
    {
        # > >= <> <= < like notlike in notin between notbetween is not null is null
        $exp = $option[0];
        $value = isset($option[1]) ? $option[1] : '';
        $whereStr = '';
        if (in_array($exp, ['=', '<>', '>', '>=', '<', '<='])) {
            // 比较运算
            $whereStr .= $field . ' ' . $exp . ' ' . $this->parseValue($value);
        } elseif (in_array($exp, ['in', 'not in'])) {
            if (is_array($value)) {
                $whereStr .= $field . ' ' . $exp . ' (' . "'" . implode("','", $value) . "'" . ')';
            } else {
                $whereStr .= $field . ' ' . $exp . ' (' . $value . ')';
            }

        } elseif (in_array($exp, ['between', 'not between'])) {
            if (is_array($value)) {
                $whereStr .= $field . ' ' . $exp . ' ' . implode(' and ', $value);
            } else {
                $whereStr .= $field . ' ' . $exp . ' ' . implode(' and ', explode(',', $value));
            }
        } elseif (in_array($exp, ['like', 'not like'])) {
            $liketype = isset($option[2]) ? $option[2] : 'both';
            $whereStr = match ($liketype) {
                'both' => $field . ' ' . $exp . ' ' . "'%{$value}%'",
                'left' => $field . ' ' . $exp . ' ' . "'%{$value}'",
                'right' => $field . ' ' . $exp . ' ' . "'{$value}%'",
            };
        } elseif (in_array($exp, ['is null', 'is not null'])) {
            $whereStr = match ($exp) {
                'is null', 'is not null' => $field . ' ' . $exp,
            };
        }
        return $whereStr;
    }

    public function select(): mixed
    {
        if ($this->section['field'] == '') {
            $this->field(true);
        }
        parent::select();

        if (self::$sqlCache) {
            if (isset(self::$selectData[$this->sql])) {
                return self::$selectData[$this->sql];
            } else {
                return self::$selectData[$this->sql] = $this->db->query($this->sql);
            }
        }
        return $this->db->query($this->sql);
    }

    public function buildSelectQuery(): string
    {
        if ($this->section['field'] == '') {
            $this->field(true);
        }
        parent::select();
        return $this->sql;
    }

    public function find(): mixed
    {
        parent::find();
        if (self::$sqlCache) {
            if (isset(self::$selectData[$this->sql])) {
                return self::$selectData[$this->sql];
            } else {
                $lists = $this->db->query($this->sql);
                return self::$selectData[$this->sql] = $lists[0] ?? [];
            }
        }

        $lists = $this->db->query($this->sql);
        return $lists[0] ?? [];
    }

    public function buildFindQuery(): string
    {
        parent::find();
        return $this->sql;
    }

    public function value(string $field): mixed
    {
        $this->field($field);
        parent::find();
        if (self::$sqlCache) {
            if (isset(self::$selectData[$this->sql])) {
                return self::$selectData[$this->sql];
            } else {
                $lists = $this->db->query($this->sql);
                $value = isset($lists[0]) ? $lists[0] : [];
                return self::$selectData[$this->sql] = $value[$field] ?? '';
            }
        }

        $lists = $this->db->query($this->sql);
        $value = isset($lists[0]) ? $lists[0] : [];
        return $value[$field] ?? '';
    }

    public function buildValueQuery(string $field): string
    {
        $this->field($field);
        parent::find();
        return $this->sql;
    }

    public function query(string|Query $sql): mixed
    {

        return $this->db->query($sql);
    }

    public function exec(string|Query $sql): mixed
    {
        return $this->db->exec($sql);
    }

    public function startTrans(): bool
    {
        return $this->db->startTrans();
    }

    public function commit(): bool
    {
        return $this->db->commit();
    }

    public function rollBack(): bool
    {
        return $this->db->rollBack();
    }


    public function parseValue(string|array $value): string|array
    {
        if (is_string($value)) {
            $value = $this->quote($value);
        } elseif (is_array($value)) {
            $value = array_map([$this, 'parseValue'], $value);
        } elseif (is_bool($value)) {
            $value = $value ? '1' : '0';
        } elseif (is_null($value)) {
            $value = 'null';
        }
        return $value;
    }


    public function sum(string $field, string $as = 'sum_num'): mixed
    {
        parent::sum($field, $as);
        return $this->find()[$as];
    }

    public function buildSumQuery(string $field, string $as = 'sum_num'): string
    {
        parent::sum($field, $as);
        parent::find();
        return $this->sql;
    }

    public function count(string $field = '*', string $as = 'total'): mixed
    {
        parent::count($field, $as);
        return (int)$this->find()[$as];
    }

    public function buildCountQuery(string $field = '*', string $as = 'total'): string
    {
        parent::count($field, $as);
        parent::find();
        return $this->sql;
    }

    public function min(string $field, string $as = 'min_num'): mixed
    {
        parent::min($field, $as)['min_num'];
        return $this->find();
    }

    public function buildMinQuery(string $field, string $as = 'min_num'): string
    {
        parent::min($field, $as);
        parent::find();
        return $this->sql;
    }


    public function max(string $field, string $as = 'max_num'): mixed
    {
        parent::max($field, $as);
        return $this->find()['max_num'];
    }

    public function buildMaxQuery(string $field, string $as = 'max_num'): string
    {
        parent::max($field, $as);
        parent::find();
        return $this->sql;
    }


    public function avg(string $field, string $as = 'avg_num'): mixed
    {
        parent::avg($field, $as);
        return $this->find()['avg_num'];
    }

    public function buildAvgQuery(string $field, string $as = 'avg_num'): string
    {
        parent::avg($field, $as);
        parent::find();
        return $this->sql;
    }

    public function parseData(array $data, array $options = []): string
    {
        $values = [];
        $fields = [];
        $options['strict'] = isset($options['strict']) ? $options['strict'] : true;
        if ($options['strict']) {
            $table_fields = $this->columns($this->tableName, $this->prefix);
            foreach ($data as $field => $value) {
                if (!in_array($field, $table_fields, true)) {
                    throw new \ErrorException('fields not exists:[' . $field . ']');
                }
                $values[] = (string)$this->quote($value);
                //$fields[] = $field;
                $fields[] = "`{$field}`";
            }
        } else {
            foreach ($data as $field => $value) {
                $values[] = (string)$this->quote($value);
                //$fields[] = $field;
                $fields[] = "`{$field}`";
            }
        }
        $this->section['field'] = implode(',', $fields);
        return implode(',', $values);
    }

    public function quote(string $value): string
    {
        return $this->db->quote($value);
    }

    public function runAutoFields(array $auto, array &$data)
    {
        //优先
        if ($auto) {
            foreach ($auto as $field) {
                $method = 'set' . ucfirst($field) . 'Attr';
                if (method_exists($this, $method)) {
                    $data[$field] = $data[$field] ?? '';
                    $data[$field] = $this->$method($data[$field], $data);
                }
            }
        }
    }

    public function autoFields(array $auto, array $data)
    {
        //优先
        if ($auto) {
            foreach ($auto as $field) {
                if (isset($data[$field])) {
                    $method = 'set' . ucfirst($field) . 'Attr';
                    if (method_exists($this, $method)) {
                        $data[$field] = $this->$method($data[$field], $data);
                    }
                }
            }
        }
        return $data;
    }


    public function insert(array $data = [], bool $replace = false, array $options = []): mixed
    {
        $data = $data ? $data : $this->data;

        if ($this->auto === false) {
            //不自动更新
        } else {
            $auto = array_unique(array_merge($this->auto, $this->insert));
            $this->runAutoFields($auto, $data);
        }

        $data = $this->filter($data);
        parent::insert($data, $replace, $options);
        $e = $this->exec($this->sql);
        if ($e) {
            return $this->getLastInsertId();
        } else {
            return null;
        }
    }

    public function auto(mixed $auto = [])
    {
        if ($auto === false) {
            $this->auto = false;
        } else {
            $this->auto = $auto;
        }
        return $this;
    }

    public function buildInsertQuery(array $data = [], bool $replace = false, array $options = []): string
    {
        $data = $data ? $data : $this->data;
        $data = $this->filter($data);
        if ($this->auto === false) {
            //不自动更新
        } else {
            $auto = array_unique(array_merge($this->auto, $this->insert));
            $this->runAutoFields($auto, $data);
        }

        parent::insert($data, $replace, $options);
        return $this->sql;
    }

    public function getLastInsertId(): int
    {
        return $this->query(parent::lastInsertId())[0]['id'];
    }


    /**
     * 生成insertAll语句
     * @param array $dataSet
     * @param array $options
     * @param bool $replace
     * @return mixed
     */
    public function insertAll(array $dataSet, array $options = [], bool $replace = false): mixed
    {
        $this->buildInsertAllQuery($dataSet, $options = [], $replace);
        return $this->exec($this->sql);
    }

    public function buildInsertAllQuery(array $dataSet, array $options = [], bool $replace = false): string
    {
        $this->section['insert'] = $replace ? 'REPLACE' : 'INSERT';
        $fields = [];
        $options['strict'] = isset($options['strict']) ? $options['strict'] : true;
        if ($options['strict']) {
            $table_fields = $this->db->columns($this->tableName);
            foreach ($dataSet as $key => $data) {
                $values = [];
                foreach ($data as $field => $value) {
                    $values[] = $this->parseValue($value);
                    if ($key == 0) {
                        if (!in_array($field, $table_fields, true)) {
                            throw new \ErrorException('fields not exists:[' . $field . ']');
                        }
                        $fields[] = $field;
                    }
                }
                $datas[] = "(" . implode(",", $values) . ")";
            }
        } else {
            foreach ($dataSet as $key => $data) {
                $values = [];
                foreach ($data as $field => $value) {
                    $values[] = $this->parseValue($value);
                    if ($key == 0) {
                        $fields[] = $field;
                    }
                }
                $datas[] = "(" . implode(",", $values) . ")";
            }
        }

        $this->section['field'] = implode(',', $fields);
        $this->section['data'] = implode(',', $datas);
        $this->sql = str_replace(
            ['%INSERT%', '%TABLE%', '%FIELD%', '%DATA%', '%COMMENT%'],
            [
                $this->section['insert'],
                $this->table,
                $this->section['field'],
                $this->section['data'],
                $this->section['comment'],
            ], $this->insertAllSql);
        $this->reset();
        return $this->sql;
    }

    public function truncate()
    {
        $this->exec(parent::truncate());
    }

    public function buildTruncateQuery()
    {
        return parent::truncate();
    }

    public function update(array $data, array $options = [], $flag = false): mixed
    {
        $data = $this->filter($data);

        if ($this->auto === false) {
            //不自动更新
        } else {
            $auto = array_unique(array_merge($this->auto, $this->update));
            $this->runAutoFields($auto, $data);
        }

        parent::update($data, $options, $flag);
        return $this->exec($this->sql);
    }

    public function buildUpdateQuery(array $data, array $options = [], $flag = false): string
    {
        $data = $this->filter($data);
        if ($this->auto === false) {
            //不自动更新
        } else {
            $auto = array_unique(array_merge($this->auto, $this->update));
            $this->runAutoFields($auto, $data);
        }
        parent::update($data, $options, $flag);
        return $this->sql;
    }

    public function parseUpdateData(array $data, array $option = []): array
    {
        $values = [];
        foreach ($data as $field => $value) {
            if (is_array($value)) {
                $values[] = "`{$field}`=`{$value[0]}`{$value[1]}";
            } else {
                $values[] = "`{$field}`=" . $this->quote($value);
            }

        }
        return $values;
    }

    public function delete(array $options = [], bool $flag = false): mixed
    {
        parent::delete($options, $flag);
        return $this->exec($this->sql);
    }

    public function buildDeleteQuery(array $options = [], bool $flag = false): string
    {
        parent::delete($options, $flag);
        return $this->sql;
    }

    public function pk(): int|string
    {
        return $this->columnClass()::PRIMARY_PK;
    }

    public function getLastSql()
    {
        return $this->sql;
    }

    public function page(int $page): self
    {
        $this->section['page'] = $page;
        return $this;
    }

    public function pagesize(int $pagesize): self
    {
        $this->section['pagesize'] = $pagesize;
        return $this;
    }


    /**
     * 懒加载 对方参数
     * @param $property
     * @return mixed
     */
    public function __get($property): mixed
    {
        return match ($property) {
            'pk' => $this->pk = $this->pk()
        };
    }

    public static function model()
    {
        if (!isset(self::$models[static::class])) {
            self::$models[static::class] = new static();
        }
        return self::$models[static::class];
    }

    public function columnClass()
    {
        return "\\app\\columns\\t_{$this->tableName}";
    }

}