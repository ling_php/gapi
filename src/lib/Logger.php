<?php

namespace gapi\lib;

use gapi\Config;
use gapi\extend\WebSocketClient;
use gapi\Hook;
use gapi\Request;

class Logger
{
    public const INFO = 'info';
    public const SQL = 'sql';
    public const DEBUG = 'debug';
    public const ERROR = 'error';

    public static string $path;
    public static string $format = 'H';
    public static string $suffix = '.log';
    public static array $level = [self::INFO, self::SQL, self::ERROR, self::DEBUG];

    public static bool $wss = false; # 开启websocket 调试  默认地址：wss://127.0.0.1:8866/wss
    public static bool $logfile = false;
    public static bool $debug = false;
    public static bool $trace = false;

    public static string $traceData = '';
    public static int $logIndex = 0;

    public static function write(?string $msg, string $type): void
    {

        if (++self::$logIndex <= 1) {
            $config = Config::app('config.php');
            self::$debug = $config['debug'];
            self::$logfile = $config['log_file'];
            self::$wss = $config['wss'];
            self::$trace = $config['trace'];
            self::$level = $config['log_level'];
            if (in_array('info', self::$level)) {
                $msg .= self::request();
            }
        }
        if ($msg == '') {
            return;
        }

        $log = [$type => $msg];
        Hook::listen('log_write', $log);

        # 只记录配置的类型
        if (!in_array($type, self::$level)) {
            return;
        }

        if (self::$trace) {
            self::$traceData .= $msg . "\n";
            return;
        }

        if (self::$logfile) {
            $info = "[{$type}]" . date('m-d H:i:s: ') . $msg . "\n";
            self::$path = ROOT_PATH . DS . 'data/logs' . DS . date('Y-m-d');
            if (!is_dir(self::$path)) {
                @mkdir(self::$path, 0777, true);
            }
            $file = self::$path . DS . date(self::$format) . self::$suffix;
            $fp = fopen($file, "a+");
            fwrite($fp, $info);
            fclose($fp);
        }
        if (self::$wss) {
            $info = "[{$type}]" . date('m-d H:i:s: ') . $msg . "\n";
            # 同步日志服务
            WebSocketClient::getInstance()->sendData($info);
        }
        return;
    }

    public static function sql(?string $msg): void
    {
        self::write($msg, 'sql');
    }

    public static function error(?string $msg): void
    {
        self::write($msg, 'error');
    }

    public static function request(): string
    {
        $request = new Request();
        $msg = '';
        foreach ($request->header->get() as $key => $value) {
            $msg .= "\n[{$key}]:{$value}";
        }
        // $msg = "\n[User-Agent]:" . $request->header->get()['User-Agent'];
        $methods = ['get', 'post', 'route'];
        foreach ($methods as $method) {
            $data = $request->$method->get();
            if ($data) {
                $msg .= "\n[" . strtoupper($method) . "]:" . var_export($request->$method->get(), 1);
            }
        }
        if ($cookie = $request->cookie::get()) {
            $msg .= "\n[COOKIE]:" . var_export($cookie, 1);
        }
        if ($session = $request->session::get()) {
            $msg .= "\n[SESSION]:" . var_export($session, 1);
        }

        return $msg;
    }

    public static function info(?string $msg): void
    {
        self::write($msg, 'info');
    }

    public static function debug(?string $msg): void
    {
        self::write($msg, 'debug');
    }

    public static function getTrace()
    {
        return self::$traceData;
    }

}