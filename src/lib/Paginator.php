<?php

namespace gapi\lib;

use gapi\Request;

class Paginator
{
    public int $firstRow; // 起始行数
    public int $listRows; // 列表每页显示行数
    public array $parameter; // 分页跳转时要带的参数
    public int $totalRows; // 总行数
    public int $totalPages; // 分页总页面数
    public int $rollPage = 10;// 分页栏每页显示的页数
    public bool $lastSuffix = true; // 最后一页是否显示总页数

    private string $p = 'page'; //分页参数名
    private int $nowPage = 1;

    private \Closure|null $urlfunc = null; //链接方法


    // 分页显示定制
    protected $config = array(
        'header' => '<a title="Total record"><b>%TOTAL_ROW%</b></a>',
        'prev' => '&lt;&lt;',
        'prev_tpl' => '<a class="prev" href="{url}">{page}</a>',
        'next' => '&gt;&gt;',
        'next_tpl' => '<a class="prev" href="{url}">{page}</a>',
        'first' => '1',
        'first_tpl' => '<a class="first" href="{url}">{page}</a>',
        'last' => '...%TOTAL_PAGE%',
        'last_tpl' => '<a class="end" href="{url}">{page}</a>',
        'link_tpl' => '<a class="num" href="{url}">{page}</a>',
        'current_tpl' => '<b>{page}</b>',
        'theme' => '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END%',
    );

    /**
     * 架构函数
     * @param array $totalRows 总的记录数
     * @param array $listRows 每页显示记录数
     * @param array $parameter 分页跳转的参数
     */
    public function __construct(int $totalRows, int $listRows = 20, $parameter = [], mixed $urlfunc = null)
    {
        $this->urlfunc = $urlfunc;
        /* 基础设置 */
        $this->totalRows = $totalRows; //设置总记录数
        $this->listRows = $listRows;  //设置每页显示行数

        $this->parameter = empty($parameter) ? $this->filter(Request::instance()->get->get()) : $parameter;

        $route_page = Request::instance()->route->get($this->p) ?? 0;
        $this->nowPage = $route_page > 0 ? $route_page : Request::instance()->get($this->p, 0);
        $this->nowPage = $this->nowPage > 0 ? $this->nowPage : 1;

        $this->firstRow = $this->listRows * ($this->nowPage - 1);
    }

    /**
     * 过滤
     */
    public function filter(array $data): array
    {
        $get = [];
        if (!empty($data)) {
            foreach ($data as $k => $v) {
                if (!empty($v)) {
                    $get[$k] = $v;
                }
            }
        }
        return $get;
    }

    /**
     * 定制分页链接设置
     * @param string $name
     * @param string $value
     */
    public function setConfig(string|array $name, string $value = '')
    {
        if (is_array($name)) {
            if($name){
                foreach ($name as $key=>$value){
                    $this->config[$key] = $value;
                }
            }
        } else {
            if (isset($this->config[$name])) {
                $this->config[$name] = $value;
            }
        }
    }

    /**
     * 生成链接URL
     * @param integer $page 页码
     * @return string
     */
    private function url(int $page)
    {
        $urlfunc = $this->urlfunc;
        if (is_callable($urlfunc)) {
            return $urlfunc($page);
        }
        return url(MODULE_NAME . '/' . CONTROLLER_NAME . '/' . ACTION_NAME, array_merge($this->parameter, array($this->p => $page)));
    }

    /**
     * 组装分页链接
     * @return string
     */
    public function show(): string
    {
        if (0 == $this->totalRows) return '';

        /* 计算分页信息 */
        $this->totalPages = ceil($this->totalRows / $this->listRows); //总页数
        if (!empty($this->totalPages) && $this->nowPage > $this->totalPages) {
            $this->nowPage = $this->totalPages;
        }

        /* 计算分页零时变量 */
        $now_cool_page = $this->rollPage / 2;
        $now_cool_page_ceil = ceil($now_cool_page);
        $this->lastSuffix = $this->config['last'] = $this->totalPages;

        //上一页
        $up_row = $this->nowPage - 1;

        $up_page = $up_row > 0 ? str_replace(['{url}', '{page}'], [$this->url($up_row), $this->config['prev']], $this->config['prev_tpl']) : '';

        //下一页
        $down_row = $this->nowPage + 1;
        $down_page = ($down_row <= $this->totalPages) ? str_replace(['{url}', '{page}'], [$this->url($down_row), $this->config['next']], $this->config['next_tpl']) : '';

        //第一页
        $the_first = '';
        if ($this->totalPages > $this->rollPage && ($this->nowPage - $now_cool_page) >= 1) {

            $the_first = str_replace(['{url}', '{page}'], [$this->url(1), $this->config['first']], $this->config['first_tpl']);
        }

        //最后一页
        $the_end = '';
        if ($this->totalPages > $this->rollPage && ($this->nowPage + $now_cool_page) < $this->totalPages) {
            $the_end = str_replace(['{url}', '{page}'], [$this->url($this->totalPages), $this->config['last']], $this->config['last_tpl']);

        }

        //数字连接
        $link_page = "";
        for ($i = 1; $i <= $this->rollPage; $i++) {
            if (($this->nowPage - $now_cool_page) <= 0) {
                $page = $i;
            } elseif (($this->nowPage + $now_cool_page - 1) >= $this->totalPages) {
                $page = $this->totalPages - $this->rollPage + $i;
            } else {
                $page = $this->nowPage - $now_cool_page_ceil + $i;
            }
            if ($page > 0 && $page != $this->nowPage) {

                if ($page <= $this->totalPages) {
                    $link_page .= str_replace(['{url}', '{page}'], [$this->url($page), $page], $this->config['link_tpl']);
                } else {
                    break;
                }
            } else {
                if ($page > 0 && $this->totalPages != 1) {
                    $link_page .= str_replace(['{page}'], [$page], $this->config['current_tpl']);
                }
            }
        }

        //替换分页内容
        $page_str = str_replace(
            array('%HEADER%', '%NOW_PAGE%', '%UP_PAGE%', '%DOWN_PAGE%', '%FIRST%', '%LINK_PAGE%', '%END%', '%TOTAL_ROW%', '%TOTAL_PAGE%'),
            array($this->config['header'], $this->nowPage, $up_page, $down_page, $the_first, $link_page, $the_end, $this->totalRows, $this->totalPages),
            $this->config['theme']);
        return "{$page_str}";
    }
}