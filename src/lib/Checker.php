<?php

namespace gapi\lib;

class Checker
{

    public static $filters = [
        'string' => '字符串格式不正确',
        'mobile' => '手机号码格式不正确',
        'require' => '%KEY%内容必须',
        'email' => '邮箱格式不正确',
        'currency' => '金额格式不正确',
        'number' => '数字格式不正确',
        'numeric' => '数字或数字字符串格式不正确',
        'float' => 'float格式不正确',
        'english' => '英文格式不正确',
        'chinese' => '中文格式不正确',
        'qq' => 'QQ格式不正确',
        'wechat' => '微信格式不正确',
        'date' => '年月日格式不正确',
        'datetime' => '0000-00-00 00:00:00格式不正确',
        'card' => '身份证格式不正确',
        'zip' => '邮编格式不正确',
        'empty' => '不为空',
        'isfalse' => '不为假',
        'istrue' => '不为真',

    ];

    public static function msg(string $filter, string $key = ''): string
    {
        return str_replace('%KEY%', $key, self::$filters[$filter]);
    }

    public static function mobile(string $s): bool
    {
        return self::empty($s) || preg_match("/^\d{7,11}$/", $s);
    }

    public static function empty(string $s): bool
    {
        return empty($s);
    }

    public static function require(string $s): bool
    {
        return !empty($s);
    }

    public static function email(string $s): bool
    {
        return preg_match("/^\w+\.?\w*@\w+\.\w*\.?[a-z]{2,}$/", $s);
    }

    public static function currency(string $s): bool
    {
        return preg_match("/^\d+(\.\d+)?$/", $s);
    }

    public static function number(string $s): bool
    {
        return self::empty($s) || preg_match("/^\d+$/", $s);
    }

    public static function numeric(string $s): bool
    {
        return is_numeric($s);
    }

    //邮编
    public static function zip(string $s): bool
    {
        return preg_match("/^\d{6}$/", $s);
    }

    public static function float(string $s): bool
    {
        return preg_match("/^[-\+]?\d+(\.\d+)?$/", $s);
    }

    public static function english(string $s): bool
    {
        return preg_match("/^[A-Za-z]+$/", $s);
    }

    public static function string(string $s): bool
    {
        return preg_match("/^\w+$/", $s);
    }

    public static function chinese(string $s): bool
    {
        return preg_match("/^[\u4e00-\u9fa5]+$/", $s);
    }

    public static function qq(string $s): bool
    {
        return preg_match("/^1[3456789][0-9]{9}$/", $s);
    }

    public static function wechat(string $s): bool
    {
        return preg_match("/^[a-zA-Z\d_]{6,}$/", $s);
    }

    #日期正则 年月日
    public static function date(string $s): bool
    {
        return preg_match("/^(?:(?!0000)[0-9]{4}([-/.]?)(?:(?:0?[1-9]|1[0-2])\1(?:0?[1-9]|1[0-9]|2[0-8])|(?:0?[13-9]|1[0-2])\1(?:29|30)|(?:0?[13578]|1[02])\1(?:31))|(?:[0-9]{2}(?:0[48]|[2468][048]|[13579][26])|(?:0[48]|[2468][048]|[13579][26])00)([-/.]?)0?2\2(?:29))$/", $s);
    }

    #0000-00-00 00:00:00
    public static function datetime(string $s): bool
    {
        return preg_match("/^(?:(?!0000)[0-9]{4}([-/.]?)(?:(?:0?[1-9]|1[0-2])\1(?:0?[1-9]|1[0-9]|2[0-8])|(?:0?[13-9]|1[0-2])\1(?:29|30)|(?:0?[13578]|1[02])\1(?:31))|(?:[0-9]{2}(?:0[48]|[2468][048]|[13579][26])|(?:0[48]|[2468][048]|[13579][26])00)([-/.]?)0?2\2(?:29))\s+([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/", $s);
    }

    //身份证
    public static function card(string $s): bool
    {
        return preg_match("/^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/", $s);
    }

    public static function istrue(bool $flag): bool
    {
        return $flag === true;
    }

    public static function isfalse(bool $flag): bool
    {
        return $flag === false;
    }
}