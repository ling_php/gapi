<?php

namespace gapi\lib;


use gapi\Response;

class Validate
{
    /**
     * @var 字符串
     */
    public const  IS_STRING = 'string';
    /**
     * @var 手机号
     */
    public const  IS_MOBILE = 'mobile';
    /**
     * @var 必填
     */
    public const  IS_REQUIRE = 'require';
    /**
     * @var E-mail
     */
    public const  IS_EMAIL = 'email';
    /**
     * @var 现金
     */
    public const  IS_CURRENCY = 'currency';
    /**
     * @var 数字/数字字符串
     */
    public const  IS_NUMBER = 'number';
    /**
     * @var 数字/数字字符串
     */
    public const  IS_NUMERIC = 'numeric';
    /**
     * @var 浮点数
     */
    public const  IS_FLOAT = 'float';
    /**
     * @var 英文
     */
    public const  IS_ENGLISH = 'english';
    /**
     * @var 中文
     */
    public const  IS_CHINESE = 'chinese';
    /**
     * @var QQ号
     */
    public const  IS_QQ = 'qq';
    /**
     * @var 微信号
     */
    public const  IS_WECHAT = 'wechat';
    /**
     * @var 日期 0000-00-00
     */
    public const  IS_DATE = 'date';
    /**
     * @var 时间 0000-00-00 00:00:00
     */
    public const  IS_DATETIME = 'datetime';
    /**
     * @var 身份证
     */
    public const  IS_CARD = 'card';
    /**
     * @var 空
     */
    public const  IS_EMPTY = 'empty';
    /**
     * @var 假
     */
    public const  IS_FALSE = 'isfalse';
    /**
     * @var 真
     */
    public const  IS_TRUE = 'istrue';
    /**
     * @var 规则
     */
    public const  IS_PATTERN = 'pattern';


    private mixed $data;
    private array $filter;
    private string $pattern = '';

    public function __construct(mixed $data = '')
    {
        $this->data = $data;
    }

    public function setData(mixed $data): self
    {
        $this->data = $data;
        return $this;
    }


    public function pattern(string $pattern): self
    {
        $this->pattern = $pattern;
        return $this;
    }

    public function filter(): self
    {
        $this->filter = func_get_args();
        if (func_num_args() == 1) {
            if (is_array($this->filter[0])) {
                $this->filter = $this->filter[0];
            }
        }
        return $this;
    }


    public function setMsg(): void
    {

        $msgs = func_get_args();
        if (func_num_args() == 1) {
            if (is_array($msgs[0])) {
                $msgs = $msgs[0];
            }
        }
        if ($this->filter) {
            foreach ($this->filter as $key => $filter) {
                $msg = $msgs[$key] ?? '';
                self::check($this->data, $filter, $msg, $this->pattern);
            }
        }
    }

    public static function check(mixed $data, string $filter, string $msg, string $pattern = ''): void
    {
        if (method_exists(Checker::class, $filter)) {
            if (!Checker::$filter($data)) {
                Response::instance()->fail()->setMsg($msg)->send();
            }
        } else {
            if ($filter == self::IS_PATTERN) {
                if (!preg_match($pattern, $data)) {
                    Response::instance()->fail()->setMsg($msg)->send();
                }
            } else {
                throw new \ErrorException("未知的验证类型[{$filter}]");
            }
        }
    }

}