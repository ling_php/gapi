<?php

namespace gapi\lib\crypt\driver;

class Des
{

    /**
     * 加密字符串
     * @param string $str 字符串
     * @param string $key 加密key
     * @return string
     */
    public static function encrypt(string $str, string $key, int $expire = 0): string
    {
       return base64_encode(openssl_encrypt($str, "DES-ECB", $key, OPENSSL_RAW_DATA));
    }

    /**
     * 解密字符串
     * @param string $str 字符串
     * @param string $key 加密key
     * @return string
     */
    public static function decrypt(string $data, string $key): string
    {
        return openssl_decrypt(base64_decode($data), "DES-ECB", $key, OPENSSL_RAW_DATA);
    }

}