<?php

namespace gapi\lib;

use gapi\Loader;
use gapi\Request;
use gapi\Response;
use gapi\Route;

class PluginsRoute extends Route
{
    public static function runRoute(string $route, array $params = [], string $namespace = 'plugins'): mixed
    {

        return parent::runRoute($route, $params, $namespace);
    }

    public static function init()
    {
        //自动加载插件路由
        $lists = dir_list_one(PLUGINS_PATH . DS);
        if ($lists) {
            foreach ($lists as $value) {
                $router_file = PLUGINS_PATH . DS . $value . DS . 'router.php';
                $module = substr($value, 0, -7); //_plugins
                $controller_dir = PLUGINS_PATH . DS . $value . DS . 'controller' . DS;
                $controllers = dir_list($controller_dir);

                //自动加载控制器路由 形如： /plugins/test/index/test
                if ($controllers) {
                    foreach ($controllers as $controller_file) {
                        $controller = strtolower(explode('.', basename($controller_file))[0]);
                        PluginsRoute::controller($module . '/' . $controller);
                    }
                }
                //自动加载路由文件
                if (is_file($router_file)) {
                    include $router_file;
                }
            }
        }

        self::runDefaultRoute();
    }
}