<?php

namespace gapi\lib\curl;
/**
 * @desc Curl 方式 http  负责远程获取信息
 */
class Curl
{

    /**
     * 远程获取数据，POST模式
     * @param string $url
     * @param array $para
     * @param string $charset
     * @param string $refer
     * @param bool $cookie
     * @return mixed
     */
    public static function post(string $url, array $para, string $charset = '', string $refer = '', bool $cookie = false, array $headers = [], string $cookie_file = ''): mixed
    {
        $url = $charset == '' ? $url : $url . "_input_charset=" . $charset;
        $ch = curl_init($url);
        //伪造cookie
        $cookie_file = $cookie_file = '' ? dirname(__FILE__) . '/cookie.txt' : $cookie_file;
        if ($cookie) {
            curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file); // 存放Cookie信息的文件名称
            curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file); // 读取上面所储存的Cookie信息
        }

        if (preg_match("/^https/i", $url)) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);//SSL证书认证
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);//严格认证
            curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');//证书地址
        }

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);// 显示输出结果
        curl_setopt($ch, CURLOPT_POST, true); // post传输数据
        curl_setopt($ch, CURLOPT_POSTFIELDS, $para);// post传输数据
        if ($refer != '') {
            curl_setopt($ch, CURLOPT_REFERER, $refer); // 看这里，你也可以说你从google来
        }
        //$ip = self::randIp();
        //伪造ip
        $headers_option = ['Connection: keep-alive', 'Keep-Alive: 30'];
        curl_setopt($ch, CURLOPT_HTTPHEADER, array_merge($headers_option, $headers));
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36");
        $responseText = curl_exec($ch);
        curl_close($ch);
        return $responseText;
    }


    /**
     * curl 方式 get 获取内容
     * @param string $url
     * @param int $time
     * @param string $refer
     * @param bool $cookie
     * @return mixed
     */
    public static function get(string $url, int $time = 0, string $refer = '', bool|string $cookie = false, array $headers = [], string $cookie_file = ''): mixed
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, trim($url));
        if ($time > 0) {
            curl_setopt($ch, CURLOPT_TIMEOUT, $time);
        }

        //伪造cookie
        $cookie_file = $cookie_file = '' ? dirname(__FILE__) . '/cookie.txt' : $cookie_file;
        if (is_string($cookie)) {
            curl_setopt($ch, CURLOPT_COOKIE, $cookie);
        } else {
            if ($cookie) {
                curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file); // 存放Cookie信息的文件名称
                curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file); // 读取上面所储存的Cookie信息
            }
        }
        // 在尝试连接时等待的秒数
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 2);          //单位 秒，也可以使用
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);     //注意，毫秒超时一定要设置这个
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 2200); //超时毫秒，cURL 7.16.2中被加入。从PHP 5.2.3起可使用


        if (preg_match("/^https/i", $url)) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);//SSL证书认证
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);//严格认证
            curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');//证书地址
        }

        //伪造浏览器数据
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36');

        //$ip = self::randIp(); //, 'X-FORWARDED-FOR:' . $ip, 'CLIENT-IP:' . $ip
        //伪造ip
        $headers_option = ['Keep-Alive: 30', 'X-Requested-With: XMLHttpRequest'];
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array_merge($headers_option, $headers));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if ($refer != '') {
            //伪造来源
            curl_setopt($ch, CURLOPT_REFERER, $refer); // 看这里，你也可以说你从google来
        }
        //禁止302跳转
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $content = curl_exec($ch);
        // $header_info = curl_getinfo($ch);
        curl_close($ch);
        return $content;
    }

    public static function cookie()
    {
        return self::extractCookies(file_get_contents(dirname(__FILE__) . '/cookie.txt'));
    }

    public static function extractCookies(string $string): array
    {
        $lines = explode(PHP_EOL, $string);

        foreach ($lines as $line) {
            $cookie = [];
            // detect httponly cookies and remove #HttpOnly prefix
            if (substr($line, 0, 10) == '#HttpOnly_') {
                $line = substr($line, 10);
                $cookie['httponly'] = true;
            } else {
                $cookie['httponly'] = false;
            }

            // we only care for valid cookie def lines
            if (strlen($line) > 0 && $line[0] != '#' && substr_count($line, "\t") == 6) {
                // get tokens in an array
                $tokens = explode("\t", $line);
                // trim the tokens
                $tokens = array_map('trim', $tokens);
                // Extract the data
                $cookie['domain'] = $tokens[0]; // The domain that created AND can read the variable.
                $cookie['flag'] = $tokens[1]; // A TRUE/FALSE value indicating if all machines within a given domain can access the variable.
                $cookie['path'] = $tokens[2]; // The path within the domain that the variable is valid for.
                $cookie['secure'] = $tokens[3]; // A TRUE/FALSE value indicating if a secure connection with the domain is needed to access the variable.
                $cookie['expiration-epoch'] = $tokens[4]; // The UNIX time that the variable will expire on.
                $cookie['name'] = urldecode($tokens[5]); // The name of the variable.
                $cookie['value'] = urldecode($tokens[6]); // The value of the variable.
                // Convert date to a readable format
                $cookie['expiration'] = date('Y-m-d h:i:s', $tokens[4]);
                // Record the cookie.
                $cookies[] = $cookie;
            }
        }
        return $cookies;
    }

    //随机ip
    private static function randIp(): string
    {
        return mt_rand(0, 255) . '.' . mt_rand(0, 255) . '.' . mt_rand(0, 255) . '.' . mt_rand(0, 255);
    }

    /**
     * @param string $url
     * @param int $time
     * @return mixed
     */
    public function check(string $url, int $time = 0): mixed
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, trim($url));
        //伪造cookie
        //$cookie_file = dirname(__FILE__).'/cookie.txt';
        $cookie_file = tempnam("tmp", "cookie");
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file); //存储cookies
        if (preg_match("/^https/i", $url)) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);//SSL证书认证
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);//严格认证
            curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');//证书地址
        }
        if ($time > 0) {
            curl_setopt($ch, CURLOPT_TIMEOUT, $time);
        }

        //伪造浏览器数据
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11");
        //伪造ip
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FORWARDED-FOR:111.222.333.4', 'CLIENT-IP:111.222.333.4'));
        //伪造来源
        curl_setopt($ch, CURLOPT_REFERER, 'www.baidu.com'); // 看这里，你也可以说你从google来
        //禁止302跳转
        //curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        $result = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return array(
            'html' => $result,
            'status' => $status,
        );
    }

    /**
     * @param mixd $item
     * @return mixed
     */
    public function urlencode(mixd $item): mixed
    {
        static $search = array('%7E');
        static $replace = array('~');
        if (is_array($item)) {
            return array_map(array(&$this, 'urlencode'), $item);
        }
        if (is_scalar($item) === false) {
            return $item;
        }
        return str_replace($search, $replace, rawurlencode($item));
    }

    /**
     * URL Decode.
     *
     * @param mixed $item Item to url decode.
     *
     * @return string URL decoded string.
     */
    public static function urldecode(mixd $item): string
    {
        if (is_array($item)) {
            return array_map(self::urldecode, $item);
        }
        return urldecode($item);
    }

    /**
     * @param string $url
     * @return string
     */
    public static function filterUrl(string $url): string
    {
        //过滤中文
        preg_match_all("/[\x80-\xff]+/", $url, $tmp);
        if (!empty($tmp)) {
            foreach ($tmp[0] as $k => $v) {
                if ($v != '') {
                    $url = str_replace($v, urlencode($v), $url);
                }
            }
        }
        //过滤空格
        $url = str_replace(' ', '%20', $url);
        $url = str_replace(',', '%2C', $url);
        $url = str_replace('&amp;', '&', $url);
        return $url;
    }

    /**
     * 下载远程图片
     * @param string $url 图片的绝对url
     * @param string $filepath 文件的完整路径（包括目录，不包括后缀名,例如/www/images/test） ，此函数会自动根据图片url和http头信息确定图片的后缀名
     * @param int $timeout 超时时间
     * @param bool $redirect 是否下载302 或者301跳转后的页面
     * @return mixed 下载成功返回一个描述图片信息的数组，下载失败则返回false
     */
    public static function downloadImage(string $url, string $filepath, int $timeout = 120, bool $redirect = true): mixed
    {

        $url = self::filterUrl($url);

        //服务器返回的头信息
        $responseHeaders = array();
        //原始图片名
        $originalfilename = '';
        //图片的后缀名
        $ext = '';
        $ch = curl_init($url);
        //设置curl_exec返回的值包含Http头
        curl_setopt($ch, CURLOPT_HEADER, 1);

        if (preg_match("/^https/i", $url)) {

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);//SSL证书认证
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);//严格认证
            curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');//证书地址
        }

        //设置curl_exec返回的值包含Http内容
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //设置抓取跳转（http 301，302）后的页面
        # curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        if ($redirect) {
            //设置抓取跳转（http 301，302）后的页面
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        }

        // curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        //设置最多的HTTP重定向的数量
        curl_setopt($ch, CURLOPT_MAXREDIRS, 1);

        // 在尝试连接时等待的秒数
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        // 最大执行时间
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);

        //最重要的一步，手动指定Referer
        curl_setopt($ch, CURLOPT_REFERER, 'http://www.baidu.com');

        //服务器返回的数据（包括http头信息和内容）
        $html = curl_exec($ch);
        //获取此次抓取的相关信息
        $httpinfo = curl_getinfo($ch);
        curl_close($ch);


        if ($html !== false) {
            //分离response的header和body，由于服务器可能使用了302跳转，所以此处需要将字符串分离为 2+跳转次数 个子串
            $httpArr = explode("\r\n\r\n", $html, 2 + $httpinfo['redirect_count']);
            //倒数第二段是服务器最后一次response的http头
            $header = $httpArr[count($httpArr) - 2];
            //倒数第一段是服务器最后一次response的内容
            $body = $httpArr[count($httpArr) - 1];
            $header .= "\r\n";

            //获取最后一次response的header信息
            preg_match_all('/([a-z0-9-_]+):\s*([^\r\n]+)\r\n/i', $header, $matches);
            if (!empty($matches) && count($matches) == 3 && !empty($matches[1]) && !empty($matches[1])) {
                for ($i = 0; $i < count($matches[1]); $i++) {
                    if (array_key_exists($i, $matches[2])) {
                        $responseHeaders[$matches[1][$i]] = $matches[2][$i];
                    }
                }
            }
            //获取图片后缀名
            if (0 < preg_match('{(?:[^\/\\\\]+)\.(jpg|jpeg|gif|png|bmp)$}i', $url, $matches)) {
                $originalfilename = $matches[0];
                $ext = $matches[1];
            } else {
                if (array_key_exists('Content-Type', $responseHeaders)) {
                    if (0 < preg_match('{image/(\w+)}i', $responseHeaders['Content-Type'], $extmatches)) {
                        $ext = $extmatches[1];
                    }
                }
            }

            //保存文件
            if (!empty($ext)) {
                $filepath .= ".{$ext}";
                //如果目录不存在，则先要创建目录
                $local_file = fopen($filepath, 'w');

                if (false !== $local_file) {
                    if (false !== fwrite($local_file, $body)) {
                        fclose($local_file);
                        //$sizeinfo = [];
                        try {
                            $sizeinfo = getimagesize($filepath);
                        } catch (\Exception $e) {
                            echo $e->getMessage();
                            return ['filename' => ''];
                        }
                        $result = ['filepath' => realpath($filepath), 'width' => $sizeinfo[0] ?? 0, 'height' => $sizeinfo[1] ?? 0, 'orginalfilename' => $originalfilename, 'filename' => pathinfo($filepath, PATHINFO_BASENAME)];


                        return $result;
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param string $url
     * @return mixed
     */
    public static function gzipGet(string $url): mixed
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_ENCODING, 'gzip'); // 关键在这里
        $content = curl_exec($curl);
        curl_close($curl);
        return $content;
    }

    /**
     * @param string $uploadurl
     * @param string $file
     * @param string $file_name
     * @return mixed
     */
    public static function upload(string $uploadurl, string $file, string $file_name = 'upload_item'): mixed
    {
        $ch = curl_init();
        //加@符号curl就会把它当成是文件上传处理
        $data = array($file_name => '@' . realpath($file));
        curl_setopt($ch, CURLOPT_URL, $uploadurl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30000);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}