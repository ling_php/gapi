<?php

namespace gapi\lib;
/**
 * Class Tree
 * @package gapi\lib
 */
class Tree
{


    /**
     * Tree constructor.
     * @param array $categorys
     * @param string $id
     * @param string $pid
     * @param string $name
     */
    public function __construct(
        public array $categorys,
        public string $id = 'id',
        public string $pid = 'pid',
        public string $name = 'name',
        public string $child = 'child')
    {
    }

    /**
     * 获取所有子级
     * @param int $catid
     * @param array $childs
     * @return array
     */
    public function getAllChild(int $catid = 0, array &$childs = array()): array
    {
        if (!empty($this->categorys)) {
            foreach ($this->categorys as $key => $value) {
                if ($value[$this->pid] == $catid) {
                    $childs[] = $value;
                    $this->getAllChild($value[$this->id], $childs);
                }
            }
        }
        return $childs;
    }

    /**
     * 获取 子级分类
     * @param int $catid
     * @return array
     */
    public function getChild(int $catid = 0): array
    {
        if (!empty($this->categorys)) {
            foreach ($this->categorys as $key => $value) {
                if ($value[$this->pid] == $catid) {
                    $childs[] = $value;
                }
            }
        }
        return $childs;
    }

    /**
     * 获取所有子级id
     * @param int $catid
     * @return array
     */
    public function getChildIds(int $catid = 0): array
    {
        $childs = $this->getAllChild($catid);

        $ids = array();
        if (!empty($childs)) {
            foreach ($childs as $v) {
                $ids[] = $v[$this->id];
            }
        }
        return $ids;
    }

    public function getParentIds(int $catid)
    {
        $parents = $this->getAllParent($catid);
        $catids = [];
        if (!empty($parents)) {
            foreach ($parents as $value) {
                $catids[] = $value['id'];
            }
        }
        return $catids;
    }

    /**
     * 获取树形结构数据
     * @param int $catid
     * @param string $child
     * @param int $max_level
     * @return mixed
     */
    public function getTree(int $catid = 0, int $max_level = 5): mixed
    {

        $current_cate = [];
        if (!empty($this->categorys)) {

            foreach ($this->categorys as $key => $value) {
                if ($value[$this->pid] == $catid) {
                    $current_cate[] = $value;
                }
            }
        }

        $cates = array();
        if (!empty($current_cate)) {
            if (isset($current_cate[$this->id]) && $this->getLevel($current_cate[$this->id]) > $max_level) {
                return $cates;
            }
            foreach ($current_cate as $k => $v) {
                $v[$this->child] = $this->getTree($v[$this->id], $max_level);
                $cates[] = $v;
            }

            return $cates;
        }
        return null;
    }

    /**
     * 获取树形 二维数组
     * @param int $catid
     * @param int $pid
     * @return array
     */
    public function getTreeSelect(int $catid, int $pid = 0): array
    {
        $tree = $this->getTree($catid);
        return $this->getTreeList($tree, $pid);
    }

    /**
     * 获取树形 二维数组
     * @param array $tree
     * @param int $pid
     * @param array $lists
     * @return array
     */
    public function getTreeList(mixed $tree, int $pid = 0, array &$lists = array()): array
    {
        if (!empty($tree)) {
            $count = count($tree);
            foreach ($tree as $key => $value) {
                $cate = $this->getCate($value[$this->id]);
                $cate['tree_level'] = $position = $this->getLevel($cate['id'], $pid) - 1;
                if ($position >= 1) {
                    if ($key == $count - 1) {
                        $cate['title_show'] = str_repeat('&nbsp;&nbsp;', $position) . '└─';
                    } else {
                        $cate['title_show'] = str_repeat('&nbsp;&nbsp;', $position) . '├─';
                    }
                }
                // $cate['title_show'] = str_repeat($space,$position);

                $lists[] = $cate;
                if (!empty($value['child'])) {
                    $this->getTreeList($value['child'], $pid, $lists);
                }
            }
        }else{
            return [];
        }
        return $lists;
    }

    /**
     * 获取上一级
     * @param int $catid
     * @return array
     */
    public function getParent(int $catid = 0): array
    {
        $cate = $this->getCate($catid);
        return $this->getCate($cate[$this->pid]);
    }

    /**
     * 获取所有上一级
     * @param int $catid
     * @param int $pid
     * @param array $cates
     * @return array
     */
    public function getAllParent(int $catid = 0, int $pid = 0, array &$cates = array()): array
    {
        $cates[] = $cate = $this->getCate($catid);
        if ($cate) {
            if ($cate[$this->pid] == $pid) {
                return $cates;
            } else {
                return $this->getAllParent($cate[$this->pid], $pid, $cates);
            }
        } else {
            return [];
        }
    }

    /**
     * 获取当前分类的层级数
     * @param int $catid
     * @param int $pid
     * @return int
     */
    public function getLevel(int $catid, int $pid = 0): int
    {
        return count($this->getAllParent($catid, $pid));
    }

    /**
     * 获取分类
     * @param int $catid
     * @return array
     */
    public function getCate(int $catid): array
    {
        $cate = array();
        if (!empty($this->categorys)) {
            foreach ($this->categorys as $key => $value) {
                if ($value[$this->id] == $catid) {
                    $cate = $value;
                    break;
                }
            }
        }
        return $cate;
    }

}
