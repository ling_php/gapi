<?php

namespace app\common\lib;


use gapi\lib\File;
use gapi\Request;
use gapi\Response;

class Upload
{
    public static Request $request;
    public static Response $response;
    public $uploadPath = 'upload';

    public function __construct(Request $request, Response $response)
    {
        self::$request = $request;
        self::$response = $response;
    }


    public function file(string $filename): Response
    {
        $file = self::$request->file($filename);
        if (!($file instanceof File)) {
            return self::$response->fail()->setMsg('文件不存在');
        }

        if (!$file->validate([
            'size' => 1024 * 1024 * 100,
            'type' => [
                'text/plain',
                'application/zip', 'application/octet-stream',
                'application/x-zip-compressed',
                'image/png', 'image/jpg', 'image/jpeg', 'image/pjpeg', 'image/gif', 'image/webp', 'image/*',
                'audio/x-mpeg-3', 'audio/mpeg3', 'video/mpeg', 'video/x-mpeg', 'audio/mpeg',
            ],
            'ext' => ['txt', 'text', 'zip', 'rar', 'jpg', 'jpeg', 'png', 'gif', 'bmp', 'mp3'],
        ])->check()) {
            return self::$response->fail()->setMsg($file->getError());
        }

        $path = ROOT_PATH . DS . $this->uploadPath . DS . 'file';
        $path = self::$request->get('path', ROOT_PATH . DS . $path);
        $file = $file->move($path, replace: self::$request->get('rename', false));
        $image = "/$path/" . str_replace('\\', '/', $file->getSaveName());

        return self::$response->success()->setMsg('上传成功')->setData(['name' => $file->getFilename(), 'url' => self::$request->domain() . $image, 'file' => $image, 'hash' => $file->hash()]);
    }

    public function image(string $filename): Response
    {
        $file = self::$request->file($filename);
        if (!($file instanceof File)) {
            return self::$response->fail()->setMsg('文件不存在');
        }

        if (!$file->validate([
            'size' => 1024 * 1024 * 20, //20MB
            'type' => ['image/png', 'image/jpg', 'image/jpeg', 'image/pjpeg', 'image/gif', 'image/webp', 'image/*',],
            'ext' => ['gif', 'jpg', 'png', 'bmp'],
        ])->check()) {
            return self::$response->fail()->setMsg($file->getError());
        }
        $savename = true;
        if (self::$request->get('rename', 0, 'number') == 1) {
            $savename = $file->getInfo()['name'];
        }

        $path = ROOT_PATH . DS . $this->uploadPath . DS . 'image';
        $path = self::$request->get('path', ROOT_PATH . DS . $path);
        $file = $file->move($path, $savename, self::$request->get('rename', false));

        $image = "/$path/" . str_replace('\\', '/', $file->getSaveName());
        $info = getimagesize($file->getFilePath());
        return self::$response->success()->setMsg('上传成功')->setData(['name' => $file->getFilename(), 'url' => self::$request->domain() . $image, 'file' => $image, 'hash' => $file->hash(), 'info' => $info]);
    }

    public static function saveName(): string
    {
        return date('Ymd') . DS . md5(microtime(true));
    }

    public function base64ImageSave(string $content)
    {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $content, $result)) {
            $ext = $result[2];
            $upload_path = '/upload/image/';
            $file = $upload_path . self::saveName();
            $save_file = ROOT_PATH . str_replace('/', DS, $file);
            $save_path = dirname($save_file);
            if (!is_dir($save_path)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($save_path, 0777, true);
            }
            $save_file = $save_file . ".{$ext}";
            $upload_url = str_replace('\\', '/', $file) . ".{$ext}";
            if (file_put_contents($save_file, base64_decode(str_replace($result[1], '', $content)))) {
                return ['url' => self::$request->domain() . $upload_url];
            }
        }
        return [];
    }

}