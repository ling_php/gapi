<?php

namespace gapi;

use gapi\lib\Logger;

class Loader
{

    public static array $versions = [];
    public static array $models = [];
    public static array $class = [];
    public static array $versionFiles = [];

    # app mvc
    private static function app(string $class): void
    {
        $class = substr($class, 4);
        $file = str_replace('\\', DS, $class) . '.php';
        //迭代版本
        foreach (self::version() as $version) {
            $class_file = APP_PATH . DS . $version . DS . $file;
            if (is_file($class_file)) {
                require $class_file;
                Logger::debug('APP 加载文件:' . $class_file);
                return;
            }
        }
    }

    public static function autoload(): void
    {
        # app
        spl_autoload_register(['\\gapi\\Loader', 'app']);
    }

    public static function register(string $class, string $method)
    {
        spl_autoload_register([$class, $method]);
    }

    public static function autoRoot($class)
    {
        $class_file = ROOT_PATH . DS . $class . '.php';
        $class_file = str_replace('\\', '/', $class_file);
        if (is_file($class_file)) {
            require $class_file;
            \gapi\lib\Logger::debug(static::class . ' 加载文件:' . $class_file);
            return;
        }
    }

    # 获取版本列表
    public static function version(string $current = ''): array
    {
        $current = $current == '' ? APP_VERSION : $current;
        $versions = [];
        if (!self::$versions) {
            $version_list = dir_list_one(APP_PATH . DS);
            sort($version_list);
            foreach ($version_list as $version) {
                if (preg_match("/v\d\.\d\.\d|application/", $version)) {
                    $versions[] = $version;
                    if ($version == $current) {
                        break;
                    }
                }
            }
            self::$versions = array_reverse($versions);
        }

        return self::$versions;
    }

    public static function system(string $file): mixed
    {
        $file = ROOT_PATH . DS . 'config' . DS . $file;
        if (is_file($file)) {
            return include $file;
        }
        return [];
    }

    public static function file(string $file, string $version = '', mixed $info = []): mixed
    {
        $version = $version == '' ? APP_VERSION : $version;
        //迭代版本
        foreach (self::version($version) as $version) {
            $version_file = APP_PATH . DS . $version . DS . $file;
            //echo $version_file."\n";
            if (is_file($version_file)) {
                return include $version_file;
            }
        }
        return [];
    }

    public static function filePath(string $file, string $version = ''): mixed
    {
        $version = $version == '' ? APP_VERSION : $version;
        //迭代版本
        foreach (self::version($version) as $version) {
            $version_file = APP_PATH . DS . $version . DS . $file;
            if (is_file($version_file)) {
                return $version_file;
            }
        }
        return APP_PATH . DS . $version . DS . $file;;
    }


    public static function configFile(string $file, string $path = '', string $version = '', mixed $info = [], string $module = ''): mixed
    {
        return self::file(($module == '' ? MODULE_NAME : $module) . DS . 'config' . ($path == '' ? '' : DS . $path) . DS . $file, $version, $info);
    }

    public static function configFilePath(string $file, string $path = '', string $module = ''): mixed
    {
        return VERSION_PATH . DS . ($module == '' ? MODULE_NAME : $module) . DS . 'config' . ($path == '' ? '' : DS . $path) . DS . $file;
    }

    public static function moduleFile(string $file, string $path = '', string $module = '', string $version = '', mixed $info = []): mixed
    {
        $module = $module == '' ? MODULE_NAME : $module;
        return self::file($module . ($path == '' ? '' : DS . $path) . DS . $file, $version, $info);
    }

    public static function moduleFilePath(string $file, string $path = '', string $module = ''): mixed
    {
        $module = $module == '' ? MODULE_NAME : $module;
        return VERSION_PATH . DS . $module . ($path == '' ? '' : DS . $path) . DS . $file;
    }

    public static function systemfile(string $file)
    {
        $file = ROOT_PATH . DS . 'config' . DS . $file;
        if (is_file($file)) {
            return $file;
        }
        return '';
    }

    public static function versionfile(string $file, string $version = ''): string
    {
        $version = $version == '' ? APP_VERSION : $version;
        if (isset(self::$versionFiles[$version . '_' . $file])) return self::$versionFiles[$version . '_' . $file];
        $current_version_file = '';
        //迭代版本
        foreach (self::version($version) as $version) {
            $version_file = APP_PATH . DS . $version . DS . $file;
            if (is_file($version_file)) {
                $current_version_file = $version_file;
                break;
            }
        }
        return self::$versionFiles[$version . '_' . $file] = $current_version_file;
    }


    public static function controllers(string $current_version = ''): array
    {
        $versions = array_merge([$current_version], self::version($current_version));
        $versions = array_reverse($versions);
        $modules = self::file('module.php');
        $controllers = [];
        foreach ($modules as $module) {
            foreach ($versions as $version) {
                //获取控制器目录
                $module_path = APP_PATH . DS . $version . DS . $module . DS . 'controller';
                //获取控制器列表
                $files = dir_list($module_path);
                foreach ($files as $file) {
                    $controllers[$module . '/' . substr(basename($file), 0, -4)] = $file;
                }
            }
        }
        //sort($controllers);
        return $controllers;
    }


    public static function model(string $table): Model
    {
        if (!isset(self::$models[$table])) {
            self::$models[$table] = new Model($table);
        }
        return self::$models[$table];
    }

    public static function class(string $class)
    {
        if (!isset(self::$class[$class])) {
            self::$class[$class] = new $class();
        }
        return self::$class[$class];
    }

    public static function mergeFile(string $file, array &$data): array
    {

    }

}