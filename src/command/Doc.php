<?php

namespace gapi\command;

use gapi\Command;
use gapi\lib\Logger;
use gapi\Loader;

class Doc
{

    public static $class = \gapi\attribute\Doc::class;

    public static function execute(?array $params, Command $output): void
    {
        echo "生成文件\n";
//        foreach(Loader::version() as $version){
//            self::update($version);
//        }
        echo APP_PATH;
        echo "\n\n";
        //$versions = Loader::version();
        $version = $params[0] ?? APP_VERSION;
        if ($version != '') {
            self::update($version);
        }

    }


    public static function update(string $version, bool $flag = true): string
    {
        $content = '';
        $controllers = Loader::controllers($version);
        $controller_items = [];
        $action_items = [];
        foreach ($controllers as $controller => $file) {

            Logger::debug(__CLASS__ . '@' . __FUNCTION__ . " => {$file}");
            $class = '\\app\\' . implode('\\controller\\', explode('/', $controller));
            //获取路由注解
            $controller_items = array_merge($controller_items,self::controller($class));
            $action_items = array_merge($action_items,self::controllerAction($class));

        }
        $content .= implode('',$action_items).implode('',$controller_items);

        $path = APP_PATH . DS . $version . DS;
        $file = $path . 'doc.php';
        if (!is_dir($path)) {
            echo "{$path}目录不存在";
            return $file;
        }

        file_put_contents($file, $content);
        if ($flag) echo "{$content}\n{$file} 生成成功\n";
        return $file;
    }

    /**
     * 获取控制器路由注解
     * @param string $class
     * @return array
     * @throws \ReflectionException
     */
    public static function controller(string $class): array
    {
        $items = [];
        $controller = new \ReflectionClass($class);
        $classAttributes = $controller->getAttributes();
        if($classAttributes){
            foreach ($classAttributes as $classAttribute){
                $item = $classAttribute->newInstance();
                if($item instanceof \gapi\attribute\Doc){
                    $items[] = self::createControllerItem($classAttribute->newInstance(),$class);
                }
            }
        }
        return $items;
    }
    /**
     * 获取控制器路由注解
     * @param string $class
     * @return array
     * @throws \ReflectionException
     */
    public static function controllerAction(string $class): array
    {
        $items = [];
        $controller = new \ReflectionClass($class);
        $methods = $controller->getMethods(\ReflectionMethod::IS_PUBLIC);
        if ($methods) {
            foreach ($methods as $method) {

                $attributes = $method->getAttributes(self::$class);
                if ($attributes) {
                    foreach ($attributes as $attribute) {
                        $item = $attribute->newInstance()->setHandler($method);
                        $items[] = self::createActionItem($item);
                    }
                }
            }
        }
        return $items;
    }

    public static function createActionItem(\gapi\attribute\Doc $item)
    {
        $class = explode('\\', $item->handler->class);
        $action = $class[1] . '/' . $class[3] . '/' . $item->handler->name;
        $pattern = [];
        if ($item->pattern) {
            foreach ($item->pattern as $name => $p) {
                $pattern[] = "'{$name}'=>'{$p}'";
            }
        }
        $methods = explode('|', $item->methods);
        $content = '';
        foreach ($methods as $method) {
            $content .= 'Route::' . $method . '([\'' . implode("','", $item->path) . '\'],\'' . $action . '\',[' . implode(',', $pattern) . ']);' . "\n";
        }
        return $content;
    }

    public static function createControllerItem(\gapi\attribute\Doc $item, string $class = '')
    {

        $class = explode('\\', $class);
        $class = $class[2] . '/' . $class[4] ;

        $methods = explode('|', $item->methods);
        $content = '';
        foreach ($methods as $method) {
            $content .= 'Route::controller(\''.strtolower($class).'\');' . "\n";
        }

        return $content;
    }
}