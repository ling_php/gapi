<?php

namespace gapi\command;


use gapi\Command;
use gapi\Debug;
use gapi\lib\Logger;

class Test
{

    public static int $i = 0;
    public static int $j = 0;

    public static function execute(?array $params, Command $output): void
    {
        define('RUNTIME_PATH', TEST_PATH . DS . RUNTIME_PATH_NAME);

        $class_name = isset($params[0]) ? $params[0] : null;
        $func_name = isset($params[1]) ? $params[1] : null;

        # 获取测试文件
        $path = ROOT_PATH . DS . 'test';
        $lists = glob("{$path}/*.php");

        $start_time = microtime(true);

        if ($class_name != null) {

            require $path . DS . $class_name . '.php';

            if ($func_name != null) {
                $class_name = '\\test\\' . $class_name;
                $method_name = $func_name;
                self::runFunc($class_name, $method_name);
            } else {
                $class = '\\test\\' . $class_name;
                self::runFile($class);
            }


        } else {

            if (!empty($lists)) {
                foreach ($lists as $file) {
                    require $file;
                    $class_name = explode('.', basename($file))[0];
                    $class = '\\test\\' . $class_name;

                    self::runFile($class);
                }
            }
        }


        $end_time = microtime(true);
        $use_time = sprintf("%.3f", $end_time - $start_time);
        $msg = "共执行" . self::$i . "个测试文件，" . self::$j . "个方法，用时 {$use_time} 秒";
        $output->writeln($msg);
        Logger::info($msg);
        Debug::end();

    }

    protected static function runFile($class)
    {
        $class = new \ReflectionClass($class);
        self::$i++;

        foreach ($class->getMethods() as $method) {
            $class_name = $method->class;
            $method_name = $method->name;

            self::runFunc($class_name, $method_name);
        }
    }

    protected static function runFunc($class_name, $method_name)
    {

        $stime = microtime(true);
        $class_name::$method_name();
        $etime = microtime(true);
        $msg = '\\' . $class_name . '::' . $method_name . '()[' . (sprintf("%.3f", $etime - $stime)) . '] is ok.';
        echo $msg . "\n";
        Logger::info($msg);
        self::$j++;
    }
}