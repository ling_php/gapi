<?php

namespace gapi\command;

use gapi\Command;

class InitData
{

    public static function execute(?array $params,Command $output): void
    {

        echo '初始化数据';
        echo model('cms/article')->truncate();
        echo "\n";
        echo model()->getLastSql();
        model('cms/category')->truncate();
        echo "\n";
        echo model('cms/category')->getLastSql();
        model('cms/tags')->truncate();
        echo "\n";
        echo model('cms/tags')->getLastSql();
        model('cms/content_tag')->truncate();
        echo "\n";
        echo model('cms/content_tag')->getLastSql();


    }

}