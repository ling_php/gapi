<?php

namespace gapi\command;

use gapi\Command;
use gapi\Loader;

class DefaultData
{

    public static function execute(?array $params, Command $output): void
    {
        echo "生成默认数据文件\n";
        echo APP_PATH;
        echo "\n\n";
        //$versions = Loader::version();
        $module = isset($params[0]) ? $params[0] : '';
        $table = isset($params[1]) ? $params[1] : '';
        if(!defined('COLUMNS_PATH')){
            define('COLUMNS_PATH',VERSION_PATH.DS.COLUMNS_PATH_NAME);
        }

        if($table == ''){
            $path = VERSION_PATH.DS.$module.DS.'demo'.DS;
            $lists = dir_list($path);
            if($lists){
                foreach ($lists as $v){
                    $_table  = explode('.',basename($v))[0];
                    self::update($module,$_table);
                }
            }
        }else{
            self::update($module,$table);
        }

    }


    public static function update(string $module,string $table): void
    {
        #echo $module.'-'.$table;
        $lists = model($table)->select();
        $pk = model($table)->pk();

        $file = VERSION_PATH.DS.$module.DS.'demo'.DS.$table.'.php';
        if($lists){
            foreach ($lists as $key=>$value){
                unset($lists[$key][$pk]);
            }
            file_put_contents($file,'<?php return '.var_export($lists,1).';');
        }

        echo "生成文件:{$file}\n";
    }
}