<?php

namespace gapi\command;

use gapi\Command;
use gapi\Config;
use gapi\database\Db;
use gapi\database\Query;
use gapi\lib\Logger;
use gapi\Loader;

class Fields
{
    public static Db $db;

    public static function execute(?array $params, Command $output): void
    {
        echo "============生成字段===========\n";
        $versions = Loader::version();
        $version = isset($params[0]) ? $params[0] : '';
        if ($version == '') {
            exit('执行语句：php build fields vx.x.x');
        }
        if (in_array($version, $versions)) {
            self::update($version);
        }


    }

    public static function update(string $version): void
    {
        define('RUNTIME_PATH', APP_PATH . DS . $version . DS . RUNTIME_PATH_NAME);
        $database = Config::database();
        self::$db = $db = Db::connect($database);
        $tables = $db->query((new Query())->tables());


        if ($tables) {
            foreach ($tables as $table) {
                $fields = $db->query((new Query())->columns($table['Name'], full: 'full'));
                self::create($fields, preg_replace("/^{$database['prefix']}/", '', $table['Name'], 1), $table);
            }
        }
        echo "============生成完成[" . count($tables) . "]个表===========";
    }

    public static function create(array $fields, string $table, array $tableinfo): void
    {
        //$columns_file = COLUMNS_PATH . DS . "t_{$table}.php";
        $columns_file = self::$db->formatColumns($table, $fields, $tableinfo);
        echo "生成文件：{$columns_file}\n";
        Logger::info(__CLASS__ . '@' . __FUNCTION__ . ":{$columns_file}");
    }

}