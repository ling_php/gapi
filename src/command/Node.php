<?php

namespace gapi\command;

use gapi\Command;
use gapi\Config;

class Auth
{

    public static function execute(?array $params, Command $output): void
    {
        echo "更新菜单节点\n";
        //$versions = Loader::version();
        $version = $params[0] ?? APP_VERSION;
        $module = isset($params[1]) ? $params[1] : null;
        if ($version != '' && $module != null) {
            self::update($version, $module);
        }

    }

    public static function update(string $version, string $module): void
    {
        //$modules = Config::module();

        $menu = self::getData($version, [$module], 'menu');
        self::updateMenu($menu);
        echo "更新菜单权限完毕\n";
    }

    public static function getData(string $version, array $modules, string $type): array
    {
        $data = [];
        $path = APP_PATH . DS . $version . DS;
        foreach ($modules as $module) {
            $file = $path . $module . DS . 'config' . DS . $type . '.php';
            if (is_file($file)) {
                echo $file . "\n";
                $dataFile = include $file;
                $data = self::mergeMenu($data, $dataFile);
            }
        }

        if ($data) {
            foreach ($data as $key => $value) {
                $data[$key]['listorder'] = isset($value['listorder']) ? $value['listorder'] : 0;
            }
        }

        $data = list_sort_by($data, 'listorder', 'desc');
        return $data;
    }

    public static function mergeMenu(array $menu, array $data): array
    {
        if ($data) {
            foreach ($data as $key => $value) {
                $menu[] = $value;
            }
        }
        return $menu;
    }


    public static function updateMenu(array $menu)
    {
        $menuModel = model('menu');
        $menuModel->truncate();
        if ($menu) {
            foreach ($menu as $value) {
                $id1 = $menuModel->insert(self::parseMenuValue($value));
                if (isset($value['subnav']) && !empty($value['subnav'])) {
                    foreach ($value['subnav'] as $val) {
                        $val['pid'] = $id1;
                        $id2 = $menuModel->insert(self::parseMenuValue($val));

                        if (isset($val['subnav']) && !empty($val['subnav'])) {
                            foreach ($val['subnav'] as $v) {
                                $v['pid'] = $id2;
                                $id3 = $menuModel->insert(self::parseMenuValue($v));
                            }
                        }

                    }
                }

            }
        }
    }


    public static function parseMenuValue(array $value): array
    {
        return [
            'listorder' => isset($value['listorder']) ? $value['listorder'] : 0,
            'pid' => isset($value['pid']) ? $value['pid'] : 0,
            'title' => $value['title'],
            'icon' => isset($value['icon']) ? $value['icon'] : '',
            'url' => isset($value['page']) ? $value['page'] : '',
            'node' => isset($value['node']) ? $value['node'] : '',
            'bind' => isset($value['bind']) ? serialize($value['bind']) : '',
            'updatetime' => NOW_TIME,
            'is_show' => isset($value['show']) ? $value['show'] : 1,
            'is_open' => isset($value['open']) ? $value['open'] : 0,
        ];
    }
}