<?php

namespace gapi\command;

use gapi\Command;
use gapi\Config;

# php build table [module] [version]
class Table
{
    public static string $module = '';

    public static function execute(?array $params, Command $output): void
    {

        self::$module = isset($params[0]) ? $params[0] : '';
        $version = isset($params[1]) ? $params[1] : Config::system()['app_version'];
        echo "生成表 - version：[{$version}]\n";
//        $versions = Loader::version();
//        if (in_array($version, $versions)) {
//            self::update($version);
//        }
        if($version!=''){
            self::update($version);
        }

        # 更新字段
        Fields::execute([$version],$output);

    }

    public static function update(string $version): void
    {
        $modules = Config::module();
        if (self::$module != '') {
            $modules = [self::$module];
        }
        foreach ($modules as $module) {
            $table_path = APP_PATH . DS . $version . DS . $module . DS . 'table';
            $files = dir_list($table_path);
            if (!empty($files)) {
                foreach ($files as $file) {

                    self::updateTable($file);
                }
            }
        }
    }

    public static function updateTable(string $file): void
    {


        $config = include $file;
        $table = pathinfo($file)['filename'];
        $Model = model();
        $ret = $Model->query("SHOW TABLES LIKE '{$Model->prefix}{$table}'");
        if ($ret) {
            $update_sql = (new \gapi\lib\Table($table, $config))->updateQuery();
            if ($update_sql) {
                foreach ($update_sql as $sql) {
                    $Model->exec($sql);
                    echo $sql;
                    echo "\n";
                }
            }
        } else {
            $create_sql = (new \gapi\lib\Table($table, $config))->createQuery();
            echo $Model->exec($create_sql);
            echo $create_sql;
            echo "\n";
        }

    }


}