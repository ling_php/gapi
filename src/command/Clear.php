<?php

namespace gapi\command;

use gapi\Command;
use gapi\Loader;

class Clear
{

    public static function execute(?array $params,Command $output): void
    {
        foreach (Loader::version() as $version) {
            $path = APP_PATH.DS.$version.DS.RUNTIME_PATH_NAME;
            dir_delete($path);
            echo "{$path} 清除成功！\n";
        }
        $path = TEST_PATH.DS.RUNTIME_PATH_NAME;
        dir_delete($path);
        echo "{$path} 清除成功！\n";
    }

}