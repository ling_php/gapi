<?php

namespace gapi\command;

use gapi\Command;
use gapi\Config;


# php build table [module] [version]
class Sql
{
    public static string $module = '';

    public static function execute(?array $params, Command $output): void
    {
        self::$module = isset($params[0]) ? $params[0] : '';
        $version = isset($params[1]) ? $params[1] : Config::system()['app_version'];
        if(APP_VERSION!=$version){
            echo '执行版本和系统版本必须一致';
            return;
        }
        echo "执行版本[".APP_VERSION."]\n";
        echo "创建新版本[{$version}]\n";
//        $versions = Loader::version();
//        if (in_array($version, $versions)) {
//            self::update($version);
//        }
        if($version!=''){
            self::update($version);
        }
    }

    public static function update(string $version): void
    {
        define('RUNTIME_PATH', APP_PATH . DS . $version . DS . RUNTIME_PATH_NAME);

        $modules = Config::module();
        if (self::$module != '') {
            $modules = [self::$module];
        }
        foreach ($modules as $module) {
            $table_path = APP_PATH . DS . $version . DS . $module . DS . 'demo';
            $files = dir_list($table_path);
            if (!empty($files)) {
                foreach ($files as $file) {
                    echo $file;
                    echo "\n";
                    self::updateTable($file);
                }
            }
        }
    }

    public static function updateTable(string $file): void
    {

        $list = include $file;

        $table = explode('.',basename($file))[0];
        $model = model($table);
        if($model->count()==0){
            if($list){
                foreach($list as $value){
                    $model->insert($value);
                }
            }
        }
    }


}