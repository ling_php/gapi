<?php

namespace gapi\command;

use gapi\Command;
use gapi\Loader;

class CreateData
{

    public static function execute(?array $params, Command $output): void
    {
        if(!defined('COLUMNS_PATH')){
            define('COLUMNS_PATH',VERSION_PATH.DS.COLUMNS_PATH_NAME);
        }
        echo "生成默认数据文件\n";
        //$versions = Loader::version();
        $table = isset($params[0]) ? $params[0] : '';
        $path = isset($params[1]) ? $params[1] : '';

        echo $path;
        echo "\n\n";
        $lists = model($table)->select();
        $pk = model($table)->pk();
        if ($lists) {
            foreach ($lists as $key => $value) {
                unset($lists[$key][$pk]);
            }
            $file = $path . DS . 'demo' . DS . $table . '.php';
            file_put_contents($file, '<?php return ' . var_export($lists, 1) . ';');
            echo "生成文件:{$file}\n";
        }

    }

}