<?php

namespace gapi\command;

use gapi\Command;
use gapi\Loader;

class Module
{

    public static function execute(?array $params, Command $output): void
    {
        echo "更新模块\n";

        $module = isset($params[0]) ? $params[0] : '';
        $table = isset($params[1]) ? $params[1] : '';
        $action = isset($params[2]) ? $params[2] : 'all';

        self::update($module, $table, $action);

    }

    public static function update(string $module, string $table, string $action)
    {
        $fields = Loader::moduleFile($table . '.php', 'table', $module)['fields'];

        if ($action == 'all' || $action == 'lists') {

            $list_tpl_file = Loader::moduleFilePath('lists.php.tpl', 'tpl', 'develop');
            $lists = [];
            if ($fields) {
                foreach ($fields as $field => $value) {
                    $item = self::parseListsItem($value, $field);
                    if ($item) {
                        $lists[] = $item;
                    }
                }
            }
            $content = file_get_contents($list_tpl_file);
            $content = str_replace('{content}', implode("\n", $lists), $content);
            $lists_file = Loader::moduleFilePath($table . '.php', 'config/lists', $module);
            if (!is_dir(dirname($lists_file))) {
                @mkdir(dirname($lists_file), 0777, 1);
            }

            file_put_contents($lists_file, $content);
            echo "生成文件：{$lists_file}\n";
        }
        if ($action == 'all' || $action == 'action') {
            $action_tpl_file = Loader::moduleFilePath('action.php.tpl', 'tpl', 'develop');
            $actions = [];
            if ($fields) {
                foreach ($fields as $field => $value) {
                    $actionItem = self::parseActionItem($value, $field);
                    if ($actionItem) {
                        $actions[] = $actionItem;
                    }
                }
            }
            $content = file_get_contents($action_tpl_file);
            $content = str_replace('{content}', implode("\n", $actions), $content);
            $action_file = Loader::moduleFilePath($table . '.php', 'config/action', $module);
            if (!is_dir(dirname($action_file))) {
                @mkdir(dirname($action_file), 0777, 1);
            }
            file_put_contents($action_file, $content);
            echo "生成文件：{$action_file}\n";
        }
    }

    public static function parseActionItem(array $value, string $field): string
    {
        if (isset($value['auto_increment']) && $value['auto_increment']) {
            return <<<DDL
\$data[] = [['name' => '{$field}','type' => 'hidden','value' => '','block'=>false]];
DDL;
        }

        if (strpos($field, 'thumb') !== false || strpos($field, 'image') !== false) {
            return <<<DDL
\$data[] = [
    [
        'title' => '{$value['comment']}', 
        'type' => 'upload', 
        'uploadtype' => 'image', 
        'limit' => 1, 
        'name' => '{$field}', 
    ]
];
DDL;
        }

        if (strpos($field, 'time') !== false) {
            return <<<DDL
\$data[] = [
    [
        'title' => '{$value['comment']}', 
        'type' => 'datetime',
        'value' => '',
        'name' => '{$field}',
        'style' => 'width:100%; max-width:300px;',
        'callback' => function (array \$data, string \$field) {
            return \$data[\$field] > 0 ? date('Y-m-d H:i:s', \$data[\$field]) : date('Y-m-d H:i:s');
        },
    ]
];

DDL;
        }
        // 文本
        if (strpos($value['type'], 'text') !== false) {
            return <<<DDL
\$data[] = [
    [
    'name' => '{$field}', 
    'title' => '{$value['comment']}', 
    'type' => 'textarea',
    'value' => '',
    'placeholder' => '',
    ]
];
DDL;
        }


        // decimal
        if (strpos($value['type'], 'tinyint') !== false) {
            return <<<DDL
\$data[] = [
    [
        'name' => '{$field}', 
        'title' => '{$value['comment']}', 
        'type' => 'select',
        'value' => '',
        'option' => ['未审核', '已审核'],
    ]
];
DDL;
        }


        return <<<DDL
\$data[] = [
    [
        'name' => '{$field}', 
        'title' => '{$value['comment']}', 
        'type' => 'text',
        'value' => '',
    ]
];
DDL;
    }

    public static function parseListsItem(array $value, string $field): string
    {
        if (isset($value['auto_increment'])) {
            return <<<DDL
\$data['{$field}'] = [
    'title' => '{$value['comment']}',
    'width' => 100,
    'sort' => true,
    'align' => 'center',
    'search_form' => [
        'title' => '{$value['comment']}',
        'type' => 'text',
        'name' => '{$field}',
        'placeholder' => '请输入{$value['comment']}',
    ]
];
DDL;
        }

        if (strpos($field, 'thumb') !== false || strpos($field, 'image') !== false) {
            return <<<DDL
\$data['{$field}'] = [
    'title' => '{$value['comment']}',
    'width' => 300,
    'align' => 'left',   
];
DDL;
        }

        if (strpos($field, 'time') !== false) {
            return <<<DDL
\$data['{$field}'] = [    
    'width'=> 200,
    'title'=>'{$value['comment']}',
    'sort' => 1,
    'callback' => function(\$data,\$field){
        return date('Y-m-d H:i',(int)\$data[\$field]);
    },
];
DDL;
        }

        if (in_array(substr($value['type'], 0, 3), ['var', 'dec', 'int'])) {
            return <<<DDL
\$data['{$field}'] = [
    'width'=>100,
    'sort' => true,
    'edit' => true,
    'title'=>'{$value['comment']}',
    'search' => true,
    'search_quick' => true,
    'search_form' => [
        'title' => '{$value['comment']}',
        'type' => 'text',
        'name' => '{$field}',
        'placeholder' => '请输入{$value['comment']}',
    ]
];
DDL;
        }

        if (strpos($value['type'], 'tinyint') !== false) {
            return <<<DDL
\$data['{$field}'] = [
    'width' => 120,
    'title' => '启用',
    'search_form' => [
        'title' => '启用',
        'type' => 'select',
        'value' => '',
        'option' => ['未启用', '启用'],
        'name' => '{$field}',
        'default' => '请选择'
    ],
    'list_type' => 'switch',
    'tplid' => '#checkbox{$field}Tpl',
    'tpl' => \app\common\lib\Lists::scriptTpl(
        id: 'checkbox{$field}Tpl',
        content: \app\common\lib\Lists::checkbox('{$field}', '启用', 'id'),
    ),
];
DDL;
            // 字符串
            if (strpos($value['type'], 'int') !== false) {
                return <<<DDL
\$data['{$field}'] =  [
    'width'=>100,
    'title'=>'{$value['comment']}',
];
DDL;
            }
        }

        return '';
    }
}