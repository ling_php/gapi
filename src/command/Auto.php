<?php

namespace gapi\command;

use gapi\Command;
use gapi\Loader;

class Auto
{

    public static function execute(?array $params, Command $output): void
    {
        echo "自动任务\n";
        $type = $params[0];
        $versions = Loader::version();

        switch ($type) {
            case 'route':
                echo 'route:';
                foreach ($versions as $version) {
                    self::route($version);
                }
                break;
            case 'fields':
                echo 'fields:';
                foreach ($versions as $version) {
                    self::fields($version);
                }
                break;
            default:
                break;
        }
    }

    public static function route(string $version): void
    {
        // header('Content-Type: text/html; charset=gbk');
        $cmd = "php build route {$version}";
        #$pipes = array();
        $out1 = "";
        system($cmd, $out1);
        // iconv('gb2312','utf-8',$out1);
    }

    public static function fields(string $version): void
    {
        $cmd = "php build fields {$version}";
        $out1 = "";
        system($cmd, $out1);
    }

}