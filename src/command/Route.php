<?php

namespace gapi\command;

use gapi\cache\Cache;
use gapi\Command;
use gapi\lib\Logger;
use gapi\Loader;

class Route
{

    public static $class = \gapi\attribute\Route::class;
    public static $urls = [];

    public static function execute(?array $params, Command $output): void
    {
        echo "生成路由规则\n";
//        foreach(Loader::version() as $version){
//            self::update($version);
//        }
        echo APP_PATH;
        echo "\n\n";
        //$versions = Loader::version();
        $version = $params[0] ?? APP_VERSION;
        if (!defined('RUNTIME_PATH')) {
            define('RUNTIME_PATH', APP_PATH . DS . $version . DS . RUNTIME_PATH_NAME);
        }
        if ($version != '') {
            self::update($version);
        }

    }


    public static function update(string $version, bool $flag = true): string
    {
        $content = '<?php
namespace app;
use gapi\Route;
';
        $controllers = Loader::controllers($version);
        $controller_routes = [];
        $action_routes = [];
        foreach ($controllers as $controller => $file) {
            Logger::debug(__CLASS__ . '@' . __FUNCTION__ . " => {$file}");
            $class = '\\app\\' . implode('\\controller\\', explode('/', $controller));
            //获取路由注解
            $controller_routes = array_merge($controller_routes, self::controller($class));
            $action_routes = array_merge($action_routes, self::controllerAction($class));
        }
        $content .= implode('', $action_routes) . implode('', $controller_routes);

        $path = APP_PATH . DS . $version . DS;
        $file = $path . 'router.php';
        if (!is_dir($path)) {
            echo "{$path}目录不存在";
            return $file;
        }

        file_put_contents($file, $content);
        if ($flag) echo "{$content}\n{$file} 生成成功\n";

        $cache = Cache::create('file');
        $cache->set('urls', self::$urls);

        return $file;
    }

    /**
     * 获取控制器路由注解
     * @param string $class
     * @return array
     * @throws \ReflectionException
     */
    public static function controller(string $class): array
    {
        $items = [];
        $controller = new \ReflectionClass($class);
        $classAttributes = $controller->getAttributes();
        if ($classAttributes) {
            foreach ($classAttributes as $classAttribute) {
                $item = $classAttribute->newInstance();
                if ($item instanceof \gapi\attribute\Route) {
                    $items[] = self::createControllerItem($classAttribute->newInstance(), $class);
                }
            }
        }
        return $items;
    }

    /**
     * 获取控制器路由注解
     * @param string $class
     * @return array
     * @throws \ReflectionException
     */
    public static function controllerAction(string $class): array
    {
        $items = [];
        $controller = new \ReflectionClass($class);
        $methods = $controller->getMethods(\ReflectionMethod::IS_PUBLIC);
        if ($methods) {
            foreach ($methods as $method) {
                $attributes = $method->getAttributes(self::$class);
                if ($attributes) {
                    foreach ($attributes as $attribute) {
                        $item = $attribute->newInstance()->setHandler($method);
                        $items[] = self::createActionItem($item);
                    }
                }
            }
        }
        return $items;
    }

    public static function createActionItem(\gapi\attribute\Route $route)
    {
        $class = explode('\\', $route->handler->class);
        $action = $class[1] . '/' . $class[3] . '/' . $route->handler->name;
        $pattern = [];
        if ($route->pattern) {
            foreach ($route->pattern as $name => $p) {
                $pattern[] = "'{$name}'=>'{$p}'";
            }
        }
        $methods = explode('|', $route->methods);
        $content = '';
        foreach ($methods as $method) {
            $content .= 'Route::' . $method . '([\'' . implode("','", $route->path) . '\'],\'' . $action . '\',[' . implode(',', $pattern) . ']);' . "\n";
        }

        self::$urls[strtolower($action)] = $route->path[0];

        return $content;
    }

    public static function createControllerItem(\gapi\attribute\Route $route, string $class = '')
    {

        $class = explode('\\', $class);
        $class = $class[2] . '/' . $class[4];

        $methods = explode('|', $route->methods);
        $content = '';
        foreach ($methods as $method) {
            $content .= 'Route::controller(\'' . strtolower($class) . '\');' . "\n";
        }

        return $content;
    }
}