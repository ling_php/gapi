<?php

namespace gapi\command;

use app\common\lib\Plugin;
use gapi\Command;

# php build pluginstable readpay [plugin_name]
class PluginTable
{
    public static string $plugin = '';

    public static function execute(?array $params, Command $output): void
    {

        $plugin_name = isset($params[0]) ? $params[0] : '';

        $tables = Plugin::create($plugin_name)->updateTable();

        if ($tables) {
            foreach ($tables as $table) {
                echo "{$table} 更新成功\n";
            }
        }
    }


}