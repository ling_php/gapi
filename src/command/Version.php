<?php

namespace gapi\command;

use gapi\Command;
use gapi\Loader;
use gapi\Response;


class Version
{

    public static function execute(?array $params, Command $output): void
    {
        $version = isset($params[0]) ? $params[0] : Config::system()['app_version'];
        $full = isset($params[1]) ? $params[1] : '';
        echo "创建新版本[{$version}]\n";
        $versions = array_reverse(Loader::version());
        $to = APP_PATH . DS . $version . DS;




        if ($full == 'full') {
            if (!dir_is_empty($to)) {
                (new Response())->setMsg("{$to} 已经存在")->send();
            }
            foreach ($versions as $v) {
                $from = APP_PATH . DS . $v . DS;
                dir_copy($from, $to);
                echo "{$from} => {$to} 复制成功\n";
            }
        } else {
            if (file_exists($to)) {
                (new Response())->setMsg("{$to} 已经存在")->send();
            }
            mkdir($to, 0777, true);
        }

    }

}