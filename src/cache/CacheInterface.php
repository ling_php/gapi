<?php

namespace gapi\cache;

interface CacheInterface
{

    /**
     * 获取 key 的生存时间
     * @param string $key
     * @return int|bool
     */
    public function ttl(string $key): int|bool;

    /**
     * @param string $key
     * @return bool
     */
    public function delete(string $key): bool;

    /**
     * @param string $key
     * @return int|bool
     */
    public function has(string $key): int|bool;

    /**
     * @param string $key
     * @param mixed $value
     * @param int|array|null $timeout
     * @return bool
     */
    public function set(string $key, mixed $value, int|array $timeout = null): bool;

    /**
     * @param array $values
     * @param int|array|null $timeout
     * @return bool
     */
    public function setMultiple(array $values, int|array $timeout = null): bool;


    /**
     * @param array $keys
     * @param mixed|null $default
     * @return array
     */
    public function getMultiple(array $keys, mixed $default = null): array;

    /**
     * @param array $keys
     * @return bool
     */
    public function deleteMultiple(array $keys): bool;

    /**
     * @param string $key
     * @param mixed|null $default
     * @return mixed
     */
    public function get(string $key, mixed $default = null): mixed;


    /**
     * @param bool $flag
     * @return bool
     */
    public function clear(bool $flag = false): bool;


}