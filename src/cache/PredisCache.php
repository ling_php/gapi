<?php

namespace gapi\cache;

use Predis\Client;

/**
 * 参考 https://www.cnblogs.com/afeige/p/14385588.html
 * composer require predis/predis
 * Class RedisCache
 * @package gapi\cache
 */
class PredisCache extends Cache implements CacheInterface
{
    private Client $_red;
    private string $prev = 'red_';
    private array $config = ['tcp://127.0.0.1:6379'];
    private mixed $_return_data = null;

    // 当调用的方法不存在会调用call方法,当方法不存在就去访问redis自身的方法
    public function __call(string $func, array $params): mixed
    {
        if ($func == 'multi') {
            $this->_return_data = $this->_red->multi($params[0]);
        } else {
            // 调用red对象下的方法，并传递参数
            $this->_return_data = call_user_func_array(array(&$this->_red, $func), $params);

        }
        return $this->_return_data;
    }

    public function __construct(array $config = [])
    {
        if ($config) {
            $this->_red = new Client($config[0]);
        } else {
            $this->_red = new Client($this->config[0]);
        }
//        if ($config) {
//            $this->_red = new Client([
//                "tcp://{$config['host']}:{$config['port']}"
//            ],['cluster'=>'redis']);
//        } else {
//            $this->_red = new Client([
//                "tcp://{$this->host}:{$this->port}"
//            ],['cluster'=>'redis']);
//        }
    }

    public function ttl(string $key): int|bool
    {
        $new_key = $this->prev . $key;
        return $this->_red->ttl($new_key);
    }

    public function delete(string $key): bool
    {
        $new_key = $this->prev . $key;
        return $this->_red->del($new_key);
    }

    public function rename(string $key1, string $key2): bool
    {
        $new_key1 = $this->prev . $key1;
        $new_key2 = $this->prev . $key2;
        return $this->_red->rename($new_key1, $new_key2);
    }

    public function has(string $key): int|bool
    {
        $new_key = $this->prev . $key;
        return $this->_red->exists($new_key);
    }

    public function set(string $key, mixed $value, int|array $timeout = null): bool
    {
        $new_key = $this->prev . $key;
        return $this->_red->set($new_key, $value, $timeout);
    }

    public function get(string $key, mixed $default = null): mixed
    {
        $new_key = $this->prev . $key;
        if ($this->_red->exists($new_key)) {
            return $this->_red->get($new_key);
        } else {
            return $default;
        }
    }

    public function setMultiple(array $values, array|int $timeout = null): bool
    {
        $multi = $this->_red->multi();
        if ($values) {
            foreach ($values as $key => $value) {
                if (!$multi->set($key, $value, $timeout)) {
                    return false;
                }
            }
        }
        $multi->exec();
        return true;
    }

    public function getMultiple(array $keys, mixed $default = null): array
    {
        $data = [];
        if ($keys) {
            foreach ($keys as $key) {
                $data[$key] = $this->_red->exists($key) ? $this->_red->get($key) : $default;
            }
        }
        return $data;
    }

    public function deleteMultiple(array $keys): bool
    {
        if ($keys) {
            foreach ($keys as $key) {
                if (!$this->delete($key)) {
                    return false;
                }
            }
        }
        return true;
    }

    public function clear(bool $flag = false): bool
    {
        return $flag ? $this->_red->flushAll() : $this->_red->flushDB();
    }

}
