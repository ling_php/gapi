<?php

namespace gapi\cache;

use gapi\Config;

class Cache
{

    private static array $instances = [];

    protected function __construct()
    {
    }

    final public static function create($name = ''): self
    {
        if($name==''){
            $name = Config::file()['cache'];
        }
        if (!isset(self::$instances[$name])) {
            $class = '\\gapi\\cache\\' . ucfirst($name) . 'Cache';
            $config = Config::file();
            if (!isset($config[$name])) {
                throw new \Exception("Cache[{$name}]未配置");
            }
            self::$instances[$name] = new $class($config[$name]);
        }
        return self::$instances[$name];
    }


}