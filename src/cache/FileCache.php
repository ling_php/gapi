<?php

namespace gapi\cache;

class FileCache extends Cache implements CacheInterface
{

    private string $directory = '';
    private string $extension = '.ini';
    private int $directoryStringLength;
    private int $extensionStringLength;

    public function __construct(array $config = [])
    {
        $this->directory = RUNTIME_PATH . DIRECTORY_SEPARATOR . 'file';
        $this->extension = isset($config['extension']) ? $config['extension'] : $this->extension;

        $this->directoryStringLength = strlen($this->directory);
        $this->extensionStringLength = strlen($this->extension);
    }

    public function createPath(string $directory): bool
    {
        return is_dir($directory) ? true : @mkdir($directory, 0777, true);
    }

    public function getFilename(string $key): string
    {
        $hash = hash('sha256', $key);
        if (
            $key === ''
            || ((strlen($key) * 2 + $this->extensionStringLength) > 255)
            || (($this->directoryStringLength + 4 + strlen($key) * 2 + $this->extensionStringLength) > 258)
        ) {
            $filename = '_' . $hash;
        } else {
            $filename = bin2hex($key);
        }
        return $this->directory
            . DIRECTORY_SEPARATOR
            . substr($hash, 0, 2)
            . DIRECTORY_SEPARATOR
            . $filename
            . $this->extension;
    }

    /**
     * @param string $filename
     * @param string $content
     * @return bool
     */
    protected function writeFile(string $filename, string $content): bool
    {
        $filepath = pathinfo($filename, PATHINFO_DIRNAME);

        if (!$this->createPath($filepath)) {
            return false;
        }

        if (!is_writable($filepath)) {
            return false;
        }
        return file_put_contents($filename, $content);
//        $tmpFile = tempnam($filepath, 'swap');
//        @chmod($tmpFile, 0666 & (~$this->umask));
//        if (file_put_contents($filename, $content) !== false) {
//            @chmod($tmpFile, 0666 & (~$this->umask));
//            if (@rename($tmpFile, $filename)) {
//                return true;
//            }
//            @unlink($tmpFile);
//        }
    }


    public function ttl(string $key): int|bool
    {
        $filename = $this->getFilename($key);
        if (!is_file($filename)) {
            return 0;
        }
        $resource = fopen($filename, 'rb');
        $line = fgets($resource);
        fclose($resource);
        if ($line !== false) {
            $lifetime = (int)$line;
        }
        if ($lifetime == 0) {
            //不限时间
            return -1;
        }
        if ($lifetime !== 0 && $lifetime > time()) {
            return $lifetime - time();
        } else {
            $this->delete($key);
        }
        return 0;
    }

    public function delete(string $key): bool
    {
        $filename = $this->getFilename($key);
        return @unlink($filename) || !is_file($filename);
    }

    public function has(string $key): int|bool
    {
        $filename = $this->getFilename($key);
        return is_file($filename);
    }

    public function set(string $key, mixed $value, array|int $timeout = null): bool
    {
        if (is_int($timeout)) {
            $lifetime = $timeout + time();
        } else {
            $lifetime = 0;
        }
        $data = serialize($value);
        return $this->writeFile($this->getFilename($key), $lifetime . PHP_EOL . $data);
    }

    public function setMultiple(array $values, array|int $timeout = null): bool
    {
        if ($values) {
            foreach ($values as $key => $value) {
                if (!$this->set($key, $value, $timeout)) {
                    return false;
                }
            }
        }
        return true;
    }

    public function deleteMultiple(array $keys): bool
    {
        if ($keys) {
            foreach ($keys as $key) {
                if (!$this->delete($key)) {
                    return false;
                }
            }
        }
        return true;
    }

    public function getMultiple(array $keys, mixed $default = null): array
    {
        $data = [];
        if ($keys) {
            foreach ($keys as $key) {
                $data[$key] = $this->has($key) ? $this->get($key) : $default;
            }
        }
        return $data;
    }

    public function get(string $key, mixed $default = null): mixed
    {
        $data = '';
        $lifetime = -1;
        $filename = $this->getFilename($key);
        if (!is_file($filename)) {
            return $default;
        }

        $resource = fopen($filename, 'rb');
        $line = fgets($resource);

        if ($line !== false) {
            $lifetime = (int)$line;
        }

        if ($lifetime !== 0 && $lifetime < time()) {
            fclose($resource);
            $this->delete($key);
            return $default;
        }

        while (($line = fgets($resource)) !== false) {
            $data .= $line;
        }

        fclose($resource);

        return unserialize($data, [
            'allowed_classes' => true,
        ]);
    }

    /**
     * 删除目录及目录下面的所有文件
     * @param string $dir
     * @return bool
     */
    public static function dirDelete(string $dir): bool
    {
        $dir = self::dirPath($dir);
        if (!is_dir($dir)) return false;
        $list = glob($dir . '*');
        foreach ($list as $v) {
            is_dir($v) ? self::dirDelete($v) : unlink($v);
        }
        return @rmdir($dir);
    }

    /**
     * @param string $path
     * @return string
     */
    public static function dirPath(string $path): string
    {
        $path = str_replace('\\', '/', $path);
        if (substr($path, -1) != '/') $path = $path . '/';
        return $path;
    }

    public function clear(bool $flag = false): bool
    {
        return self::dirDelete($this->directory);
    }
}