<?php

namespace gapi\cache;

class Sqlite3Cache extends Cache implements CacheInterface
{

    private string $dbfile = 'cache.db';
    /**
     * @var string
     */
    private string $table = 'cache';
    /**
     * The ID field will store the cache key.
     */
    public const ID_FIELD = 'k';

    /**
     * The data field will store the serialized PHP value.
     */
    public const DATA_FIELD = 'd';

    /**
     * The expiration field will store a date value indicating when the
     * cache entry should expire.
     */
    public const EXPIRATION_FIELD = 'e';


    public function tableExists(string $table)
    {
        try {
            $this->sqlite->exec("SELECT * FROM {$table}");
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Sqlite3Cache constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->sqlite = new \SQLite3($config['dbfile']);
        $this->table = isset($config['table']) ? $config['table'] : $this->table;
        $this->sqlite->exec(
            sprintf(
                'CREATE TABLE IF NOT EXISTS %s(%s TEXT PRIMARY KEY NOT NULL, %s BLOB, %s INTEGER)',
                $this->table,
                static::ID_FIELD,
                static::DATA_FIELD,
                static::EXPIRATION_FIELD
            )
        );
    }

    /**
     * @return array
     */
    private function getFields(): array
    {
        return [static::ID_FIELD, static::DATA_FIELD, static::EXPIRATION_FIELD];
    }

    /**
     * @param string $key
     * @param mixed|null $default
     * @return mixed
     */
    public function get(string $key, mixed $default = null): mixed
    {
        $item = $this->findById($key);

        if (!$item) {
            return false;
        }

        return unserialize($item[self::DATA_FIELD], [
            'allowed_classes' => true,
        ]);
    }

    /**
     * @param $id
     * @param bool $includeData
     * @return array|null
     */
    private function findById($id, bool $includeData = true): ?array
    {
        [$idField] = $fields = $this->getFields();

        if (!$includeData) {
            $key = array_search(static::DATA_FIELD, $fields);
            unset($fields[$key]);
        }

        $statement = $this->sqlite->prepare(sprintf(
            'SELECT %s FROM %s WHERE %s = :id LIMIT 1',
            implode(',', $fields),
            $this->table,
            $idField
        ));
        $statement->bindValue(':id', $id, SQLITE3_TEXT);
        $item = $statement->execute()->fetchArray(SQLITE3_ASSOC);
        if ($item === false) {
            return null;
        }
        if ($this->isExpired($item)) {
            $this->delete($id);
            return null;
        }
        return $item;
    }

    /**
     * @param array $item
     * @return bool
     */
    private function isExpired(array $item): bool
    {
        return isset($item[static::EXPIRATION_FIELD]) &&
            $item[self::EXPIRATION_FIELD] !== null &&
            $item[self::EXPIRATION_FIELD] < time();
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param DateInterval|int|null $ttl
     * @return bool
     */
    public function set(string $key, mixed $value, array|int $timeout = null): bool
    {
        $statement = $this->sqlite->prepare(sprintf(
            'INSERT OR REPLACE INTO %s (%s) VALUES (:id, :data, :expire)',
            $this->table,
            implode(',', $this->getFields())
        ));
        $statement->bindValue(':id', $key);
        $statement->bindValue(':data', serialize($value), SQLITE3_BLOB);
        if (is_int($timeout)) {
            $lifetime = time() + $timeout;
        } else if ($timeout === null) {
            $lifetime = 0;
        }
        $statement->bindValue(':expire', $lifetime > 0 ? $lifetime : null);

        return $statement->execute() instanceof SQLite3Result;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function delete(string $key): bool
    {
        [$idField] = $this->getFields();

        $statement = $this->sqlite->prepare(sprintf(
            'DELETE FROM %s WHERE %s = :id',
            $this->table,
            $idField
        ));

        $statement->bindValue(':id', $key);
        return $statement->execute() instanceof SQLite3Result;
    }

    /**
     * @param bool $flag
     * @return bool
     */
    public function clear(bool $flag = false): bool
    {
        return $this->sqlite->exec(sprintf('DELETE FROM %s', $this->table));
    }

    /**
     * @param iterable $keys
     * @param mixed|null $default
     * @return iterable
     */
    public function getMultiple(array $keys, mixed $default = null): array
    {
        $data = [];
        foreach ($keys as $key) {
            $data[$key] = $this->get($key, $default);
        }
        return $data;
    }

    /**
     * @param iterable $values
     * @param array|int|null $timeout
     * @return bool
     */
    public function setMultiple(iterable $values, array|int $timeout = null): bool
    {
        foreach ($values as $key => $value) {
            $this->set($key, $value, $timeout);
        }
        return true;
    }

    /**
     * @param iterable $keys
     * @return bool
     */
    public function deleteMultiple(iterable $keys): bool
    {
        foreach ($keys as $key) {
            if (!$this->delete($key)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $key
     * @return bool
     */
    public function has($key): bool
    {
        return $this->findById($key, false) !== null;
    }

    public function ttl(string $key): int|bool
    {
        [$idField] = $fields = $this->getFields();
        $statement = $this->sqlite->prepare(sprintf(
            'SELECT %s FROM %s WHERE %s = :id LIMIT 1',
            implode(',', $fields),
            $this->table,
            $idField
        ));
        $statement->bindValue(':id', $key, SQLITE3_TEXT);
        $item = $statement->execute()->fetchArray(SQLITE3_ASSOC);
        if(!$item){
            return 0;
        }
        if ($item[self::EXPIRATION_FIELD] == 0) {
            return -1;
        } else {
            $ttl = $item[self::EXPIRATION_FIELD] - time();
            if ($ttl <= 0) {
                $this->delete($key);
                return 0;
            } else {
                return $ttl;
            }
        }
    }
}