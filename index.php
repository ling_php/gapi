<?php
require __DIR__ . DIRECTORY_SEPARATOR . 'vendor/autoload.php';
$app = (new \gapi\Application())->create();
$app->send(
    [
        'method' => 'get',
        'path' => ['/phpinfo'],
        'action' => function (\gapi\Request $request, \gapi\Response $response) {
            phpinfo();
            return $response->string();
        },
    ]
);
$app->send();

//match ((new \gapi\Request())->domain()) {
//    'http://admins.com'=> (new \gapi\Application())->create()->send(),
//    'http://a.jiuaitu.com' => (new \gapi\Application('v1.0.1'))->create()->send(),
//    'http://a3.com' => (new \gapi\Application('v1.0.0'))->create()->send(
//        [
//            'method' => 'get',
//            'path' => ['/hello/{num}'],
//            'action' => 'home/index/hello',
//            'pattern' => ['num' => '\d+']
//        ]
//    ),
//    'http://a2.com' => (new \gapi\Application('v1.0.2'))->create()->send(
//        [
//            'method' => 'get',
//            'path' => ['/'],
//            'action' => function (\gapi\Request $request, \gapi\Response $response) {
//                return $response->setMsg('Hello world! - Closure');
//            },
//
//        ]
//    ),
//};

//\gapi\Route::all(['/hello/{num}'],'home/Index/hello',['num'=>'\d+']);


