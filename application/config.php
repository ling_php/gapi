<?php

use gapi\lib\Logger;

return [
    #日志配置
    'debug' => false,
    'trace' => false,
    'log_file' => false,
    'wss' => false,
    'log_level' => [Logger::ERROR], //Logger::INFO,  Logger::SQL, Logger::INFO, Logger::SQL , Logger::DEBUG, Logger::INFO

    #js跨域配置
    'js_allow_origin' => true,
    'allow_origin' => 'http://localhost:8080,http://localhost:8081,http://192.168.0.102:8080,http://a.jiuaitu.com,http://a.com,http://gadmin.jiuaitu.com',

    #权限验证
    'auth_key' => 'ling2021',
    'auth_expire' => 86400 * 7, //验证过期时间

    # 缓存配置
    'cache' => 'file',
    'redis' => [
        'host' => '127.0.0.1',
        'port' => '6379',
        'dbindex' => 1,
    ],
    'predis' => [
        'tcp://127.0.0.1:6379',
    ],
    'file' => [
        'extension' => '.ini',
    ],
    'sqlite3' => [
        'dbfile' => 'cache3.db',
    ],

    //模板
    'template' => [
        'engine' => \gapi\template\driver\Ling::class,
    ],
    'tpl_strip_space' => false,
    'static_path' => [
        '__STATIC__' => '/static',
        '__IMAGE__' => '/static/images',
        '__CSS__' => '/static/css',
        '__JS__' => '/static/js',
    ],
    'upload_path' => 'upload/',
];