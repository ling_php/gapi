<?php
$ignore = ['runtime', 'column'];
$lists = dir_list_one(VERSION_PATH.DS);
$modules = array_diff($lists,$ignore);
sort($modules);
return $modules;