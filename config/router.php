<?php

use gapi\Route;
use gapi\Response;
use gapi\Request;
use gapi\Session;


Route::all(['/upload/{file_name}@{width}X{height}', '/themes/.+?/static/images/{file_name}@{width}X{height}'], function (Request $request, Response $response) {
    if (str_contains($request->uri, '/themes/')) {
        $tmp = explode('@', $request->uri);
        $file = ROOT_PATH . $tmp[0];
    } else {
        $file = ROOT_PATH . DS . 'upload/' . $request->route['file_name'];
    }

    if (!is_file($file)) {
        return '';
    }

    $cache_time = 60 * 60 * 24 * 30;
    header("Expires: " . gmdate("D, d M Y H:i:s", NOW_TIME + $cache_time) . " GMT");
    header("Pragma: cache");
    header("Cache-Control: max-age=$cache_time");
    $etag = md5_file($file);
    header("Etag: $etag");

    $image = \Intervention\Image\ImageManagerStatic::make($file);
    $height = $request->route['height'];
    $width = $request->route['width'];

    $img = getimagesize($file);
    $imgw = $img[0];
    $imgh = $img[1];
    if ($height == 'auto') {
        $height = round($width * $imgh / $imgw);
    }
    if ($width == 'auto') {
        $width = round($height * $imgw / $imgh);
    }
    echo $image->resize($width, $height)->response($image->extension);
    return $response->string();
}, ['file_name' => '.+', 'width' => '\w+', 'height' => '\w+',]);
//上传文件
Route::all(['/uploadfile'], function (Request $request, Response $response) {
    Session::boot();
    \app\common\lib\Helper::uploadFile($request, $response);
    return $response->success();
}, []);
//上传图片
Route::all(['/uploadimage'], function (Request $request, Response $response) {
    Session::boot();
    \app\common\lib\Helper::upload($request, $response);
    return $response->success();
}, []);
//文件管理
Route::all(['/upload/filemanager'], function (Request $request, Response $response) {
    $token = $request->get('token', '');
    (new \app\common\middleware\DbAuth)->getUser($request, $response, $token);
    $user = $request->getMiddleWareData()['manager'];
    return $response->success();
}, []);

//文件管理2
Route::all(['/upload/filemanagers'], function (Request $request, Response $response) {
    $token = $request->get('token', '');
    (new \app\common\middleware\DbAuth)->getUser($request, $response, $token);
    $user = $request->getMiddleWareData()['manager'];
    return $response->success();
}, []);

//图片裁剪
Route::all(['/image/crop'], function (Request $request, Response $response) {
    $token = $request->get('token', '');
    \app\common\middleware\DbAuth::getUser($request, $response, $token);
    $user = $request->getMiddleWareData()['manager'];

    $base64_image_content = $request->post('image');
    $upload = new \app\common\lib\Upload($request, $response);
    $ret = $upload->base64ImageSave($base64_image_content);
    if ($ret) {
        $manager = $request->getMiddleWareData()['manager'];
        $this->model::model()->update([t_managers::AVATAR => $ret['url']], [t_managers::ID => $manager[t_managers::ID]]);
        return $response->success()->setMsg('保存成功')->setData($ret);
    }

    return $response->fail()->setMsg('请求错误');
}, []);
//ueditor
Route::all(['/editor/ueditor'], function (Request $request, Response $response) {
    # action 入口
    $action = $request->get('action');
    $path = ROOT_PATH . \gapi\Config::app()['static_path']['__STATIC__'] . '/lib/ueditor/1.4.3/php/';
    $config = json_decode(preg_replace("/\/\*[\s\S]+?\*\//", "", file_get_contents($path . 'config.json')), true);
    switch ($action) {
        case 'config':
            $result = json_encode($config);
            break;
        /* 上传图片 */
        case 'uploadimage':
            /* 上传涂鸦 */
        case 'uploadscrawl':
            /* 上传视频 */
        case 'uploadvideo':
            /* 上传文件 */
        case 'uploadfile':
            Session::boot();
            $data = \app\common\lib\Helper::upload($request, $response);
            \app\common\form\ueditor\Ueditor::success($data);
            return $response->string();
            break;
        /* 列出图片 */
        case 'listimage':
            $file = new \app\common\form\ueditor\FileManager('', 'image', 'name');
            $result = $file->ueditor();
            break;
        /* 列出文件 */
        case 'listfile':
            $file = new \app\common\form\ueditor\FileManager('', 'file', 'name');
            $result = $file->ueditor();
            break;
        /* 抓取远程文件 */
        case 'catchimage':
            // $result = include($path . "action_crawler.php");
            break;

        default:
            $result = json_encode(array(
                'state' => '请求地址出错'
            ));
            break;
    }

    /* 输出结果 */
    if (isset($_GET["callback"])) {
        if (preg_match("/^[\w_]+$/", $_GET["callback"])) {
            echo htmlspecialchars($_GET["callback"]) . '(' . $result . ')';
        } else {
            echo json_encode(array(
                'state' => 'callback参数不合法'
            ));
        }
    } else {
        echo $result;
    }
    return $response->string();
}, []);
//图片预览
Route::all(['/attachment/preview'], function (Request $request, Response $response) {

    $DIR = 'preview';
    // Create target dir
    if (!file_exists($DIR)) {
        @mkdir($DIR);
    }

    $cleanupTargetDir = true; // Remove old files
    $maxFileAge = 5 * 3600; // Temp file age in seconds

    if ($cleanupTargetDir) {
        if (!is_dir($DIR) || !$dir = opendir($DIR)) {
            die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
        }

        while (($file = readdir($dir)) !== false) {
            $tmpfilePath = $DIR . DIRECTORY_SEPARATOR . $file;

            // Remove temp file if it is older than the max age and is not the current file
            if (@filemtime($tmpfilePath) < time() - $maxFileAge) {
                @unlink($tmpfilePath);
            }
        }
        closedir($dir);
    }

    $src = file_get_contents('php://input');

    if (preg_match("#^data:image/(\w+);base64,(.*)$#", $src, $matches)) {

        $previewUrl = sprintf(
            "%s://%s%s",
            isset($_SERVER['HTT 吧PS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['HTTP_HOST'],
            $_SERVER['REQUEST_URI']
        );
        $previewUrl = str_replace("preview.php", "", $previewUrl);

        $base64 = $matches[2];
        $type = $matches[1];
        if ($type === 'jpeg') {
            $type = 'jpg';
        }

        $filename = md5($base64) . ".$type";
        $filePath = $DIR . DIRECTORY_SEPARATOR . $filename;

        if (file_exists($filePath)) {
            die('{"jsonrpc" : "2.0", "result" : "' . $previewUrl . 'preview/' . $filename . '", "id" : "id"}');
        } else {
            $data = base64_decode($base64);
            file_put_contents($filePath, $data);
            die('{"jsonrpc" : "2.0", "result" : "' . $previewUrl . 'preview/' . $filename . '", "id" : "id"}');
        }

    } else {
        die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "un recoginized source"}}');
    }
    return $response->string();
}, []);
//百度地图
Route::all(['/baidumap'], function (Request $request, Response $response) {
    include ROOT_PATH . DS . 'static/map/map.html';
    return $response->string();
}, []);